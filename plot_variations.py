import numpy as np

import matplotlib.pyplot as plt
from cycler import cycler
from coffea import util
from coffea.processor import accumulate
import hist
import hist.intervals
import uproot
import os
import mplhep as hep

from vll.utils.plotting import plotWithRatio, RebinHist, SetRangeHist, GroupBy
from vll.utils.crossSections import lumis, crossSections, nevents


def load_data_from_config(config_file):

    with open(config_file, "r") as f:
        config = json.load(f)
    
    input_dir = config.get("input_dir")
    outdir = config.get("outdir")
    version = config.get("version")
    era = config.get("era","2018")
    mc_files = config.get("mc_files", {})
    outputMC = accumulate([
        util.load(os.path.join(input_dir, version, era, mc_file)) for mc_file in mc_files
    ])
    
    return outputMC, input_dir, outdir, version, era

def main(config_file):

    outputMC, input_dir, outdir, version, era = load_data_from_config(config_file)

    luminosity = lumis[era]/1000.

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # Plot superimposed distributions
    variableList = ["MET_pt",'lead_lep_pt','sublead_lep_pt','lep_mll','event_HT','event_bjet_HT','lead_jet_pT','sub_jet_pT','lead_bjet_pT','sub_bjet_pT']
    axes = ['dataset', 'lepFlavor','tauMultiplicity','bJetMultiplicity','systematic']
    xRange1=[0,300]
    xRange2=[0,500]
    xRange3=[0,1000]
    xRanges = [xRange2,xRange2,xRange1,xRange2,[300,1000],xRange3,xRange3,xRange3,xRange3,xRange3]
    labels = ['MET [GeV]','Leading lepton $p_{T}$ [GeV]','Sub-leading lepton $p_{T}$ [GeV]','$m_{ll}$ [GeV]',' Event jet $H_{T}$ [GeV]',' Event b-jet $H_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading jet $p_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading b-jet $p_{T}$ [GeV]']


    #######################
    # Grouping
    #######################

    groupingCategory = {
        "CR": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b'],
        "SR": ['jetSel_4j4b','jetSel_5j4b','jetSel_6j4b'],
    }

    groupingMCDatasets = {
        "other": [ "TTHH","TTWW","TTZZ","TTZH","TTWZ","TTWH","WW","ZZ","WZ","WWW","ZZZ","WWZ","WZZ",  ],
        "tttt": [ "TTTT", ],
        "ttV" : [ "TTW","TTZ",],
        "ttH" : [ "TTHToNonBB","TTHToBB",],
        "singlet-top": [ "ST_tW_channel","ST_tbarW_channel","ST_s_channel","ST_t-channel_antiT","ST_t-channel_T",],
        "dy": ["DYjetsM50"],#,"DYjetsM10to50"],
        "ttbar": [ "TTbarPowheg_Dilepton",],
        "VLL_M-500GeV": ["VLL_EE_M-500GeV","VLL_EN_M-500GeV","VLL_NN_M-500GeV",],
        "VLL_M-550GeV": ["VLL_EE_M-550GeV","VLL_EN_M-550GeV","VLL_NN_M-550GeV",],
        "VLL_M-600GeV": ["VLL_EE_M-600GeV","VLL_EN_M-600GeV","VLL_NN_M-600GeV",],
        "VLL_M-650GeV": ["VLL_EE_M-650GeV","VLL_EN_M-650GeV","VLL_NN_M-650GeV",],
        "VLL_M-700GeV": ["VLL_EE_M-700GeV","VLL_EN_M-700GeV","VLL_NN_M-700GeV",],
        "VLL_M-750GeV": ["VLL_EE_M-750GeV","VLL_EN_M-750GeV","VLL_NN_M-750GeV",],
        "VLL_M-800GeV": ["VLL_EE_M-800GeV","VLL_EN_M-800GeV","VLL_NN_M-800GeV",],
        "VLL_M-850GeV": ["VLL_EE_M-850GeV","VLL_EN_M-850GeV","VLL_NN_M-850GeV",],
        "VLL_M-900GeV": ["VLL_EE_M-900GeV","VLL_EN_M-900GeV","VLL_NN_M-900GeV",],
        "VLL_M-950GeV": ["VLL_EE_M-950GeV","VLL_EN_M-950GeV","VLL_NN_M-950GeV",],
        "VLL_M-1000GeV": ["VLL_EE_M-1000GeV","VLL_EN_M-1000GeV","VLL_NN_M-1000GeV",],
    }

    groupingSyst = {
        "muEffWeight": ['nominal', 'muEffWeightUp', 'muEffWeightDown',],
        "eleEffWeight": ['nominal', 'eleEffWeightUp', 'eleEffWeightDown',],
        "TrigWeight": ['nominal', 'muTrigWeightUp', 'muTrigWeightDown',],
        "puWeight": ['nominal', 'puWeightUp','puWeightDown',],
        "btagVariation_cferr1": ['nominal', 'btagVariation_cferr1Up','btagVariation_cferr1Down'],
        "btagVariation_cferr2": ['nominal','btagVariation_cferr2Up','btagVariation_cferr2Down'],
        "btagVariation_hf": ['nominal','btagVariation_hfUp','btagVariation_hfDown'],
        "btagVariation_hfstats1":  ['nominal', 'btagVariation_hfstats1Up', 'btagVariation_hfstats1Down',], 
        "btagVariation_hfstats2": ['nominal','btagVariation_hfstats2Up','btagVariation_hfstats2Down'], 
        "btagVariation_lf": ['nominal','btagVariation_lfUp','btagVariation_lfDown'], 
        "btagVariation_lfstats1": ['nominal','btagVariation_lfstats1Up','btagVariation_lfstats1Down'], 
        "btagVariation_lfstats2": ['nominal','btagVariation_lfstats2Up','btagVariation_lfstats2Down'], 
        "JES": ['nominal','JESUp','JESDown'], 
        "JER": ['nominal','JERUp','JERDown'], 
        "ISR": ['nominal','ISRUp','ISRDown'],
        "FSR": ['nominal','FSRUp','FSRDown'],
        "PDF": ['nominal','PDFUp','PDFDown']
    }

    # Group MC histograms
    histList = []
    for samp, sampList in groupingMCDatasets.items():
        histList += [outputMC[s] for s in sampList]

    outputMCHist = accumulate(histList)

    for key, histo in outputMCHist.items():
        if isinstance(histo, hist.Hist):
            outputMCHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingMCDatasets)

    # For debugging
    fig, ax = plt.subplots(figsize=(10, 10))
    for _dataset in groupingMCDatasets:
        o_dir = outdir+'/'+_dataset
        if not os.path.exists(o_dir):
            os.makedirs(o_dir)
        h = outputMCHist['event_ST'][{'lepFlavor':sum}][{'dataset':_dataset}][{'dileptonCharge': sum}]
        h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
        h = h[{'bJetMultiplicity':'SR'}]
        for _systematic in groupingSyst:
            h_sys = h[{'systematic': ["nominal", _systematic+'Up', _systematic+'Down']}]
            h_sys[::10j, :].plot1d(overlay='systematic', linestyle='-')
            print(h_sys)
            hep.cms.label( data=False, year=2018, lumi=58)
            ax.legend(['Nominal', 'Up', 'Down'], loc='best')
            plt.legend()
            plt.savefig(o_dir+'/'+'syst_'+_systematic+'.png')
            plt.close()

if __name__ == "__main__":

    import argparse
    import json

    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--config_file", type=str, help="Path to the config file")

    args = parser.parse_args()

    main(args.config_file)