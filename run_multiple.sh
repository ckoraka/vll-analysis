#!/bin/bash

# List of processes to run
processes=("MCTTbar2l" "TTV" "TTVV" "MCZJets" "DiBoson" "TriBoson" "MCSingleTop" "Signal" "Data")

# Define worker count for each process
declare -A workers
workers["DiBoson"]=30
workers["TriBoson"]=30
workers["TTV"]=70
workers["TTVV"]=70
workers["MCTTbar2l"]=100
workers["MCZJets"]=100
workers["MCSingleTop"]=100
workers["Signal"]=100
workers["Data"]=100

# Path to the singularity image
singularity_image="/cvmfs/cms.hep.wisc.edu/ckoraka/coffea-dask-with-mt2-tf.sif"

# Environment setup function
install_env() {
    set -e
    echo "Installing shallow virtual environment in $PWD/.env..."
    python -m venv --without-pip --system-site-packages .env
    unlink .env/lib64  # HTCondor can't transfer symlink to directory and it appears optional
    export TMPDIR=$(mktemp -d -p .)
    .env/bin/python -m ipykernel install --user
    rm -rf $TMPDIR && unset TMPDIR
    .env/bin/python -m pip install -q git+https://github.com/CoffeaTeam/lpcjobqueue.git@v0.2.3
    echo "done."
}

# Ensure the environment is prepared
export JUPYTER_PATH=/srv/.jupyter
export JUPYTER_RUNTIME_DIR=/srv/.local/share/jupyter/runtime
export JUPYTER_DATA_DIR=/srv/.local/share/jupyter
export IPYTHONDIR=/srv/.ipython

# Loop through the processes
for process in "${processes[@]}"; do
    # Set workers for the current process
    num_workers=${workers[$process]}

    # Name the screen session according to the process name
    screen_name="${process}_screen"

    # Start a new screen session in detached mode
    screen -dmS "$screen_name" bash -c "
        EXTERNAL_BIND=\${PWD} singularity exec -B \${PWD}:/srv -B /etc/condor -B /scratch -B /cvmfs -B /hdfs --pwd /srv \
        $singularity_image \
        /bin/bash --rcfile /srv/.bashrc -c '
            [[ -d .env ]] || install_env
            source .env/bin/activate
            alias pip=\"python -m pip\"
            /usr/local/bin/python runFullDataset2018.py $process -e wiscjq --era 2018 --workers $num_workers --chunksize 30000 --outdir v7_2018
        ';
        exec bash
    "

    echo "Started screen session: $screen_name for process: $process with $num_workers workers"
done

echo "All screen sessions started."
