#!/usr/bin/env python3
import uproot
import datetime
import logging

from coffea import util, processor
from coffea.nanoevents import NanoAODSchema
from vll.processor import VLLProcessor

import time
import os

import argparse

# Import 2018 file lists for all necessary groups
from samples.fileLists.skims.Signal_2018 import filesetSignal as filelistSignal
from samples.fileLists.skims.MC_2018 import filelist2018 as filelistMC
from samples.fileLists.skims.Data_2018 import data2018 as filelistData

# Define mappings for groups, dynamically adjust based on the mcGroup passed
mc_group_mapping = {
    "Signal": [key for key in filelistSignal if "VLL" in key],
    "MCTTbar1l": ["TTbarPowheg_Semilept", "TTbarPowheg_Hadronic"],
    "MCTTbar2l": ["TTbarPowheg_Dilepton"],
    "TTV": ["TTW", "TTZ", "TTHToNonBB", "TTHToBB"],
    "TTVV": ["TTHH", "TTWW", "TTZZ", "TTZH", "TTWZ", "TTWH", "TTTT"],
    "MCZJets": [key for key in filelistMC if "DY" in key],
    "DiBoson": ["WW", "ZZ", "WZ"],
    "TriBoson": ["WWW", "ZZZ", "WWZ", "WZZ"],
    "MCSingleTop": [key for key in filelistMC if "ST" in key],
}

mc_nonother = {key for group in mc_group_mapping.values() for key in group}

mc_group_mapping["MCOther"] = [
    key for key in filelistMC if (key not in mc_nonother) and ("Data" not in key)
]
mc_group_mapping["MCAll"] = [
    key for group in mc_group_mapping.values() for key in group
]

def move_X509():
    try:
        _x509_localpath = (
            [
                line
                for line in os.popen("voms-proxy-info").read().split("\n")
                if line.startswith("path")
            ][0]
            .split(":")[-1]
            .strip()
        )
    except Exception as err:
        raise RuntimeError(
            "x509 proxy could not be parsed, try creating it with 'voms-proxy-init'"
        ) from err
    _x509_path = f'/scratch/{os.environ["USER"]}/{_x509_localpath.split("/")[-1]}'
    os.system(f"cp {_x509_localpath} {_x509_path}")
    return os.path.basename(_x509_localpath)

if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s %(name)s:%(levelname)s:%(message)s",
        level=logging.WARNING,
    )

    parser = argparse.ArgumentParser(
        description="Batch processing script for vll analysis"
    )
    parser.add_argument(
        "mcGroup",
        choices=list(mc_group_mapping) + ["Data"],
        help="Name of process to run",
    )
    parser.add_argument(
        "--era",
        choices=["2018"],  # Only allowing 2018 in this case
        default="2018",
        help="Era to run over",
    )
    parser.add_argument("--chunksize", type=int, default=10000, help="Chunk size")
    parser.add_argument("--maxchunks", type=int, default=None, help="Max chunks")
    parser.add_argument("--workers", type=int, default=1, help="Number of workers")
    parser.add_argument("--outdir", type=str, default="Outputs", help="Where to put the output files")
    parser.add_argument(
        "--batch", action="store_true", help="Batch mode (no progress bar)"
    )
    parser.add_argument(
        "-e",
        "--executor",
        choices=["local", "wiscjq", "debug"],
        default="local",
        help="How to run the processing",
    )
    args = parser.parse_args()

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    tstart = time.time()

    print("Running mcGroup {}".format(args.mcGroup))

    # Define executor based on choice
    if args.executor == "local":
        if args.workers > 4:
            raise RuntimeError("You probably shouldn't run more than 4 cores locally")
        executor = processor.FuturesExecutor(
            workers=args.workers, status=not args.batch
        )
    elif args.executor == "debug":
        executor = processor.IterativeExecutor(status=not args.batch)
    elif args.executor == "wiscjq":
        from distributed import Client
        from dask_jobqueue import HTCondorCluster

        if args.workers == 1:
            print("Are you sure you want to use only one worker?")

        os.environ["CONDOR_CONFIG"] = "/etc/condor/condor_config"
        _x509_path = move_X509()

        cluster = HTCondorCluster(
            cores=1,
            memory="5 GB",
            disk="3 GB",
            death_timeout='60',
            job_extra_directives={
                "+JobFlavour": '"tomorrow"',
                "log": "dask_job_output.$(PROCESS).$(CLUSTER).log",
                "output": "dask_job_output.$(PROCESS).$(CLUSTER).out",
                "error": "dask_job_output.$(PROCESS).$(CLUSTER).err",
                "should_transfer_files": "yes",
                "when_to_transfer_output": "ON_EXIT_OR_EVICT",
                "transfer_executable": "false",
                "+SingularityImage": '"/cvmfs/cms.hep.wisc.edu/ckoraka/coffea-dask-with-mt2-tf.sif"',
                "Requirements": "HasSingularityJobStart",
                "InitialDir": f'/scratch/{os.environ["USER"]}',
                "transfer_input_files": f'{os.environ["EXTERNAL_BIND"]}/.env,{_x509_path},{os.environ["EXTERNAL_BIND"]}/vll'
            },
            job_script_prologue=[
                "export XRD_RUNFORKHANDLER=1",
                f"export X509_USER_PROXY={_x509_path}",
            ]
        )
        cluster.adapt(minimum=1, maximum=args.workers)
        client = Client(cluster)
        executor = processor.DaskExecutor(client=client, status=not args.batch)

    runner = processor.Runner(
        executor=executor,
        schema=NanoAODSchema,
        chunksize=args.chunksize,
        maxchunks=args.maxchunks,
        skipbadfiles=True,
        xrootdtimeout=300,
    )

    if args.mcGroup == "Data":
        print('Processing Data')
        job_fileset = {key: filelistData[key] for key in filelistData if "Data" in key}
        output = runner(
            job_fileset,
            treename="Events",
            processor_instance=VLLProcessor(isMC=False, era=args.era),
        )
    elif args.mcGroup == "Signal":
        print('Processing Signal')
        job_fileset = {key: filelistSignal[key] for key in filelistSignal}
        output = runner(
            job_fileset,
            treename="Events",
            processor_instance=VLLProcessor(isMC=True, era=args.era),
        )
    else:  # Handle other MC groups like MCTTbar2l, TTV, etc.
        print('Processing MC group: {}'.format(args.mcGroup))
        job_fileset = {key: filelistMC[key] for key in mc_group_mapping[args.mcGroup]}
        output = runner(
            job_fileset,
            treename="Events",
            processor_instance=VLLProcessor(isMC=True, era=args.era),
        )

    elapsed = time.time() - tstart
    print(f"Total time: {elapsed:.1f} seconds")

    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    #outfile = os.path.join(args.outdir, f"output_{args.mcGroup}_run{timestamp}.coffea")
    outfile = os.path.join(args.outdir, f"output_{args.mcGroup}.coffea")
    util.save(output, outfile)
    print(f"Saved output to {outfile}")
