redirector = "root://cmsxrootd.fnal.gov//"
#redirector = "root://cmsxrootd.hep.wisc.edu//"

filesetTTbarPowheg_Dilepton2016pre = {      
    "TTbarPowheg_Dilepton": [
        redirector+"/store/mc/RunIISummer20UL16NanoAODAPVv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_preVFP_v11-v1/100000/48931429-D876-6D4D-89A2-B61D5F9D76AF.root", 
        redirector+"/store/mc/RunIISummer20UL16NanoAODAPVv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_preVFP_v11-v1/100000/D91BE235-EA9D-8D43-B60B-B65B39CF6D66.root", 
        redirector+"/store/mc/RunIISummer20UL16NanoAODAPVv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_preVFP_v11-v1/100000/E867C14D-2453-5645-840E-6576EC140C42.root", 
        redirector+"/store/mc/RunIISummer20UL16NanoAODAPVv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_preVFP_v11-v1/100000/4BA0E6F3-A883-C54E-9681-F6906D401108.root"
    ]
}

filesetTTbarPowheg_Dilepton2016post = {      
    "TTbarPowheg_Dilepton": [
        redirector+"/store/mc/RunIISummer20UL16NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_v17-v1/120000/089E6E3A-DFD1-7148-BD6F-F611D22A1B62.root", 
        redirector+"/store/mc/RunIISummer20UL16NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_v17-v1/120000/3CFB3ECE-3C55-FA4C-BCCA-3A0E566710AC.root", 
        redirector+"/store/mc/RunIISummer20UL16NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_v17-v1/120000/4295A038-1506-FF44-A8EA-D0F727B626FC.root", 
        redirector+"/store/mc/RunIISummer20UL16NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mcRun2_asymptotic_v17-v1/120000/D6C8F4D5-7970-754C-944B-700A5A1C6402.root"
    ]
}

filesetTTbarPowheg_Dilepton2017 = {      
    "TTbarPowheg_Dilepton": [
        redirector+"/store/mc/RunIISummer20UL17NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v9-v1/2510000/EFBBD9DF-1B6A-C54A-9CA5-704B819BB6A8.root", 
        redirector+"/store/mc/RunIISummer20UL17NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v9-v1/2510000/E7185D42-E19B-4743-B5D2-2D2A008AB656.root", 
        redirector+"/store/mc/RunIISummer20UL17NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v9-v1/2510000/47450C94-04DF-7048-A46E-E77DEFE35A0A.root", 
        redirector+"/store/mc/RunIISummer20UL17NanoAODv9/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v9-v1/2510000/C9E789DD-BB0B-0344-84C0-FDA560DD2E94.root"
    ]
}

filesetTTbarPowheg_Dilepton2018 = {      
    "TTbarPowheg_Dilepton": [
        redirector+"/store/mc/RunIISummer20UL17NanoAODv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v8-v1/230000/39A0CE26-0241-A74A-8322-C8801E775058.root",
        redirector+"/store/mc/RunIISummer20UL17NanoAODv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v8-v1/230000/39EF31C7-6ADD-2A43-8193-4B8700443F2E.root",
        redirector+"/store/mc/RunIISummer20UL17NanoAODv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v8-v1/230000/3E5CBB96-5598-4F4F-9A8B-F6BAEBAD937A.root",
        redirector+"/store/mc/RunIISummer20UL17NanoAODv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/NANOAODSIM/106X_mc2017_realistic_v8-v1/230000/45AE4F5C-AA8D-2747-A6C1-BD6F4F027B56.root"
    ]
}

filesetSignal = {
    "VLL_EE_M-600GeV": [
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EE_to_4b/RunIISummer20UL18_NanoAODv9/230718_212705/0000/NanoAODv9_10.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EE_to_4b/RunIISummer20UL18_NanoAODv9/230718_212705/0000/NanoAODv9_12.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EE_to_4b/RunIISummer20UL18_NanoAODv9/230718_212705/0000/NanoAODv9_13.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EE_to_4b/RunIISummer20UL18_NanoAODv9/230718_212705/0000/NanoAODv9_14.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EE_to_4b/RunIISummer20UL18_NanoAODv9/230718_212705/0000/NanoAODv9_15.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EE_to_4b/RunIISummer20UL18_NanoAODv9/230718_212705/0000/NanoAODv9_16.root"
    ],
    "VLL_EN_M-600GeV": [
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212731/0000/NanoAODv9_10.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212731/0000/NanoAODv9_11.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212731/0000/NanoAODv9_12.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212731/0000/NanoAODv9_13.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212731/0000/NanoAODv9_14.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_EN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212731/0000/NanoAODv9_15.root"
    ],
    "VLL_NN_M-600GeV": [
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_NN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212756/0000/NanoAODv9_11.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_NN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212756/0000/NanoAODv9_12.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_NN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212756/0000/NanoAODv9_13.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_NN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212756/0000/NanoAODv9_14.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_NN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212756/0000/NanoAODv9_15.root",
        "/hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m600GeV_NN_to_4b/RunIISummer20UL18_NanoAODv9/230718_212756/0000/NanoAODv9_16.root"
    ]
}

filesetSignalv9 = {
    "VLL_NN_M-1000GeV": [
          "/hdfs/store/mc/RunIISummer20UL18NanoAODv9/VLferm_m800GeV_EE_to_4b_TuneCP5_13TeV-madgraph-pythia8/NANOAODSIM/106X_upgrade2018_realistic_v16_L1v1-v2/70000/86587366-BF96-C140-A0E5-F56D9D2F4854.root"
	]
}

data2018 = {
    "Data_SingleEle_runA_2018": [
        redirector+"/store/data/Run2018A/EGamma/NANOAOD/UL2018_MiniAODv2_NanoAODv9-v1/270000/8F96C11E-463B-AE44-AF36-450A13152533.root", 
        redirector+"/store/data/Run2018A/EGamma/NANOAOD/UL2018_MiniAODv2_NanoAODv9-v1/270000/98A23080-A90A-ED48-8918-8588DA55088D.root", 
        redirector+"/store/data/Run2018A/EGamma/NANOAOD/UL2018_MiniAODv2_NanoAODv9-v1/270000/02713A49-3272-B84F-A4CC-F0E924FCAB90.root", 
        redirector+"/store/data/Run2018A/EGamma/NANOAOD/UL2018_MiniAODv2_NanoAODv9-v1/270000/B113CF33-8FFB-9942-8FFA-37D9AC036CEC.root", 
        redirector+"/store/data/Run2018A/EGamma/NANOAOD/UL2018_MiniAODv2_NanoAODv9-v1/270000/1A9DC7C6-1C51-3A48-A8BE-45105FD7B499.root"
	]
}

data2017 = {
    "Data_SingleEle_runE_2017": [
        redirector+"/store/data/Run2017E/SingleElectron/NANOAOD/UL2017_MiniAODv2_NanoAODv9-v1/120000/9C426B76-FFDF-394D-82AA-7237B7985622.root", 
        redirector+"/store/data/Run2017E/SingleElectron/NANOAOD/UL2017_MiniAODv2_NanoAODv9-v1/120000/C8935834-CD14-6446-8321-2EE550B3170A.root", 
        redirector+"/store/data/Run2017E/SingleElectron/NANOAOD/UL2017_MiniAODv2_NanoAODv9-v1/120000/71076304-A300-3A44-8B5A-0C378F4A9C2B.root", 
        redirector+"/store/data/Run2017E/SingleElectron/NANOAOD/UL2017_MiniAODv2_NanoAODv9-v1/120000/2CA5E928-B7F5-B547-898F-D1498D76E7F8.root", 
        redirector+"/store/data/Run2017E/SingleElectron/NANOAOD/UL2017_MiniAODv2_NanoAODv9-v1/120000/4C327E03-05F1-5E48-9F16-D17FB6413E97.root"
    ]
}

data2016post = {
    "Data_SingleEle_runH_2016": [
        redirector+"/store/data/Run2016H/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/280000/CB7BF1F8-DBBD-874D-BC11-D0F7871CAF66.root", 
        redirector+"/store/data/Run2016H/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/280000/3998437C-F335-9D49-A58A-D77FAAA88A41.root", 
        redirector+"/store/data/Run2016H/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/280000/5416D54B-F2A9-7042-8107-2FFE8609DC36.root", 
        redirector+"/store/data/Run2016H/SingleElectron/NANOAOD/UL2016_MiniAODv2_NanoAODv9-v1/280000/6577E09B-FFE6-B44A-B89C-8ABE6DC5245E.root"
    ]
}

data2016pre = {
    "Data_SingleEle_runF-HIMP_2016": [
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/HIPM_UL2016_MiniAODv2_NanoAODv9-v2/2520000/EADCA4F7-447A-5746-A34C-6CECCE724BF8.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/HIPM_UL2016_MiniAODv2_NanoAODv9-v2/40000/0E71FDFE-7263-3A44-BC24-0A201F098E8A.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/HIPM_UL2016_MiniAODv2_NanoAODv9-v2/40000/0F1DBB7A-8D75-0C41-B8FB-181F8553AB26.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/HIPM_UL2016_MiniAODv2_NanoAODv9-v2/40000/138A00F0-5691-A34D-94F0-21CF3D490CD7.root",
        redirector+"/store/data/Run2016F/SingleElectron/NANOAOD/HIPM_UL2016_MiniAODv2_NanoAODv9-v2/40000/1762FD0A-7657-6843-BA5A-3B28BFD3FBAF.root"
    ]
}
