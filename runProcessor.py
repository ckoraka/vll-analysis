from coffea import util, processor
from coffea.nanoevents import NanoEventsFactory, NanoAODSchema
from vll.processor import VLLProcessor
from vll.utils.crossSections import lumis, crossSections, nevents
import awkward as ak
import hist
import time
import time
import sys
import os
import argparse
import uproot
from filelists.fileset import *

uproot.open.defaults["xrootd_handler"] = uproot.source.xrootd.MultithreadedXRootDSource

redirector = "root://xrootd-cms.infn.it//"


# Initialize these :
#---
era = "2018"
listOfFiles = filesetSignal #filesetTTbarPowheg_Dilepton2018# filesetTTbarPowheg_Dilepton #filesetSignal 
isMC = True
#---

tstart = time.time()
outfile = "test.coffea"
print('Will create : ',outfile)

output = processor.run_uproot_job(
    listOfFiles,
    "Events",
    VLLProcessor(isMC=isMC,era=era),
    processor.futures_executor, # iterative_executor, futures_executor : Run "multithreaded"
    executor_args={'schema': NanoAODSchema,'workers': 4,'skipbadfiles': True,'xrootdtimeout':300},
    chunksize=10000,
    maxchunks=None,
)


elapsed = time.time() - tstart
print("Total time: %.1f seconds"%elapsed)
util.save(output, outfile)
print(f"Saved output to {outfile}")
