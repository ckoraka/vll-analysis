#!/bin/bash

INPUT_CONFIG=$1

# Compile executable
rm TRF_truth
echo ">>> Compile TRF executable ..."
COMPILER=$(root-config --cxx)
FLAGS=$(root-config --cflags --libs)
time $COMPILER -Iinclude -g -O3 -Wall -Wextra -o TRF_truth TRF_truth.C $FLAGS 

while IFS=, read -r InputFile MAP WP topDir OutputFile
do
    if [ ! -d ${topDir} ] 
    then
        mkdir -p ${topDir}
    fi

    echo ">>> Start"
    echo ">>> Calculate TRF weights for sample :: ${InputFile}"
    echo ">>> Use tagging probability maps from :: ${MAP} and for WP : ${WP}"
    echo ">>> Closure Plots are saved in directory  : ${topDir}"
    echo ">>> Created output .root file in path  : ${OutputFile}"
    ./TRF_truth $InputFile $MAP $WP $topDir $OutputFile
    echo ">>> End"                                           
    echo "  "                                           
done < ${INPUT_CONFIG}


