#!/bin/bash

CONF_FILE=$1

# Compile executable
rm TaggingProb_Map
echo ">>> Compile b-tagging probability map creation executable ..."
COMPILER=$(root-config --cxx)
FLAGS=$(root-config --cflags --libs)
time $COMPILER -g -O3 -Wall -Wextra  -o TaggingProb_Map TaggingProb_Map.C $FLAGS

while IFS=, read -r InputFile OutputFile WP
do
    echo ">>> WP of DeepJET tagger used :: ${WP}"
    echo ">>> Create tagging probability Maps for sample:: ${InputFile}"
    echo ">>> Create tagging probability Maps :: ${OutputFile}"
    ./TaggingProb_Map ${InputFile} ${OutputFile} ${WP}
done < ${CONF_FILE}

