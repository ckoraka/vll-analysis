#!/usr/bin/env python3
import hist
import hist.intervals
import matplotlib.pyplot as plt
from coffea import util
from coffea.processor import accumulate
import numpy as np
import uproot
import os

from vll.utils.plotting import RebinHist, SetRangeHist, GroupBy, rebin_hist
from vll.utils.crossSections import lumis, crossSections, nevents, neventsSkimmed, nentries

storePerBin = True

sl = hist.tag.Slicer()

def load_data_from_config(config_file):
    with open(config_file, "r") as f:
        config = json.load(f)
    
    input_dir =  config.get("input_dir","/afs/hep.wisc.edu/home/ckoraka/VLL-analysis/coffea-FW/vll-analysis/Outputs")
    era = config.get("era", "2018")
    version = config.get("version", "v2")
    outdir = config.get("outdir", "RootFiles")
    mc_files = config.get("mc_files", [])
    data_files = config.get("data_files", [])

    # Accumulate MC output
    outputMC = accumulate([util.load(os.path.join(input_dir, version, era, mc_file)) for mc_file in mc_files])

    # Accumulate Data output
    outputData = accumulate([util.load(os.path.join(input_dir, version, era, data_file)) for data_file in data_files])

    return outputMC, outputData, outdir, era, version


def main(config_file):

    import hist

    outputMC, outputData, outdir, era, version = load_data_from_config(config_file)

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    #######################
    # Grouping
    #######################

    groupingCategory = {
        "CR": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b'],
        "SR": ['jetSel_4j4b','jetSel_5j4b','jetSel_6j4b'],
    }

    groupingMCDatasets = {
        "other": [ "TTHH","TTWW","TTZZ","TTZH","TTWZ","TTWH","WW","ZZ","WZ","WWW","ZZZ","WWZ","WZZ",],
        "tttt": [ "TTTT", ],
        "ttV" : [ "TTW","TTZ",],
        "ttH" : [ "TTHToNonBB","TTHToBB",],
        "singlet-top": [ "ST_tW_channel","ST_tbarW_channel","ST_s_channel","ST_t-channel_antiT","ST_t-channel_T",],
        "dy": ["DYjetsM50","DYjetsM10to50"],
        "ttbar": [ "TTbarPowheg_Dilepton",],
        "VLL_M-500GeV": ["VLL_EE_M-500GeV","VLL_EN_M-500GeV","VLL_NN_M-500GeV",],
        "VLL_M-550GeV": ["VLL_EE_M-550GeV","VLL_EN_M-550GeV","VLL_NN_M-550GeV",],
        "VLL_M-600GeV": ["VLL_EE_M-600GeV","VLL_EN_M-600GeV","VLL_NN_M-600GeV",],
        "VLL_M-650GeV": ["VLL_EE_M-650GeV","VLL_EN_M-650GeV","VLL_NN_M-650GeV",],
        "VLL_M-700GeV": ["VLL_EE_M-700GeV","VLL_EN_M-700GeV","VLL_NN_M-700GeV",],
        "VLL_M-750GeV": ["VLL_EE_M-750GeV","VLL_EN_M-750GeV","VLL_NN_M-750GeV",],
        "VLL_M-800GeV": ["VLL_EE_M-800GeV","VLL_EN_M-800GeV","VLL_NN_M-800GeV",],
        "VLL_M-850GeV": ["VLL_EE_M-850GeV","VLL_EN_M-850GeV","VLL_NN_M-850GeV",],
        "VLL_M-900GeV": ["VLL_EE_M-900GeV","VLL_EN_M-900GeV","VLL_NN_M-900GeV",],
        "VLL_M-950GeV": ["VLL_EE_M-950GeV","VLL_EN_M-950GeV","VLL_NN_M-950GeV",],
        "VLL_M-1000GeV": ["VLL_EE_M-1000GeV","VLL_EN_M-1000GeV","VLL_NN_M-1000GeV",],
    }

    ##############################################
    # Scale MC histograms with xsection and lumi
    ##############################################

    for samp, sampList in groupingMCDatasets.items():
        for s in sampList:
            print('Compare: ',s,'   ', nevents[era][s],'   ',outputMC[s]['EventCount'])
            event_count = (outputMC[s]['EventCount'].value / nentries[era][s]) * neventsSkimmed[era][s]
            #event_count = outputMC[s]['EventCount'].value
            lumiSF = crossSections[s] * lumis[era] / event_count
            if 'VLL' in s:
                lumiSF = 0.02 * lumis[era] / event_count
            for key, histo in outputMC[s].items():
                if isinstance(histo, hist.Hist):
                    histo *= lumiSF

    # Group MC histograms
    histList = []
    for samp, sampList in groupingMCDatasets.items():
        histList += [outputMC[s] for s in sampList]

    outputMCHist = accumulate(histList)
    for key, histo in outputMCHist.items():
        if isinstance(histo, hist.Hist):
            outputMCHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingMCDatasets)

    # Group data histograms
    outputDataHist = accumulate([histo for key, histo in outputData.items()])

    var = 'dnn_vll'
    ax = 'dnn'

    # var = 'event_ST'
    # ax = 'event_HT'

    h = outputMCHist[var]
    h = h[{'lepFlavor':sum,'dnnReg':sum,'HTregion':'HT_high'}]
    h = GroupBy(h, "bJetMultiplicity", "bJetMultiplicity", groupingCategory)

    # new_bins = np.array([
    #     h.axes[ax].edges[30],
    #     h.axes[ax].edges[35],
    #     h.axes[ax].edges[40],
    #     h.axes[ax].edges[45],
    #     h.axes[ax].edges[50],
    #     h.axes[ax].edges[55],
    #     h.axes[ax].edges[60],
    #     h.axes[ax].edges[65],
    #     h.axes[ax].edges[70],
    #     h.axes[ax].edges[77],
    #     h.axes[ax].edges[85],
    #     h.axes[ax].edges[95],
    #     h.axes[ax].edges[107],
    #     h.axes[ax].edges[140],
    #     h.axes[ax].edges[300],
    # ])  

    # new_bins2 = np.array([
    #     h.axes[ax].edges[30],
    #     h.axes[ax].edges[40],
    #     h.axes[ax].edges[50],
    #     h.axes[ax].edges[60],
    #     h.axes[ax].edges[70],
    #     h.axes[ax].edges[80],
    #     h.axes[ax].edges[90],
    #     h.axes[ax].edges[100],
    #     h.axes[ax].edges[110],
    #     h.axes[ax].edges[130],
    #     h.axes[ax].edges[150],
    #     h.axes[ax].edges[180],
    #     h.axes[ax].edges[200],
    #     h.axes[ax].edges[230],
    #     h.axes[ax].edges[260],
    #     h.axes[ax].edges[300],
    # ])  

    # new_bins4 = np.array([
    #     h.axes[ax].edges[30],
    #     h.axes[ax].edges[45],
    #     h.axes[ax].edges[55],
    #     h.axes[ax].edges[65],
    #     h.axes[ax].edges[75],
    #     h.axes[ax].edges[85],
    #     h.axes[ax].edges[95],
    #     h.axes[ax].edges[105],
    #     h.axes[ax].edges[120],
    #     h.axes[ax].edges[140],
    #     h.axes[ax].edges[170],
    #     h.axes[ax].edges[300],
    # ]) 

    new_bins4 = np.array([
        h.axes[ax].edges[0],
        h.axes[ax].edges[10],
        h.axes[ax].edges[20],
        h.axes[ax].edges[30],
        h.axes[ax].edges[40],
        h.axes[ax].edges[50],
        h.axes[ax].edges[60],
        h.axes[ax].edges[70],
        h.axes[ax].edges[80],
        h.axes[ax].edges[85],
        h.axes[ax].edges[90],
        h.axes[ax].edges[93],
        h.axes[ax].edges[96],
        h.axes[ax].edges[98],
        h.axes[ax].edges[100],
    ])  

    h = rebin_hist(h, ax, new_bins4)

    # Data histogram 
    hData = outputDataHist[var]
    hData = GroupBy(hData, "bJetMultiplicity", "bJetMultiplicity", groupingCategory)
    hData = hData[{'lepFlavor':sum,'dnnReg':sum,'HTregion':'HT_high','systematic':sum,'dataset':sum}]
    hData = rebin_hist(hData, ax, new_bins4)



    if not os.path.exists(outdir):
        os.makedirs(outdir)

    systematics = [s for s in h.axes["systematic"] if not s.startswith("Q2Scale")]
    if(storePerBin):
        for i in range(len(new_bins4) - 1):
            for _region in ["SR","CR"]:
                outputFile = uproot.recreate(os.path.join(outdir, "VLL-2l_ST_bin_"+str(i)+"_"+ _region+"_region.root"))
                hist_data = hData[{'bJetMultiplicity':_region}]
                hist_data = hist_data[i:i+1,]
                print(hist_data)
                outputFile["data_obs"] = hist_data
                for _systematic in systematics:
                    for _dataset in groupingMCDatasets:
                        histname = f"{_dataset}"
                        hist = h[{'dataset':_dataset,'bJetMultiplicity':_region,'systematic':_systematic}]
                        # if('btagVariation_hfUp' in _systematic and 'ttbar' in _dataset and _region=='CR'):
                        #     print('systematic: ',_systematic,'  scale factor: ',scaling_factor)
                        hist = hist[i:i+1,] 
                        if _systematic  == 'nominal': 
                            outputFile[histname] = hist
                        else:                  
                            outputFile[_systematic+"/"+histname] = hist

                outputFile.close()
    else:
        for _region in ["SR","CR"]:
            outputFile = uproot.recreate(os.path.join(outdir, "VLL-2l_ST_"+ _region+"_region.root"))
            hist_data = hData[{'bJetMultiplicity':_region}]
            outputFile["data_obs"] = hist_data
            for _systematic in systematics:
                for _dataset in groupingMCDatasets:
                    histname = f"{_dataset}"
                    hist = h[{'dataset':_dataset,'bJetMultiplicity':_region,'systematic':_systematic}]
                    if _systematic  == 'nominal': 
                        outputFile[histname] = hist
                    else:                  
                        outputFile[_systematic+"/"+histname] = hist
            outputFile.close()


if __name__ == "__main__":

    import argparse
    import json

    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--config_file", type=str, help="Path to the config file")

    args = parser.parse_args()

    main(args.config_file)
