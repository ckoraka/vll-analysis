import numpy as np

import matplotlib.pyplot as plt
from cycler import cycler
from coffea import util
from coffea.processor import accumulate
import hist
import hist.intervals
import os

import mplhep as hep

from vll.utils.plotting import plotWithRatio, RebinHist, SetRangeHist, GroupBy
from vll.utils.crossSections import lumis, crossSections, nevents, neventsSkimmed, nentries, sum_genW

hep.style.use("CMS")

def calculate_bin_ratio_squared_sum(numerator_hist, denominator_hist):
    assert len(numerator_hist) == len(denominator_hist), "Histograms must have the same number of bins"
    ratio_squared = ((numerator_hist - denominator_hist) ** 2) / denominator_hist
    ratio_squared = np.nan_to_num(ratio_squared, nan=0, posinf=0, neginf=0)
    ratio_squared_sum = np.nansum(ratio_squared)
    result = np.sqrt(ratio_squared_sum)
    return result

def load_data_from_config(config_file):
    with open(config_file, "r") as f:
        config = json.load(f)
    
    input_dir = config.get("input_dir")
    outdir = config.get("outdir")
    version = config.get("version")
    era = config.get("era")
    background_mc_files = config.get("background_mc_files", {})
    outputMC = accumulate([
        util.load(os.path.join(input_dir, version, era, mc_file)) for mc_file in background_mc_files.values()
    ])
    
    signal_file = config.get("signal_file")
    outputSignal = accumulate([
        util.load(os.path.join(input_dir, version, era, signal_file)),
    ])

    data_files = config.get("data_files", [])
    outputData = accumulate([
        util.load(os.path.join(input_dir, version, era, data_file)) for data_file in data_files
    ])

    return outputMC, outputSignal, outputData, input_dir, outdir, version, era

def calculate_bin_ratio_squared_sum(numerator_hist, denominator_hist):
    assert len(numerator_hist) == len(denominator_hist), "Histograms must have the same number of bins"
    ratio_squared = (numerator_hist ** 2) / denominator_hist
    ratio_squared_sum = np.sum(ratio_squared)
    result = np.sqrt(ratio_squared_sum)
    return result

def main(config_file):

    outputMC, outputSignal, outputData, input_dir, outdir, version, era = load_data_from_config(config_file)

    luminosity = lumis[era] / 1000.

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    #######################
    # Grouping
    #######################
    groupingBackground = {
        "Triboson": ["WWW","ZZZ","WWZ","WZZ",],
        "Diboson": ["WW","ZZ","WZ", ], 
        "ttVV": [ "TTHH","TTWW","TTZZ","TTZH","TTWZ","TTWH", ],
        "4top": [ "TTTT", ],
        "ttV" : [ "TTW","TTZ",],
        "ttH" : [ "TTHToNonBB","TTHToBB",],
        "Single-top": [ "ST_tW_channel","ST_tbarW_channel","ST_s_channel","ST_t-channel_antiT","ST_t-channel_T",],
        "DY": ["DYjetsM50","DYjetsM10to50"],
        "ttbar": [ "TTbarPowheg_Dilepton",],
    }

    groupingSignalMassPoint = {
        "VLL_M-500GeV": ["VLL_EE_M-500GeV","VLL_EN_M-500GeV","VLL_NN_M-500GeV",],
        "VLL_M-550GeV": ["VLL_EE_M-550GeV","VLL_EN_M-550GeV","VLL_NN_M-550GeV",],
        "VLL_M-600GeV": ["VLL_EE_M-600GeV","VLL_EN_M-600GeV","VLL_NN_M-600GeV",],
        "VLL_M-650GeV": ["VLL_EE_M-650GeV","VLL_EN_M-650GeV","VLL_NN_M-650GeV",],
        "VLL_M-700GeV": ["VLL_EE_M-700GeV","VLL_EN_M-700GeV","VLL_NN_M-700GeV",],
        "VLL_M-750GeV": ["VLL_EE_M-750GeV","VLL_EN_M-750GeV","VLL_NN_M-750GeV",],
        "VLL_M-800GeV": ["VLL_EE_M-800GeV","VLL_EN_M-800GeV","VLL_NN_M-800GeV",],
        "VLL_M-850GeV": ["VLL_EE_M-850GeV","VLL_EN_M-850GeV","VLL_NN_M-850GeV",],
        "VLL_M-900GeV": ["VLL_EE_M-900GeV","VLL_EN_M-900GeV","VLL_NN_M-900GeV",],
        "VLL_M-950GeV": ["VLL_EE_M-950GeV","VLL_EN_M-950GeV","VLL_NN_M-950GeV",],
        "VLL_M-1000GeV": ["VLL_EE_M-1000GeV","VLL_EN_M-1000GeV","VLL_NN_M-1000GeV",],
    }

    groupingCategory = {
         "CR": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b', 'jetSel_4j3b', 'jetSel_5j3b','jetSel_6j3b'],
         "SR": ['jetSel_4j4b','jetSel_5j4b','jetSel_6j4b'],
    }

    groupingCategory2 = {
        "CR1": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b',],
        "CR2": ['jetSel_4j3b', 'jetSel_5j3b', 'jetSel_6j3b',],
        "SR":  ['jetSel_4j4b', 'jetSel_5j4b', 'jetSel_6j4b',], 
    }

    groupingCategory3 = {
        "CR1": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b',],
        "CR2": ['jetSel_4j3b', 'jetSel_5j3b', 'jetSel_6j3b',],
        "CR3": ['jetSel_5j2b', 'jetSel_6j2b',],
        "CR4": ['jetSel_5j3b', 'jetSel_6j3b',],
        "CR5": ['jetSel_6j2b',],
        "CR6": ['jetSel_6j3b',],
        "SR":  ['jetSel_4j4b', 'jetSel_5j4b', 'jetSel_6j4b',], 
        "SR1": ['jetSel_5j4b',], 
        "SR2": ['jetSel_6j4b',], 
    }

    if era=="2018":
       groupingData = {
           "SingleElectron": ['Data_SingleEle_runA_2018','Data_SingleEle_runC_2018','Data_SingleEle_runB_2018','Data_SingleEle_runD_2018',],
           "SingleMuon": ['Data_SingleMu_runA_2018','Data_SingleMu_runB_2018','Data_SingleMu_runC_2018','Data_SingleMu_runD_2018',],
       }
    elif era=="2017":
        groupingData = {
            "SingleElectron": ['Data_SingleEle_runE_2017','Data_SingleEle_runD_2017','Data_SingleEle_runF_2017','Data_SingleEle_runC_2017','Data_SingleEle_runB_2017',],
            "SingleMuon": ['Data_SingleMu_runE_2017','Data_SingleMu_runD_2017','Data_SingleMu_runF_2017','Data_SingleMu_runC_2017','Data_SingleMu_runB_2017',],
        }
    elif era=="2016post":
        groupingData = {
            "SingleElectron": ['Data_SingleEle_runH_2016','Data_SingleEle_runG_2016','Data_SingleEle_runF_2016',],
            "SingleMuon": ['Data_SingleMu_runH_2016','Data_SingleMu_runG_2016','Data_SingleMu_runF_2016',],
        }
    else:        
        groupingData = {
            "SingleElectron": ['Data_SingleEle_runF-HIMP_2016','Data_SingleEle_runD_2016','Data_SingleEle_runC_2016','Data_SingleEle_runBver1_2016','Data_SingleEle_runBver2_2016','Data_SingleEle_runE_2016',],
            "SingleMuon": ['Data_SingleMu_runF-HIMP_2016','Data_SingleMu_runD_2016','Data_SingleMu_runC_2016','Data_SingleMu_runBver1_2016','Data_SingleMu_runBver2_2016','Data_SingleMu_runE_2016',],
        }

    # Plot superimposed distributions
    # axes = ['dataset', 'lepFlavor','bJetMultiplicity','dileptonCharge','systematic']

    # variableList = ["MET_pt","dnn_vll",'MET_phi','event_MT','event_ratioJetHT','event_deltaPhiJetMet','nJets','nbJets','lead_lep_pt','sublead_lep_pt','lep_mll','event_HT','event_bjet_HT','lead_jet_pT','sub_jet_pT','lead_bjet_pT','sub_bjet_pT','event_ST','dilep_dr']
    # xRanges = [xRange2,[0,1],[-3.2,3.2],xRange4,[0,1],[0,6],[0,10],[0,10],xRange2,[0,500],xRange2,[0,1500],[0,1500],xRange3,xRange2,xRange3,xRange3,xRange4,[0,5.]]
    # labels = ['MET [GeV]','DNN','MET phi','Event MT','Ratio','Dphi','N jets','N b-jets','Leading lepton $p_{T}$ [GeV]','Sub-leading lepton $p_{T}$ [GeV]','$m_{ll}$ [GeV]',' Event jet $H_{T}$ [GeV]',' Event b-jet $H_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading jet $p_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading b-jet $p_{T}$ [GeV]',' Event $S_{T}$ [GeV]','$DeltaR_{ll}$']

    # Define variable list
    variableList = ["MET_pt","dnn_vll",'MET_phi','event_MT','event_ratioJetHT','event_deltaPhiJetMet','nJets','nbJets','lead_lep_pt','sublead_lep_pt','lep_mll','event_HT','event_bjet_HT','lead_jet_pT','sub_jet_pT','lead_bjet_pT','sub_bjet_pT','event_ST','dilep_dr']

    xRange2 = [0,1000]
    xRange3 = [0,1500]
    xRanges = [
        [0,700],       # MET_pt
        [0,1],         # dnn_vll
        [-3.2, 3.2],   # MET_phi
        [0,200],       # event_MT
        [0, 1],        # event_ratioJetHT
        [0, 6],        # event_deltaPhiJetMet
        [0, 10],       # nJets
        [0, 10],       # nbJets
        xRange2,       # lead_lep_pt
        xRange2,       # sublead_lep_pt
        xRange2,       # lep_mll
        [250,1750],       # event_HT
        [250,1750],       # event_bjet_HT
        xRange2,       # lead_jet_pT
        xRange2,       # sub_jet_pT
        xRange2,       # lead_bjet_pT
        xRange2,       # sub_bjet_pT
        [250,3000],       # event_ST
        [0, 5]         # dilep_dr
    ]

    labels = [
        'MET [GeV]',
        'DNN',
        'MET phi',
        'Event MT',
        'Ratio',
        'Dphi',
        'N jets',
        'N b-jets',
        'Leading lepton $p_{T}$ [GeV]',
        'Sub-leading lepton $p_{T}$ [GeV]',
        '$m_{ll}$ [GeV]',
        'Event jet $H_{T}$ [GeV]',
        'Event b-jet $H_{T}$ [GeV]',
        'Leading jet $p_{T}$ [GeV]',
        'Sub-leading jet $p_{T}$ [GeV]',
        'Leading b-jet $p_{T}$ [GeV]',
        'Sub-leading b-jet $p_{T}$ [GeV]',
        'Event $S_{T}$ [GeV]',
        '$\Delta R_{ll}$'
    ]


    ##############################################
    # Scale MC histograms with xsection and lumi
    ##############################################

    for samp, sampList in groupingBackground.items():
        for s in sampList:
            # event_count = outputMC[s]['EventCount'].value 
            # event_count = nevents[era][s]
            # event_count = (outputMC[s]['EventCount'].value / nentries[era][s]) * neventsSkimmed[era][s]
            event_count = (outputMC[s]['EventCount'].value / nentries[era][s]) * sum_genW[era][s]
            lumiSF = crossSections[s] * lumis[era] / event_count
            print('Compare for ',s,' ', nevents[era][s],' - ',event_count,' = ',(nevents[era][s] - event_count)*100/nevents[era][s])
            for key, histo in outputMC[s].items():
                if isinstance(histo, hist.Hist):
                    histo *= lumiSF

    for samp, sampList in groupingSignalMassPoint.items():
        for s in sampList:
            # event_count = outputSignal[s]['EventCount'].value
            # event_count = nevents[era][s]
            # event_count = (outputSignal[s]['EventCount'].value / nentries[era][s]) * neventsSkimmed[era][s]
            event_count = (outputSignal[s]['EventCount'].value / nentries[era][s]) * sum_genW[era][s]
            lumiSF = crossSections[s] * lumis["2018"] /event_count
            print('Compare for ',s,' ', nevents[era][s],' - ',event_count,' = ',(nevents[era][s] - event_count)*100/nevents[era][s])
            for key, histo in outputSignal[s].items():
                if isinstance(histo, hist.Hist):
                    histo *= lumiSF

    # Cross-checking 
    for samp, sampList in groupingData.items():
        for s in sampList:
            event_count = outputData[s]['EventCount']
            print('Compare for ',s,' ', nevents[era][s],' - ',event_count,' = ',(nevents[era][s] - event_count)*100/nevents[era][s])

    #######################
    # MC histogram grouping
    #######################

    # Signal
    outputSigHist = accumulate([histo for key, histo in outputSignal.items()])

    # Background
    histList = []
    for samp, sampList in groupingBackground.items():
        histList += [outputMC[s] for s in sampList]
        
    outputMCHist = accumulate(histList)
    for key, histo in outputMCHist.items():
        if isinstance(histo, hist.Hist):
            outputMCHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingBackground)

    #########################
    # Data histogram grouping
    #########################
    #outputDataHist = accumulate([histo for key, histo in outputData.items()])
    histList = []
    for samp, sampList in groupingData.items():
        histList += [outputData[s] for s in sampList]

    outputDataHist = accumulate(histList)
    for key, histo in outputDataHist.items():
        if isinstance(histo, hist.Hist):
            outputDataHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingData)

    #######################
    # Data vs MC histograms
    #######################

    # Inclusive
    if(True):
        for reg in  ['CR1','CR2','SR']:
            o_dir = outdir+'/DataMC'+reg
            if not os.path.exists(o_dir):
                os.makedirs(o_dir)
            for htreg in  ['HT_low','HT_high']:
                for var, rg, lb in zip(variableList,xRanges,labels):
                    # Signal
                    hs = outputSigHist[var][{'lepFlavor':sum}]
                    hs = hs[{'systematic':'nominal'}]
                    hs = GroupBy(hs, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                    hs = hs[{'bJetMultiplicity':reg}]
                    hs = hs[{'HTregion':htreg}]
                    hs = hs[{'dnnReg':sum}]

                    # Background
                    h = outputMCHist[var][{'lepFlavor':sum}]
                    h = h[{'systematic':'nominal'}]
                    h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                    h = h[{'bJetMultiplicity':reg}]
                    h = h[{'HTregion':htreg}]
                    h = h[{'dnnReg':sum}]

                    # Data
                    hData = outputDataHist[var][{'lepFlavor':sum,'systematic':sum,'dataset':sum}]
                    hData = GroupBy(hData, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                    hData = hData[{'bJetMultiplicity':reg}]
                    hData = hData[{'HTregion':htreg}]
                    hData = hData[{'dnnReg':sum}]

                    # Plot
                    plotWithRatio(h[:,::4j], hData[::4j],overlay='dataset', stacked=True, invertStack=True, xRange=rg, yRange=[1.,None], logY=True, leg='upper right',binwnorm=None, xlabel=lb, lumi=luminosity, era=era)
                    plt.savefig(o_dir+'/'+var+'_'+htreg+'.png')
                    plt.close()

    # ee/emu/mumu
    reg = 'CR1'                
    if(True):
        o_dir = outdir+'/DataMC_SplitFlavor'
        if not os.path.exists(o_dir):
            os.makedirs(o_dir)

        for var, rg, lb in zip(variableList,xRanges,labels):
            for flav in ['ee','emu','mumu']:
                # Signal
                hs = outputSigHist[var][{'lepFlavor':flav}]
                hs = hs[{'systematic':'nominal'}]
                hs = GroupBy(hs, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                hs = hs[{'bJetMultiplicity':reg}]
                hs = hs[{'HTregion':sum}]
                hs = hs[{'dnnReg':sum}]

                # Background
                h = outputMCHist[var][{'lepFlavor':flav}]
                h = h[{'systematic':'nominal'}]
                h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                h = h[{'bJetMultiplicity':reg}]
                h = h[{'HTregion':sum}]
                h = h[{'dnnReg':sum}]

                # Data
                hData = outputDataHist[var][{'lepFlavor':flav,'systematic':sum,'dataset':sum}]
                hData = GroupBy(hData, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                hData = hData[{'bJetMultiplicity':reg}]
                hData = hData[{'HTregion':sum}]
                hData = hData[{'dnnReg':sum}]


                # Plot
                plotWithRatio(h, hData, overlay='dataset', stacked=True, xRange=rg, yRange=[1.,None], logY=True, leg='upper right',binwnorm=None, xlabel=lb, lumi=luminosity)
                plt.savefig(o_dir+'/'+var+'_'+flav+'.png')
                plt.close()


    #############################
    # Print Signal distributions
    #############################

    if(True):
        o_dir = outdir+'/SignalMassPoints'
        if not os.path.exists(o_dir):
            os.makedirs(o_dir)
        for region in ['CR1','CR2','SR']:
            hs = outputSigHist['dnn_vll'][{'lepFlavor':sum}]
            hs = GroupBy(hs, 'dataset', 'dataset', groupingSignalMassPoint)
            hs = GroupBy(hs,'bJetMultiplicity', 'bJetMultiplicity',groupingCategory2)
            hs = hs[{'systematic':'nominal'}]
            hs = hs[{'bJetMultiplicity':region}]
            hs = hs[{'HTregion': sum}]
            hs = hs[{'dnnReg': sum}]
            hs = 1000*hs
            hh = hs[:,::4j]
            plt.figure(figsize=(24,6))
            hh.plot1d(overlay='dataset',linewidth=3)
            plt.legend(title="Signal x 1000", fontsize=8, title_fontsize=12)
            plt.savefig(o_dir+'/Signal_comparison_'+str(region)+'.png')
            plt.close()


    #############################
    # Print Event yields
    #############################
    if(True):
        #for region in ['CR1','CR2','CR3','CR4','CR5','CR6','SR','SR1','SR2']:
        for region in ['CR1','CR2','SR']:
            print('Region:',region)
            for samp, sampList in groupingBackground.items():
                for HTregion in ['HT_low','HT_high']:
                    print(HTregion)
                    h = outputMCHist['sub_bjet_pT'][{'lepFlavor':sum}][{'systematic':'nominal'}]
                    h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                    h = h[{'bJetMultiplicity':region}]
                    h = h[{'HTregion':HTregion}]
                    h = h[{'dnnReg':sum}]
                    print('Number of events for dataset : ', samp, '  and channel : ',HTregion,' is ',  h[{'dataset':samp, 'pt':sum}])

        #for region in ['CR1','CR2','CR3','CR4','CR5','CR6','SR','SR1','SR2']:
        for region in ['CR1','CR2','SR']:
            print('Region:',region)
            for samp, sampList in groupingSignalMassPoint.items():
                for HTregion in ['HT_low','HT_high']:
                    print(HTregion)
                    hs = outputSigHist['sub_bjet_pT'][{'lepFlavor':sum}][{'systematic':'nominal'}]
                    hs = GroupBy(hs, 'dataset', 'dataset', groupingSignalMassPoint)
                    hs = GroupBy(hs, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory2)
                    hs = hs[{'bJetMultiplicity':region}]
                    hs = hs[{'HTregion':HTregion}]
                    hs = hs[{'dnnReg':sum}]
                    print('Number of events for dataset : ', samp, '  and channel : ',HTregion,' is ',  hs[{'dataset':samp, 'pt':sum}])


    # For debugging
    # systematics = [s for s in outputMCHist['event_ST'].axes["systematic"]]
    # print(systematics)
    # fig, ax = plt.subplots(figsize=(10,10))
    # outputMCHist['event_ST'][{'lepFlavor':sum}][{'dataset':'ttbar'}][{'bJetMultiplicity':'jetSel_4j2b'}][{'HTregion': sum}][{'dnnReg': sum}].plot1d(overlay='systematic')
    # plt.legend()
    # plt.savefig('syst.png')
    # plt.close()


    #######################
    # CutFlow
    #######################
    hData = outputDataHist['CutFlow'][{'dataset':sum}]
    h = outputMCHist['CutFlow']
    plotWithRatio(h, hData, overlay='dataset', yRange=[1.,None], logY=True, leg='upper right',binwnorm=None, xlabel="Cutflow",lumi=luminosity,era=era)
    plt.savefig(outdir+'/Cutflow.png')
    plt.close()

    #######################
    # Signal vs Background
    #######################

    if(True):
        samp = 'VLL_M-600GeV'
        o_dir = outdir+'/SvsB'
        if not os.path.exists(o_dir):
            os.makedirs(o_dir)

        for var, rg, lb in zip(variableList,xRanges,labels):
            for HTregion in ['HT_low','HT_high']:

                # Signal
                hs = outputSigHist[var][{'lepFlavor':sum}]
                hs = GroupBy(hs, 'dataset', 'dataset', groupingSignalMassPoint)
                hs = hs[{'systematic':'nominal'}]
                hs = GroupBy(hs, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
                hs = hs[{'bJetMultiplicity':'SR'}]
                hs = hs[{'HTregion':HTregion}]
                hs = hs[{'dnnReg':sum}]
                hs = hs[{'dataset':'VLL_M-600GeV'}]
                # Background
                h = outputMCHist[var][{'lepFlavor':sum}]
                h = h[{'systematic':'nominal'}]
                h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
                h = h[{'bJetMultiplicity':'SR'}]
                h = h[{'dnnReg':sum}]
                h = h[{'HTregion':HTregion}]

                h_comb = h[{'dataset':sum}]
                h_comb = h_comb+hs

                hb = h[{'dataset':sum}]*1000
                hs = hs*1000

                hb[::5j].plot1d(density=True,label='Background',)
                hs[::5j].plot1d(density=True,label='Signal',)
                plt.legend(title='S vs B:'+samp+'_'+HTregion, fontsize=8, title_fontsize=9, loc='upper right')
                plt.savefig(o_dir+'/'+var+'_SvsB_Norm_'+HTregion+'.png')  
                plt.close()

                if var=='dnn_vll':
                    plotWithRatio(h=h[:,::4j], hData=h_comb[::4j], overlay='dataset', stacked=True, density=False, leg='upper right',binwnorm=None, xlabel=lb, lumi=luminosity, xRange=rg)
                    plt.savefig(o_dir+'/'+var+'_SvsB_'+HTregion+'.png')  
                    plt.close()

                if var=='event_ST':
                    plotWithRatio(h=h[:,::4j], hData=h_comb[::4j], overlay='dataset', stacked=True, density=False, leg='upper right',binwnorm=None, xlabel=lb, lumi=luminosity, xRange=rg)
                    plt.savefig(o_dir+'/'+var+'_SvsB_'+HTregion+'.png')  
                    plt.close()



if __name__ == "__main__":
    import argparse
    import json

    parser = argparse.ArgumentParser(description="Process some integers.")
    parser.add_argument("--config_file", type=str, help="Path to the config file")

    args = parser.parse_args()

    main(args.config_file)
