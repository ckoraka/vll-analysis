#!/bin/bash

file=$1
if [ -f "$file" ] ; then
    rm "$file"
fi

echo "filesetSignal = {" >> $1

for mass in {500..1000..50}
do
    for process in {EE,EN,NN}
    do
        echo "\"VLL_"${process}"_M-"${mass}"GeV\": [" >> $1
        ls /hdfs/store/user/ckoraka/VLferm_EWcouplings_4321_m${mass}GeV_${process}_to_4b/RunIISummer20UL18_NanoAODv9/*/*/*.root >> $1
        echo "]," >> $1
    done
done

echo "}" >> $1

var1=/hdfs
var2=\"/hdfs

sed -i "s|$var1|$var2|g" $1

var1=.root
var2=\.root\"\,

sed -i "s|$var1|$var2|g" $1

# Remove last comma 
#var1='.root", /\n ],'
#var2='.root" /\n ],'
#echo $var1
#echo $var2
#sed -i "s|$var1|$var2|g" $1
