import numpy as np

import matplotlib.pyplot as plt
from cycler import cycler
from coffea import util
from coffea.processor import accumulate
import hist
import hist.intervals
import uproot
import os

import mplhep as hep

from vll.utils.plotting import plotWithRatio, RebinHist, SetRangeHist, GroupBy
from vll.utils.crossSections import lumis, crossSections, nevents

saveHists = True
plotComp = True
version = 'v2'

era = "2018"
luminosity = lumis[era]/1000.

outdir = "Plots_compare"
if not os.path.exists(outdir):
    os.makedirs(outdir)


# Background MC accumulator 
outputMC = accumulate(
    [
        #util.load('Outputs/output_MCTTbar2l_run20240222_094156.coffea'),
        util.load('Outputs/output_MCTTbar2l_run20240404_105614.coffea'),
    ]
)

# Signal MC accumulator 
outputSignal = accumulate(
    [
		#util.load('Outputs/'+version+'/output_Signal_run20231006_053626.coffea'),
		util.load('Outputs/output_Signal_run20240410_212352.coffea'),

    ]
)


#######################
# Grouping
#######################
groupingBackground = {
    "ttbar": [ "TTbarPowheg_Dilepton",],
}

groupingSignalMassPoint = {
    #"VLL_M-500GeV": ["VLL_EE_M-500GeV","VLL_EN_M-500GeV","VLL_NN_M-500GeV",],
    #"VLL_M-550GeV": ["VLL_EE_M-550GeV","VLL_EN_M-550GeV","VLL_NN_M-550GeV",],
    "VLL_M-600GeV": ["VLL_EE_M-600GeV","VLL_EN_M-600GeV","VLL_NN_M-600GeV",],
    #"VLL_M-650GeV": ["VLL_EE_M-650GeV","VLL_EN_M-650GeV","VLL_NN_M-650GeV",],
    #"VLL_M-700GeV": ["VLL_EE_M-700GeV","VLL_EN_M-700GeV","VLL_NN_M-700GeV",],
    #"VLL_M-750GeV": ["VLL_EE_M-750GeV","VLL_EN_M-750GeV","VLL_NN_M-750GeV",],
    #"VLL_M-800GeV": ["VLL_EE_M-800GeV","VLL_EN_M-800GeV","VLL_NN_M-800GeV",],
    #"VLL_M-850GeV": ["VLL_EE_M-850GeV","VLL_EN_M-850GeV","VLL_NN_M-850GeV",],
    #"VLL_M-900GeV": ["VLL_EE_M-900GeV","VLL_EN_M-900GeV","VLL_NN_M-900GeV",],
    #"VLL_M-950GeV": ["VLL_EE_M-950GeV","VLL_EN_M-950GeV","VLL_NN_M-950GeV",],
    #"VLL_M-1000GeV": ["VLL_EE_M-1000GeV","VLL_EN_M-1000GeV","VLL_NN_M-1000GeV",],
}

groupingCategory = {
    "CR": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b', ],#'jetSel_4j3b', 'jetSel_5j3b','jetSel_6j3b'],
    "SR": ['jetSel_4j4b','jetSel_5j4b','jetSel_6j4b'],
}

groupingCategory2 = {
    "CR1": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b',],
    "CR2": ['jetSel_4j3b', 'jetSel_5j3b', 'jetSel_6j3b',],
    "SR":  ['jetSel_4j4b', 'jetSel_5j4b', 'jetSel_6j4b',], 
}

groupingCategory3 = {
    "CR1": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b',],
    "CR2": ['jetSel_4j3b', 'jetSel_5j3b', 'jetSel_6j3b',],
    "CR3": ['jetSel_5j2b', 'jetSel_6j2b',],
    "CR4": ['jetSel_5j3b', 'jetSel_6j3b',],
    "CR5": ['jetSel_6j2b',],
    "CR6": ['jetSel_6j3b',],
    "SR":  ['jetSel_4j4b', 'jetSel_5j4b', 'jetSel_6j4b',], 
    "SR1": ['jetSel_5j4b',], 
    "SR2": ['jetSel_6j4b',], 
}



# Plot superimposed distributions
variableList = ["MET_pt",'lead_lep_pt','sublead_lep_pt','lep_mll','event_HT','event_bjet_HT','lead_jet_pT','sub_jet_pT','lead_bjet_pT','sub_bjet_pT','event_ST','dilep_dr']
axes = ['dataset', 'lepFlavor','bJetMultiplicity','dileptonCharge','systematic']
xRange1=[0,700]
xRange2=[0,800]
xRange3=[0,1500]
xRange4=[0,2000]
xRanges = [xRange2,xRange2,[0,500],xRange2,[0,1500],[0,1500],xRange3,xRange2,xRange3,xRange3,xRange4,[0,5.]]
labels = ['MET [GeV]','Leading lepton $p_{T}$ [GeV]','Sub-leading lepton $p_{T}$ [GeV]','$m_{ll}$ [GeV]',' Event jet $H_{T}$ [GeV]',' Event b-jet $H_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading jet $p_{T}$ [GeV]','Leading b-jet $p_{T}$ [GeV]','Sub-leading b-jet $p_{T}$ [GeV]',' Event $S_{T}$ [GeV]','$DeltaR_{ll}$']

##############################################
# Scale MC histograms with xsection and lumi
##############################################

for samp, sampList in groupingBackground.items():
    for s in sampList:
        event_count = outputMC[s]['EventCount']
        if(s=='TTbarPowheg_Dilepton'):
            event_count = outputMC[s]['EventCount'].value
        lumiSF = crossSections[s] * lumis[era] / event_count
        #print('Compare for ',s,' ', nevents[era][s],' - ',event_count,' = ',(nevents[era][s] - event_count)*100/nevents[era][s])
        for key, histo in outputMC[s].items():
            if isinstance(histo, hist.Hist):
                histo *= lumiSF

for samp, sampList in groupingSignalMassPoint.items():
    for s in sampList:
        event_count = outputSignal[s]['EventCount'].value
        lumiSF = crossSections[s] * lumis["2018"] /event_count
        #print('Compare for ',s,' ', nevents[era][s],' - ',event_count,' = ',(nevents[era][s] - event_count)*100/nevents[era][s])
        for key, histo in outputSignal[s].items():
            if isinstance(histo, hist.Hist):
                histo *= lumiSF

#######################
# MC histogram grouping
#######################

# Signal
outputSigHist = accumulate([histo for key, histo in outputSignal.items()])

# Background
histList = []
for samp, sampList in groupingBackground.items():
    histList += [outputMC[s] for s in sampList]

outputMCHist = accumulate(histList)
for key, histo in outputMCHist.items():
    if isinstance(histo, hist.Hist):
        outputMCHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingBackground)


#############################
# Print Signal distributions
#############################

if(True):
    o_dir = outdir+'/SignalMassPoints'
    if not os.path.exists(o_dir):
        os.makedirs(o_dir)

    for region in ['CR','SR']:
        for leptonPair in ['OS','SS']:

            hs = outputSigHist['event_ST'][{'lepFlavor':sum}]
            hs = GroupBy(hs, 'dataset', 'dataset', groupingSignalMassPoint)
            hs = GroupBy(hs,'bJetMultiplicity', 'bJetMultiplicity',groupingCategory)
            hs = hs[{'systematic':'nominal'}]
            hs = hs[{'bJetMultiplicity':region}]
            hs = hs[{'dileptonCharge': sum}]
            hs = 1000*hs
            hh = hs[:,::8j]
            hh.plot1d(overlay='dataset')
            plt.legend(title="Signal x 1000", fontsize=8, title_fontsize=12)
            plt.savefig(o_dir+'/Signal_comparison_'+str(region)+'_'+str(leptonPair)+'.png')
            plt.close()


#############################
# Print Event yields
#############################
if(True):
    for region in ['CR','SR']:
        print('Region:',region)
        for samp, sampList in groupingBackground.items():
            for leptonPair in ['OS','SS']:
                h = outputMCHist['sub_bjet_pT'][{'lepFlavor':sum}][{'systematic':'nominal'}]
                h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
                h = h[{'bJetMultiplicity':region}]
                h = h[{'dileptonCharge':leptonPair}]
                print('Number of events for dataset : ', samp, '  and channel : ',leptonPair,' is ',  h[{'dataset':samp, 'pt':sum}])

    for region in ['CR','SR']:
        print('Region:',region)
        for samp, sampList in groupingSignalMassPoint.items():
            for leptonPair in ['OS','SS']:
                hs = outputSigHist['sub_bjet_pT'][{'lepFlavor':sum}][{'systematic':'nominal'}]
                hs = GroupBy(hs, 'dataset', 'dataset', groupingSignalMassPoint)
                hs = GroupBy(hs, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
                hs = hs[{'bJetMultiplicity':region}]
                hs = hs[{'dileptonCharge':leptonPair}]
                print('Number of events for dataset : ', samp, '  and channel : ',leptonPair,' is ',  hs[{'dataset':samp, 'pt':sum}])


#######################
# Signal vs Background
#######################

if(True):
    samp = 'VLL_M-600GeV'
    o_dir = outdir+'/SvsB'
    if not os.path.exists(o_dir):
        os.makedirs(o_dir)

    for var, rg, lb in zip(variableList,xRanges,labels):
        for leptonPair in ['OS','SS']:

            # Signal
            hs = outputSigHist[var][{'lepFlavor':sum}]
            hs = GroupBy(hs, 'dataset', 'dataset', groupingSignalMassPoint)
            hs = hs[{'systematic':'nominal'}]
            hs = GroupBy(hs, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
            hs = hs[{'bJetMultiplicity':'SR'}]
            hs = hs[{'dileptonCharge':sum}]
            hs = hs[{'dataset':'VLL_M-600GeV'}]

            # Background
            h = outputMCHist[var][{'lepFlavor':sum}]
            h = h[{'systematic':'nominal'}]
            h = GroupBy(h, 'bJetMultiplicity', 'bJetMultiplicity', groupingCategory)
            h = h[{'bJetMultiplicity':'SR'}]
            h = h[{'dileptonCharge':sum}]

            h_comb = h[{'dataset':sum}]
            h_comb = h_comb+hs

            hb = h[{'dataset':sum}]*1000
            hs = hs*1000

            hb[::5j].plot1d(density=True,label='Background',)
            hs[::5j].plot1d(density=True,label='Signal',)
            plt.legend(title='S vs B:'+samp+'_'+leptonPair, fontsize=8, title_fontsize=9, loc='upper right')
            plt.savefig(o_dir+'/'+var+'_SvsB_Norm_'+leptonPair+'.png')  
            plt.close()

            if var=='event_ST':
                # Plot comparison 
                plotWithRatio(h=h[:,::4j], hData=h_comb[::4j], overlay='dataset', stacked=True, density=False, leg='upper right',binwnorm=None, xlabel=lb, lumi=luminosity, xRange=rg)
                plt.savefig(o_dir+'/'+var+'_SvsB_'+leptonPair+'.png')  
                plt.close()

