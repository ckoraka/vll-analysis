#!/bin/bash

# Define the list of data strings
data_list=('data1' 'data2' 'data3')

# Navigate to the directory containing the script
cd /afs/hep.wisc.edu/home/ckoraka/VLL-analysis/coffea-FW/vll-analysis

# Loop through each item in the data_list
for data_item in "${data_list[@]}"; do
    # Update the line in the script
    sed -i "23s/.*/from samples.fileLists.Data_2016 import $data_item as filelistData/" runFullDataset.py

    # Create a new screen session and run the command
    screen -dmS $data_item /bin/bash -c "./shell && /usr/local/bin/python runFullDataset.py Data -e wiscjq --era '2016pre' --workers 300"

    echo "Started processing for $data_item"
done

# Detach the screen
screen -d
