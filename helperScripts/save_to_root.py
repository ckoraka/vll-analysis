#!/usr/bin/env python3

import hist
import hist.intervals
import matplotlib.pyplot as plt
from coffea import util
from coffea.processor import accumulate
import numpy as np
import uproot
import os

from vll.utils.plotting import RebinHist, SetRangeHist, GroupBy, rebin_hist
from vll.utils.crossSections import lumis, crossSections, nevents

era = "2018"
version = 'v2'

outputMC = accumulate(
    [
        util.load('Outputs/tests/output_MCTTbar2l_run20231130_110414.coffea'),
        util.load('Outputs/v2/output_Signal_run20231006_053626.coffea')
    ]
)

# Data accumulator 
outputData = accumulate(
    [
        util.load('Outputs/'+version+'/output_DataMu_run20231010_085943.coffea'),
        util.load('Outputs/'+version+'/output_DataEleA_run20231006_082754.coffea'),
        util.load('Outputs/'+version+'/output_DataEleB_20231010_131102.coffea'),
        util.load('Outputs/'+version+'/output_DataEleC_run20231005_095857.coffea'),
        util.load('Outputs/'+version+'/output_DataEleD_20231010_131102.coffea'),
    ]
)

#######################
# Grouping
#######################

groupingCategory = {
    "CR": ['jetSel_4j2b', 'jetSel_5j2b', 'jetSel_6j2b'],
    "SR": ['jetSel_4j4b','jetSel_5j4b','jetSel_6j4b'],
}

groupingMCDatasets = {
    #"other": [ "TTHH","TTWW","TTZZ","TTZH","TTWZ","TTWH","WW","ZZ","WZ","WWW","ZZZ","WWZ","WZZ",  ],
    #"tttt": [ "TTTT", ],
    #"ttV" : [ "TTW","TTZ",],
    #"ttH" : [ "TTHToNonBB","TTHToBB",],
    #"singlet-top": [ "ST_tW_channel","ST_tbarW_channel","ST_s_channel","ST_t-channel_antiT","ST_t-channel_T",],
    #"dy": ["DYjetsM50","DYjetsM10to50"],
    "ttbar": [ "TTbarPowheg_Dilepton",],
    "VLL_M-500GeV": ["VLL_EE_M-500GeV","VLL_EN_M-500GeV","VLL_NN_M-500GeV",],
    "VLL_M-550GeV": ["VLL_EE_M-550GeV","VLL_EN_M-550GeV","VLL_NN_M-550GeV",],
    "VLL_M-600GeV": ["VLL_EE_M-600GeV","VLL_EN_M-600GeV","VLL_NN_M-600GeV",],
    "VLL_M-650GeV": ["VLL_EE_M-650GeV","VLL_EN_M-650GeV","VLL_NN_M-650GeV",],
    "VLL_M-700GeV": ["VLL_EE_M-700GeV","VLL_EN_M-700GeV","VLL_NN_M-700GeV",],
    "VLL_M-750GeV": ["VLL_EE_M-750GeV","VLL_EN_M-750GeV","VLL_NN_M-750GeV",],
    "VLL_M-800GeV": ["VLL_EE_M-800GeV","VLL_EN_M-800GeV","VLL_NN_M-800GeV",],
    "VLL_M-850GeV": ["VLL_EE_M-850GeV","VLL_EN_M-850GeV","VLL_NN_M-850GeV",],
    "VLL_M-900GeV": ["VLL_EE_M-900GeV","VLL_EN_M-900GeV","VLL_NN_M-900GeV",],
    "VLL_M-950GeV": ["VLL_EE_M-950GeV","VLL_EN_M-950GeV","VLL_NN_M-950GeV",],
    "VLL_M-1000GeV": ["VLL_EE_M-1000GeV","VLL_EN_M-1000GeV","VLL_NN_M-1000GeV",],
}
    


sl = hist.tag.Slicer()

if __name__ == "__main__":

    ##############################################
    # Scale MC histograms with xsection and lumi
    ##############################################

    for samp, sampList in groupingMCDatasets.items():
        for s in sampList:
            print('Compare: ', nevents[era][s],'   ',outputMC[s]['EventCount'])
            lumiSF = (crossSections[s] * lumis[era]) / nevents[era][s]
            if 'VLL' in s:
                lumiSF = (0.005 * lumis[era]) / nevents[era][s]
            for key, histo in outputMC[s].items():
                if isinstance(histo, hist.Hist):
                    histo *= lumiSF

    # Group MC histograms
    histList = []
    for samp, sampList in groupingMCDatasets.items():
        histList += [outputMC[s] for s in sampList]

    outputMCHist = accumulate(histList)
    for key, histo in outputMCHist.items():
        if isinstance(histo, hist.Hist):
            outputMCHist[key] = GroupBy(histo, 'dataset', 'dataset', groupingMCDatasets)

    # Group data histograms
    outputDataHist = accumulate([histo for key, histo in outputData.items()])

    var = 'event_ST'
    ax = 'event_HT'

    h = outputMCHist[var]
    h = h[{'lepFlavor':sum,'dileptonCharge':sum}]
    h = GroupBy(h, "bJetMultiplicity", "bJetMultiplicity", groupingCategory)

    new_bins = np.array([
        h.axes[ax].edges[30],
        h.axes[ax].edges[35],
        h.axes[ax].edges[40],
        h.axes[ax].edges[45],
        h.axes[ax].edges[50],
        h.axes[ax].edges[55],
        h.axes[ax].edges[60],
        h.axes[ax].edges[65],
        h.axes[ax].edges[70],
        h.axes[ax].edges[77],
        h.axes[ax].edges[85],
        h.axes[ax].edges[95],
        h.axes[ax].edges[107],
        h.axes[ax].edges[140],
        h.axes[ax].edges[300],
    ])  

    new_bins2 = np.array([
        h.axes[ax].edges[30],
        h.axes[ax].edges[40],
        h.axes[ax].edges[50],
        h.axes[ax].edges[60],
        h.axes[ax].edges[70],
        h.axes[ax].edges[80],
        h.axes[ax].edges[90],
        h.axes[ax].edges[100],
        h.axes[ax].edges[110],
        h.axes[ax].edges[130],
        h.axes[ax].edges[150],
        h.axes[ax].edges[180],
        h.axes[ax].edges[200],
        h.axes[ax].edges[230],
        h.axes[ax].edges[260],
        h.axes[ax].edges[300],
    ])  

    new_bins3 = np.array([
        h.axes[ax].edges[30],
        h.axes[ax].edges[45],
        h.axes[ax].edges[55],
        h.axes[ax].edges[65],
        h.axes[ax].edges[75],
        h.axes[ax].edges[85],
        h.axes[ax].edges[95],
        h.axes[ax].edges[105],
        h.axes[ax].edges[120],
        h.axes[ax].edges[140],
        h.axes[ax].edges[170],
        h.axes[ax].edges[300],
    ])  


    h = rebin_hist(h, ax, new_bins3)


    hData = outputDataHist[var]
    hData = GroupBy(hData, "bJetMultiplicity", "bJetMultiplicity", groupingCategory)
    hData = hData[{'lepFlavor':sum,'dileptonCharge':sum,'bJetMultiplicity':'SR','systematic':sum,'dataset':sum}]
    hData = rebin_hist(hData, ax, new_bins3)

    outdir = "RootFiles"
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    outputFile = uproot.recreate(os.path.join(outdir, "HT_Output_SR.root"))

    outputFile["data_obs"] = hData

    systematics = [s for s in h.axes["systematic"] if not s.startswith("Q2Scale")]
    for _dataset in groupingMCDatasets:#["VLL_M-600GeV"]:#,"ttbar", "dy", "singlet","ttH","ttV", "tttt","other"]:
        for _systematic in systematics:
            histname = f"{_dataset}_{_systematic}" if (not f"{_systematic}" == 'nominal') else f"{_dataset}"
            outputFile[histname] = h[{'dataset':_dataset,'bJetMultiplicity':'SR','systematic':_systematic}]

    outputFile.close()

