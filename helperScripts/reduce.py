import coffea
from coffea.processor import accumulate
from coffea import util

def select_keys(input_file, output_file, keys_to_keep):
    fin = accumulate([util.load(input_file),])
    out_dict = {}
    for key in keys_to_keep:
        if key in fin:
            out_dict[key] = fin[key]
        else:
            print(f"Key '{key}' not found in input file.")
	 
    util.save(out_dict, output_file)
        
# Example usage:
input_file = 'Outputs/v4/2016pre/output_Data_run20240517_171443.coffea'
output_file = 'output.coffea'
keys_to_keep = ['Data_SingleEle_runD_2016','Data_SingleEle_runC_2016','Data_SingleEle_runBver1_2016','Data_SingleEle_runBver2_2016','Data_SingleEle_runE_2016','Data_SingleMu_runD_2016','Data_SingleMu_runC_2016','Data_SingleMu_runBver1_2016','Data_SingleMu_runBver2_2016','Data_SingleMu_runE_2016']

select_keys(input_file, output_file, keys_to_keep)
