
import pandas as pd
import pyarrow.parquet as pq
import uproot
import os

# Base directory where subfolders exist
base_directory_path = '/hdfs/store/user/ckoraka/TTbar-VLL/2018/'

# List all subfolders under the base directory
subfolders = [f.path for f in os.scandir(base_directory_path) if f.is_dir()]

print(subfolders)

for subfolder in subfolders:
    subfolder_name = os.path.basename(subfolder).replace('/hdfs/store/user/ckoraka/TTbar-VLL/2018/dataset_', '')
    all_files = os.listdir(subfolder)
    dataframes = []

    for file_name in all_files:
        file_path = os.path.join(subfolder, file_name)
        if file_name.endswith('.parquet'):
            # Read the Parquet file and convert it to a DataFrame
            data = pq.read_table(file_path)
            df = data.to_pandas()
            dataframes.append(df)
    
    # Concatenate all DataFrames from the Parquet files
    concatenated_df = pd.concat(dataframes, ignore_index=True)

    # Create the output ROOT file with the subfolder name
    output_root_file = subfolder_name+".root"
    print(output_root_file)
    file = uproot.recreate(output_root_file)
    file['tree'] = concatenated_df

    print(f"Processed {subfolder_name} and saved to {output_root_file}")
