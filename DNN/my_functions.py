import inspect
import os
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import auc
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from array import array
from ROOT import *


def compare_train_test_binary(clf, X_train, y_train, X_test, y_test, xlabel):
    decisions = []  # list to hold decisions of classifier
    for X, y in ((X_train, y_train), (X_test, y_test)):  # loop through train and test
        if hasattr(clf, "predict"):  # if predict function exists
            # Predictions for signal (y > 0.5) and background (y < 0.5)
            d1 = clf.predict(X[y > 0.5])  # signal
            d2 = clf.predict(X[y < 0.5])  # background (e.g., ttbb)
        decisions += [d1, d2]  # add to list of classifier decisions

    highest_decision = max(np.max(d) for d in decisions)  # get maximum decision score
    bin_edges = []  # list to hold bin edges
    bin_edge = -0.1  # start counter for bin_edges
    while bin_edge < highest_decision:  # up to the highest decision score
        bin_edge += 0.02  # increment
        bin_edges.append(bin_edge)

    # Unbinned Kolmogorov-Smirnov Test on two sorted arrays (requires sorting!):
    x_gaussA_array = array('d', sorted(decisions[0]))  # signal (train)
    x_gaussB_array = array('d', sorted(decisions[2]))  # background (train)
    pKS = TMath.KolmogorovTest(len(x_gaussA_array), x_gaussA_array, len(x_gaussB_array), x_gaussB_array, "D")
    print('KS probability class signal (train):', pKS)
    extraString_Signal = 'KS prob. Signal (train): ' + str("{:.2f}".format(pKS))

    x_gaussA_array = array('d', sorted(decisions[1]))  # signal (test)
    x_gaussB_array = array('d', sorted(decisions[3]))  # background (test)
    pKS = TMath.KolmogorovTest(len(x_gaussA_array), x_gaussA_array, len(x_gaussB_array), x_gaussB_array, "D")
    print('KS probability class background (test):', pKS)
    extraString_Bkg = 'KS prob. Background (test): ' + str("{:.2f}".format(pKS))

    # Plot train-test data
    figKS = plt.figure(figsize=(20, 12))
    
    # Plot for Signal and Background (Train)
    plt.hist(decisions[0], bins=bin_edges, density=True, histtype='stepfilled', color='blue', label='Signal (train)', alpha=0.5)
    plt.hist(decisions[1], bins=bin_edges, density=True, histtype='stepfilled', color='orange', label='Background (train)', alpha=0.5)

    ax = plt.gca()  # get current axis    
    plt.text(0.5, 1.05, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold", 
             horizontalalignment='center', verticalalignment='center', transform=ax.transAxes, fontsize=28)

    # For Signal (Test) -- plotting with error bars
    hist_ttHbb, bin_edges = np.histogram(decisions[2], bins=bin_edges, density=True)
    scale = len(decisions[2]) / sum(hist_ttHbb)  # normalizing signal
    err_ttHbb = np.sqrt(hist_ttHbb * scale) / scale  # error on test signal
    width = 0.1  # histogram bin width
    center = (bin_edges[:-1] + bin_edges[1:]) / 2  # bin centers
    plt.errorbar(x=center, y=hist_ttHbb, yerr=err_ttHbb, fmt='o', c='blue', label='Signal (test)')  # Signal (test)

    # For Background (Test) -- plotting with error bars
    hist_ttbb, bin_edges = np.histogram(decisions[3], bins=bin_edges, density=True)
    scale = len(decisions[3]) / sum(hist_ttbb)  # normalizing background
    err_ttbb = np.sqrt(hist_ttbb * scale) / scale  # error on test background
    plt.errorbar(x=center, y=hist_ttbb, yerr=err_ttbb, fmt='o', c='orange', label='Background (test)')  # Background (test)

    # Add KS probabilities to legend
    handles, labels = ax.get_legend_handles_labels()
    handles.append(mpatches.Patch(color='none', label=extraString_Signal))
    handles.append(mpatches.Patch(color='none', label=extraString_Bkg))

    plt.xlabel(xlabel, fontsize=24)  # x-axis label
    plt.ylabel("Arbitrary units", fontsize=24)  # y-axis label
    plt.legend(loc='best', handles=handles, fontsize=20)  # add legend
    plt.savefig('KS_binary_classification.png')  # Save the plot as PNG
    plt.close(figKS)  # Close the plot


def compare_train_test_binary_2(clf, X_train, y_train, X_test, y_test, xlabel):
    decisions = [] # list to hold decisions of classifier
    for X,y in ((X_train, y_train), (X_test, y_test)): # train and test
        if hasattr(clf, "predict"): # if predict function exists
            d1 = clf.predict(X[y>0.5])# signal
            d2 = clf.predict(X[y<0.5])# background ttbb
        decisions += [d1, d2] # add to list of classifier decision

    highest_decision = max(np.max(d) for d in decisions) # get maximum score
    bin_edges = [] # list to hold bin edges
    bin_edge = -0.1 # start counter for bin_edges
    while bin_edge < highest_decision: # up to highest score
        bin_edge += 0.02 # increment
        bin_edges.append(bin_edge)

    # Unbinned Kolmogorov-Smirnov Test on two sorted arrays (requires sorting!):
    x_gaussA_array = array('d', sorted(decisions[0]))
    x_gaussB_array = array('d', sorted(decisions[2]))
    pKS = TMath.KolmogorovTest(len(x_gaussA_array), x_gaussA_array,len(x_gaussB_array), x_gaussB_array, "D")
    print('KS probability class signal :',pKS)
    extraString_Signal = 'KS prob. Signal: '+ str("{:.2f}".format(pKS))
    x_gaussA_array = array('d', sorted(decisions[1]))
    x_gaussB_array = array('d', sorted(decisions[3]))
    pKS = TMath.KolmogorovTest(len(x_gaussA_array), x_gaussA_array,len(x_gaussB_array), x_gaussB_array, "D")
    print('KS probability class bkg ttlf :',pKS)
    extraString_Bkg = 'KS prob. Background: '+ str("{:.2f}".format(pKS))

    # Plot train-test data
    figKS = plt.figure(figsize=(20, 12))
    plt.hist(decisions[0],bins=bin_edges,density=True,histtype='stepfilled',color='blue',label='Signal (train)',alpha=0.5)
    plt.hist(decisions[1],bins=bin_edges,density=True,histtype='stepfilled',color='orange',label='Background (train)',alpha=0.5)

    ax = plt.gca()    
    plt.text(0.5, 1.05, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold", horizontalalignment='center',verticalalignment='center', transform=ax.transAxes, fontsize=28)

    hist_ttHbb, bin_edges = np.histogram(decisions[2],bins=bin_edges,density=True )
    scale = len(decisions[2]) / sum(hist_ttHbb) # between raw and normalised
    err_ttHbb = np.sqrt(hist_ttHbb * scale) / scale # error on test background
    width = 0.1 # histogram bin width
    center = (bin_edges[:-1] + bin_edges[1:]) / 2 # bin centres
    plt.errorbar(x=center, y=hist_ttHbb, yerr=err_ttHbb, fmt='o',c='blue', label='Signal (test)' ) # Signal (test)

    hist_ttbb, bin_edges = np.histogram(decisions[3],bins=bin_edges,density=True )
    scale = len(decisions[3]) / sum(hist_ttbb) # between raw and normalised
    err_ttbb = np.sqrt(hist_ttbb * scale) / scale # error on test background
    plt.errorbar(x=center, y=hist_ttbb, yerr=err_ttbb, fmt='o',c='orange', label='Background (test)' ) # tt (test)

    handles, labels = ax.get_legend_handles_labels()
    handles.append(mpatches.Patch(color='none', label=extraString_Signal))
    handles.append(mpatches.Patch(color='none', label=extraString_Bkg))

    plt.xlabel(xlabel,fontsize=24) # write x-axis label
    plt.ylabel("Arbitrary units",fontsize=24) # write y-axis label
    plt.legend(loc='best',handles=handles,fontsize=20) # add legend
    plt.savefig('KS_binary_classification.png')
    plt.close(figKS)


def selection_criteria(event_HT,nbJets): 
    if(event_HT>250 and nbJets>=4): return True
    else: return False


def plot_input_features(X, y, idx_label, xlabel):
    decisions = [] 
    d1 = X[y==1,idx_label]
    d2 = X[y==0,idx_label] 
    decisions += [d1, d2] 

    highest_decision = max(np.max(d) for d in decisions) # get maximum
    lowest_decision = max(np.min(d) for d in decisions) # get minimum

    bin_edges = [] # list to hold bin edges
    bin_edge =  0.# start counter for bin_edges
    while bin_edge < highest_decision: # up to highest score
        bin_edges.append(bin_edge)
        bin_edge += 0.05*(highest_decision - lowest_decision) # increment

    if(xlabel=='mbb'):
        bin_edges = [0.,20.,40.,60.,80.,100.,120.,140.,160.,180.,200.,220.,240.,260.,280.,300.,320.,340.,360.,380.,400.,420.,440.,460.,480.,500.]

    # Plot train-test data
    fig = plt.figure(figsize=(20, 12))

    ax = plt.gca()    
    plt.text(0.5, 1.05, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold", horizontalalignment='center',verticalalignment='center', transform=ax.transAxes, fontsize=28)

    plt.hist(decisions[0],bins=bin_edges,density=True,histtype='stepfilled',color='blue',label='Signal - VLL',alpha=0.5)
    plt.hist(decisions[1],bins=bin_edges,density=True,histtype='stepfilled',color='orange',label='Background - tt+jets',alpha=0.5)

    plt.xlabel(xlabel,fontsize=28) # write x-axis label
    plt.ylabel("Arbitrary units",fontsize=28) # write y-axis label
    plt.legend(loc='best',fontsize=24) # add legend
    plt.savefig('Var_'+xlabel+'_.png')
    plt.close(fig)


def AUC_ROC(X_train, y_train, X_test, y_test):

    # fit model
    clf = OneVsRestClassifier(LogisticRegression())
    clf.fit(X_train, y_train)
    pred = clf.predict(X_test)
    pred_prob = clf.predict(X_test)

    # roc curve for classes
    fpr = {}
    tpr = {}
    thresh ={}
    auc_ ={}
    fauc_={}

    n_class = 4

    for i in range(n_class):
        fpr[i], tpr[i], thresh[i] = roc_curve(y_test, pred_prob[:,i], pos_label=i)
        auc_[i] = auc(fpr[i], tpr[i])
        print('AUC',auc_[i])
        fauc_[i] = "{:.2f}".format(auc_[i])
    print(' Length ',len(fpr))

    # plotting
    fig = plt.figure(figsize=(16, 16))
    plt.plot(fpr[0], tpr[0], linestyle='--',linewidth=2,color='blue', label='Class ttHbb vs Rest - AUC:'+str(fauc_[0]))
    plt.plot(fpr[1], tpr[1], linestyle='--',linewidth=2,color='orange', label='Class ttbb vs Rest - AUC:'+str(fauc_[1]))
    plt.plot(fpr[2], tpr[2], linestyle='--',linewidth=2,color='mediumpurple', label='Class ttcc vs Rest - AUC:'+str(fauc_[2]))
    plt.plot(fpr[3], tpr[3], linestyle='--',linewidth=2,color='cadetblue', label='Class ttlf vs Rest - AUC:'+str(fauc_[3]))
    plt.title('Multiclass ROC curve')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive rate')
    plt.grid(True)
    plt.legend(loc='best')
    plt.savefig('Multiclass-ROC.png',dpi=300);
    plt.close(fig)


    myfile = TFile( 'ROC_curves.root', 'RECREATE' )

    for i in range(n_class):
        gr = TGraph(len(fpr[i]),fpr[i],tpr[i] )
        gr.SetLineColor( 2 )  
        gr.SetLineWidth( 4 )
        gr.SetMarkerColor( 4 )
        gr.SetMarkerStyle( 21 )
        gr.SetTitle( 'AUC class '+str(i) )  
        gr.GetXaxis().SetTitle( 'False positive rate' )
        gr.GetYaxis().SetTitle( 'True positive rate' )
        gr.SetName('AUC_class_'+str(i))
        gr.Write()

    myfile.Close()


