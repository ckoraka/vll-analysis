This framework is used to train and evaluate a DNN model using tensorflow and keras.

## Instructions

**For preparing the environment :**

1. Run under a CMSSW release
2. Export the Keras back-end
3. Set PYTHONHOME path

**Commands to run :**

**Step-up CMSSW**
```
cmsrel CMSSW_14_0_0
cd CMSSW_14_0_0/src
cmsenv
cd ../..
export KERAS_BACKEND=tensorflow
```

**To run the training script :**
```
python3 train_model_binary.py
```

Two files are produced when running the model training. The **model.h5** file which contains the model weights and architecture and the **variable_norm.csv** file which contains the input feature 
normalization values needed for the evaluation script.


**To run the evaluation script (updated for python3) :**
```
python3  evaluate.py input.root output.root conf.json
```
where :

1. **input** Input .root file
2. **output** Output .root file
3. **conf.json** Json file with configs


where :


1. **input** Input .root file 
2. **output** Output .root file
3. **conf.json** Json file with configs 

The evaluation script produces a root file with the values of the output nodes and any additional branch from the input tree - should configue the **conf.json** file accordingly.
