import ROOT
import matplotlib.pyplot as plt
import numpy as np

# Open the ROOT files
file_ttbar = ROOT.TFile.Open("test500.root")
file_vll = ROOT.TFile.Open("testS500.root")

# Access the trees from both files
tree_ttbar = file_ttbar.Get("tree")
tree_vll = file_vll.Get("tree")

# Set up a canvas to plot
canvas = ROOT.TCanvas("canvas", "Superimposed VLL Plots", 800, 600)

# Define histograms for vll from both datasets
hist_ttbar = ROOT.TH1F("hist_ttbar", "vll for TTbar", 50, 0, 1)
hist_vll = ROOT.TH1F("hist_vll", "vll for VLL_M-600GeV", 50, 0, 1)

# Loop over the TTbar tree
for entry in tree_ttbar:
    if entry.nbJets > 3 and entry.event_HT>300:  # Apply the condition nbJets > 3
        hist_ttbar.Fill(entry.vll)

# Loop over the VLL tree
for entry in tree_vll:
    if entry.nbJets > 3 and entry.event_HT>300:  # Apply the condition nbJets > 3
        hist_vll.Fill(entry.vll)

# Set histogram styles
hist_ttbar.SetLineColor(ROOT.kRed)  # Red color for TTbar
hist_ttbar.SetLineWidth(2)
hist_vll.SetLineColor(ROOT.kBlue)   # Blue color for VLL
hist_vll.SetLineWidth(2)

# Draw the histograms on the canvas
hist_ttbar.Draw("HIST")
hist_vll.Draw("HIST SAME")

# Add legend
legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
legend.AddEntry(hist_ttbar, "TTbar", "l")
legend.AddEntry(hist_vll, "VLL_M-600GeV", "l")
legend.Draw()

# Save the plot as a PNG file
canvas.SaveAs("superimposed_vll_plot.png")

# Close the files
file_ttbar.Close()
file_vll.Close()

print("Plot saved as 'superimposed_vll_plot.png'")
