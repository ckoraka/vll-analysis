#!/usr/bin/env python

from __future__ import print_function

import time
start_time = time.time()

import tensorflow as tf
from keras.models import load_model
from tensorflow import keras
from keras import layers
from keras import optimizers
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten
from keras import regularizers
from keras.wrappers.scikit_learn import KerasClassifier
from keras.callbacks import EarlyStopping

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.svm import SVC
from sklearn.utils import class_weight

from sklearn.utils import shuffle

from tensorflow.keras import Sequential, layers, regularizers
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping

from matplotlib import pyplot
import numpy as np
import pandas as pd
import uproot as uprt
import seaborn as sns
import h5py
import csv
import re
import random

from my_functions import compare_train_test_binary
from my_functions import selection_criteria
from my_functions import plot_input_features
from my_functions import AUC_ROC

from keras.callbacks import LambdaCallback

from sklearn.inspection import permutation_importance
from sklearn.metrics import make_scorer, accuracy_score

print(tf.__version__)

class MyCustomCallback(tf.keras.callbacks.Callback):

  def on_train_batch_begin(self, batch, logs=None):
    print(self.model.layers[0].get_weights())

  def on_train_batch_end(self, batch, logs=None):
    print(self.model.layers[0].get_weights())

def prepare_data():

    masses = range(500,1050, 50)
    categories = ['EE', 'EN', 'NN']
    dataset_files = []
    for category in categories:
        for mass in masses:
            dataset_files.append(f'dataset_VLL_{category}_M-{mass}GeV.root')

    samples = ['dataset_TTbarPowheg_Dilepton.root'] + dataset_files
    DataFrames = {}  # define empty dictionary to hold dataframes
    Selection = {}

    Selection_inputs = ["event_HT","nbJets"]

    ML_inputs = ["event_HT","event_bHT", "event_MLL", "ratioJetHT","deltaPhibJetMet", "min_bjet_dr", "deltaR_bjetLep", "min_dr_lepb", "max_dr_lepb", "nJets","dileptonMass", "event_MET",]

    print('Preparing data')
    for s in samples:  
        for year in ["2018","2017","2016post","2016pre"]:
            print(s)
            if 'VLL' in s:  
                match = re.search(r'M-(\d+)GeV', s)
                if match:
                    mass_point = int(match.group(1)) 
                else:
                    raise ValueError(f"Mass not found in signal dataset name: {s}")
            else:  
                mass_point = None

            if '.root' in s:
                file = uprt.open("/afs/hep.wisc.edu/home/ckoraka/VLL-analysis/coffea-FW/vll-analysis/helperScripts/rootfiles_"+year+"/" + s )
                tree = file["tree"]
                name = s+str(year)
                DataFrames[name] = tree.arrays(ML_inputs, library="pd")
                Selection[name] = tree.arrays(Selection_inputs, library="pd")
                if 'VLL' not in s:
                    DataFrames[name]['mass_point'] = np.random.choice(masses, size=len(DataFrames[name]))
                else:  
                    DataFrames[name]['mass_point'] = np.full(len(DataFrames[name]), mass_point)
            else:
                raise ValueError("Invalid file extension. Supported formats: .root, .parquet")

            DataFrames[name] = DataFrames[name][np.vectorize(selection_criteria)(Selection[name].event_HT,Selection[name].nbJets)]
            
            if 'VLL' in name:
                DataFrames[name] = DataFrames[name].iloc[0:200]  # first rows of dataframe
    
    ML_inputs.append('mass_point')        

    all_MC = []  # define empty list that will contain all features for the MC
    all_y = []  # define empty list that will contain labels whether an event in signal or background

    for s in samples:  
        for year in ["2018","2017","2016post","2016pre"]:
            name = s+str(year)
            if name != 'data': 
                all_MC.append(DataFrames[name][ML_inputs])  # append the MC dataframe to the list containing all MC features
                if 'VLL' in name: 
                    all_y.append(np.ones(DataFrames[name].shape[0]))  # signal events are labelled with 1
                else:
                    all_y.append(np.full(DataFrames[name].shape[0], 0))  # All backgrounds labelled with 0

    X = np.concatenate(all_MC)  # concatenate the list of MC dataframes into a single 2D array of features, called X
    y = np.concatenate(all_y)  # concatenate the list of labels into a single 1D array of labels, called y

    # Shuffle
    X, y = shuffle(X, y, random_state=42)
    # Balance
    signal_mask = y == 1
    background_mask = y == 0
    X_signal = X[signal_mask]
    y_signal = y[signal_mask]
    X_background = X[background_mask]
    y_background = y[background_mask]
    num_events = 10000
    X_signal_u = X_signal[:num_events]
    y_signal_u = y_signal[:num_events]
    X_background_u = X_background[:num_events]
    y_background_u = y_background[:num_events]

    # Combine the undersampled background and signal data
    X_balanced = np.concatenate([X_signal_u, X_background_u])
    y_balanced = np.concatenate([y_signal_u, y_background_u])

    print(X_balanced,y_balanced)

    print('Size Signal: ', len(X_background))
    print('Size Bkg: ', len(X_signal_u))

    # make train and test sets
    print('Preparing train and test data')
    X_train, X_test, y_train, y_test = train_test_split(X_balanced, y_balanced, test_size=0.30, random_state=492)  # set the random seed for reproducibility

    scaler = StandardScaler() 
    scaler.fit(X_train) 
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    for x, var in enumerate(ML_inputs):
        plot_input_features(X, y, x, var)

    # Save .csv with normalizations
    mean = scaler.mean_
    std = scaler.scale_
    with open('variable_norm.csv', mode='w') as norm_file:
        headerList = ['', 'mu', 'std']
        norm_writer = csv.DictWriter(norm_file, delimiter=',', fieldnames=headerList)
        norm_writer.writeheader()
        for x, var in enumerate(ML_inputs):
            print(var, mean[x], std[x])
            norm_writer.writerow({'': var, 'mu': mean[x], 'std': std[x]})

    X_valid_scaled, X_train_nn_scaled = X_train_scaled[:1000], X_train_scaled[1000:]  # first 1000 events for validation
    y_valid, y_train_nn = y_train[:1000], y_train[1000:]  # first 1000 events for validation

    print('Input feature correlation ttbar')
    print(DataFrames['dataset_TTbarPowheg_Dilepton.root'+year].corr())  # Pearson
    fig = pyplot.figure(figsize=(20, 16))
    corrMatrix = DataFrames['dataset_TTbarPowheg_Dilepton.root'+year].corr()
    ax = pyplot.gca()
    pyplot.text(0.5, 1.05, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold",horizontalalignment='center', verticalalignment='center', transform=ax.transAxes, fontsize=28)
    sns.heatmap(corrMatrix, annot=True, cmap=pyplot.cm.Blues)
    pyplot.savefig('correlation_ttbar.png')

    print('Input feature correlationn Signal')
    print(DataFrames['dataset_VLL_EE_M-600GeV.root'+year].corr())  # Pearson
    fig = pyplot.figure(figsize=(20, 16))
    corrMatrix = DataFrames['dataset_VLL_EE_M-600GeV.root'+year].corr()
    ax = pyplot.gca()
    pyplot.text(0.5, 1.05, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold", horizontalalignment='center', verticalalignment='center', transform=ax.transAxes, fontsize=28)
    sns.heatmap(corrMatrix, annot=True, cmap=pyplot.cm.Blues)
    pyplot.savefig('correlation_signal.png')


    return X_train_nn_scaled, y_train_nn, X_test_scaled, y_test, X_valid_scaled, y_valid

def nn_model():
    model = Sequential()
    model.add(Dense(249, input_dim=13, kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=7e-4)))
    model.add(BatchNormalization())  
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.2))   
    model.add(Dense(128, kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=7e-4)))
    model.add(BatchNormalization())  
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.2)) 
    model.add(Dense(64, kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=7e-4)))
    model.add(BatchNormalization()) 
    model.add(layers.LeakyReLU())
    model.add(layers.Dropout(0.2))   
    model.add(Dense(1, activation='sigmoid'))

    return model

def train_model(X_train, y_train, X_test, y_test, X_val, y_val):

    # fetch cnn model
    model = nn_model()

    # simple early stopping
    es = EarlyStopping(monitor='val_loss', mode='min', restore_best_weights=True, patience=20, verbose=1)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=5, min_lr=1e-6)

    # Compile model
    model.compile(loss='binary_crossentropy',optimizer='adagrad',metrics=['accuracy'])
    #model.compile(optimizer=Adam(learning_rate=0.001), loss='binary_crossentropy', metrics=['AUC', 'Precision', 'Recall'])
    weight_print = MyCustomCallback()

    # Fit model
    history = model.fit(X_train, y_train, epochs=200, batch_size=128, validation_data=(X_val, y_val), verbose=1, callbacks=[es, reduce_lr])
    #callbacks = [weight_print])

    # plot ROC
    decisions_tf = model.predict(X_test)
    fpr_tf, tpr_tf, thresholds_tf = roc_curve(y_test, decisions_tf)
    auc_ = auc(fpr_tf,tpr_tf)
    fauc_ = "{:.2f}".format(auc_)
    figRoc = pyplot.figure(figsize=(15, 15))
    ax = pyplot.gca()    
    pyplot.text(0.5, 1.05, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold", horizontalalignment='center',verticalalignment='center', transform=ax.transAxes, fontsize=28)
    pyplot.plot(tpr_tf, 1-fpr_tf, linestyle='--',linewidth=8,color='blue', label='VLL vs tt - AUC:'+str(fauc_))
    #pyplot.plot([1, 0], [1, 0], linestyle='dotted', color='grey', label='Luck') # plot diagonal line
    pyplot.xlabel('Signal efficiency',fontsize=28)
    pyplot.ylabel('Background Rejection',fontsize=28)
    pyplot.legend(loc='best',fontsize=28)
    pyplot.grid()
    pyplot.savefig('ROC.png')

    # plot train-test comparisons
    compare_train_test_binary(model,X_train,y_train,X_test,y_test,'VLL vs ttbar')

    # evaluate model
    loss, acc = model.evaluate(X_test, y_test, verbose=1, batch_size=64)
    print('Test loss: {:.4f}'.format(loss))
    print('Test accuracy: {:.4f}'.format(acc))

    # confusion matrix
    y_pred = model.predict(X_test)
    Y_test = y_test.reshape(len(y_test),1)
    Y_pred = y_pred
    Y_pred[y_pred<0.5] = 0
    Y_pred[y_pred>0.5] = 1 
    mat = confusion_matrix(Y_test, Y_pred)
    classes = [0,1]
    con_mat_norm = np.around(mat.astype('float') / mat.sum(axis=1)[:, np.newaxis], decimals=2)
    con_mat_df = pd.DataFrame(con_mat_norm, index=classes, columns=classes)

    # plot confusion matrix
    fig1 = pyplot.figure(figsize=(15, 15))
    ax = pyplot.gca()    
    pyplot.text(0.5, 1.05, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold", horizontalalignment='center',verticalalignment='center', transform=ax.transAxes, fontsize=28)
    sns.heatmap(con_mat_df, annot=True, cmap=pyplot.cm.Blues)
    #pyplot.tight_layout()
    
    pyplot.ylabel('True Class',fontsize=28)
    pyplot.xlabel('Predicted Class',fontsize=28)
    pyplot.savefig('confusion_matrix.png')
       
    # r = permutation_importance(model, X_test, y_test, n_repeats=10,random_state=0,scoring='neg_mean_squared_error')
    # for i in r.importances_mean.argsort()[::-1]:
    #     if r.importances_mean[i] - 2 * r.importances_std[i] > 0:
    #         print(f"{ML_inputs[i]:<25} {r.importances_mean[i]:.3f} +/- {r.importances_std[i]:.3f}")


    # save trained model
    model.save('model.h5')

    return acc, history


def plot_model(history):
    # plot entropy loss
    pyplot.subplot(2, 1, 1)
    ax = pyplot.gca()    
    pyplot.text(0.5, 1.15, "CMS Simulation (Work In Progress)      (13 TeV)", fontweight="bold", horizontalalignment='center',verticalalignment='center', transform=ax.transAxes, fontsize=28)
    pyplot.title('Entropy Loss',fontsize=28)
    pyplot.plot(history[1].history['loss'], color='blue', label='train')
    pyplot.plot(history[1].history['val_loss'], color='red', label='test')

    # plot accuracy
    pyplot.subplot(2, 1, 2)
    pyplot.title('Accuracy',fontsize=28)
    pyplot.plot(history[1].history['accuracy'], color='blue', label='train')
    pyplot.plot(history[1].history['val_accuracy'], color='red', label='test')
    pyplot.xlabel('Epoch',fontsize=20)
    pyplot.savefig('loss_accuraccy.png')


def main():

    # 1 load train dataset
    X_train, y_train, X_test, y_test, X_val, y_val = prepare_data()

    # 2 train model
    history = train_model(X_train, y_train, X_test, y_test, X_val, y_val)

    # 3 plot model
    plot_model(history)

    print(('\033[1m'+'> Time Elapsed = {:.3f} sec'+'\033[0m').format((time.time()-start_time)))

if __name__ == "__main__":
    main()
