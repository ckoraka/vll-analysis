
import pathlib
import shutil

from coffea.analysis_tools import PackedSelection, Weights
from coffea.nanoevents import NanoAODSchema, NanoEventsFactory
from coffea.nanoevents.methods import nanoaod

import hist

import awkward as ak
import numpy as np
import pandas 

from .scalefactors.puScalefactors import *
from .scalefactors.jerjesCorrections import *
from .scalefactors.efficiencies import *

from .scalefactors import (
    jerjesCorrections,
    ceval2016pre,
    ceval2016post,
    ceval2017,
    ceval2018,
    ele_trig_sf_2016pre,
    ele_trig_err_statData_2016pre,
    ele_trig_err_statMC_2016pre,
    ele_trig_err_altMCEff_2016pre,
    ele_trig_err_altSignalModel_2016pre,
    ele_trig_err_altBkgModel_2016pre,
    ele_trig_err_altTagSelection_2016pre,
    ele_trig_sf_2016post,
    ele_trig_err_statData_2016post,
    ele_trig_err_statMC_2016post,
    ele_trig_err_altMCEff_2016post,
    ele_trig_err_altSignalModel_2016post,
    ele_trig_err_altBkgModel_2016post,
    ele_trig_err_altTagSelection_2016post,
    ele_trig_sf_2017,
    ele_trig_err_statData_2017,
    ele_trig_err_statMC_2017,
    ele_trig_err_altMCEff_2017,
    ele_trig_err_altSignalModel_2017,
    ele_trig_err_altBkgModel_2017,
    ele_trig_err_altTagSelection_2017,
    ele_trig_sf_2018,
    ele_trig_err_statData_2018,
    ele_trig_err_statMC_2018,
    ele_trig_err_altMCEff_2018,
    ele_trig_err_altSignalModel_2018,
    ele_trig_err_altBkgModel_2018,
    ele_trig_err_altTagSelection_2018,
)

writeOutParquet = False

# ######################################################################################################################
# Scale Factors using Correctionlib, from POG: https://gitlab.cern.ch/cms-nanoAOD/jsonpog-integration/-/tree/master/POG
# ######################################################################################################################
# https://btv-wiki.docs.cern.ch/ScaleFactors/

def btagSF(j, syst="central",era="2018"):
    # until correctionlib handles jagged data natively we have to flatten and unflatten
    j, nj = ak.flatten(j), ak.num(j)
    if(era=="2018"):
        sf = ceval2018["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    elif(era=="2017"):
        sf = ceval2017["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    elif(era=="2016post"):
        sf = ceval2016post["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    elif(era=="2016pre"):
        sf = ceval2016pre["deepJet_shape"].evaluate(syst,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(j.pt),np.array((j.btagDeepFlavB)))
    else:
        raise Exception(f"Error: Unknown era \"{era}\".")

    return ak.unflatten(sf, nj)

# Evaluate btag sf from central correctionlib json
def btagSFWP(j, syst="central",type="deepJet_incl", era="2018",WP=0.3040):
    j, nj = ak.flatten(j), ak.num(j)
    pt = ak.where(j.pt>1000.0,1000.0,j.pt)
    if(era=="2018"):
        sf = ceval2018[type].evaluate(syst,WP,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(pt))
    elif(era=="2017"):
        sf = ceval2017[type].evaluate(syst,WP,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(pt))
    elif(era=="2016post"):
        sf = ceval2016post[type].evaluate(syst,WP,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(pt))
    elif(era=="2016pre"):
        sf = ceval2016pre[type].evaluate(syst,WP,np.array(j.hadronFlavour),np.array(abs(j.eta)),np.array(pt))
    else:
        raise Exception(f"Error: Unknown era \"{era}\".")

    return sf

# Evaluate btag method 1a weight for a single WP (https://twiki.cern.ch/twiki/bin/viewauth/CMS/BTagSFMethods)
#   - Takes as input a given array of eff and sf and a mask for whether or not the events pass a tag
#   - Returns P(DATA)/P(MC)
#   - Where P(MC) = Product over tagged (eff) * Product over not tagged (1-eff)
#   - Where P(DATA) = Product over tagged (eff*sf) * Product over not tagged (1-eff*sf)
def get_method1a_wgt_singlewp(eff,sf,passes_tag):
    p_mc = ak.prod(eff[passes_tag],axis=-1) * ak.prod(1-eff[~passes_tag],axis=-1)
    p_data = ak.prod(eff[passes_tag]*sf[passes_tag],axis=-1) * ak.prod(1-eff[~passes_tag]*sf[~passes_tag],axis=-1)
    wgt = p_data/p_mc
    return wgt

def muonSFs(muon, wp, syst="central", era='2018'):
    mu, nmu = ak.flatten(muon), ak.num(muon)
    mu_l = muon[muon.pt<15]
    mu_h = muon[muon.pt>=15]
    mu_l, nmu_l = ak.flatten(mu_l), ak.num(mu_l)
    mu_h, nmu_h = ak.flatten(mu_h), ak.num(mu_h)

    if (era=="2018"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/MUO/'+era+'_UL/'+'muon_Z.json')
        era = era+'_UL'
    elif (era=="2017"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/MUO/'+era+'_UL/'+'muon_Z.json')
        era = era+'_UL'
    elif (era=="2016post"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/MUO/'+era+'VFP_UL/'+'muon_Z.json')
        era = era+'VFP_UL'
    elif (era=="2016pre"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/MUO/'+era+'VFP_UL/'+'muon_Z.json')
        era = era+'VFP_UL'
    else:
        raise Exception(f"Error: Unknown era \"{era}\".")

    if(wp=="Tight"):
        sf_muID_h = ceval["NUM_TightID_DEN_genTracks"].evaluate(era, np.array(abs(mu_h.eta)), np.array(mu_h.pt), syst)
        sf_muID_l = np.ones(len(mu_l.eta))
        muID_h = ak.unflatten(sf_muID_h, nmu_h)
        muID_l = ak.unflatten(sf_muID_l, nmu_l)
        muID = ak.concatenate((muID_h,muID_l),axis = 1)

        sf_muIso_h = ceval["NUM_LooseRelIso_DEN_TightIDandIPCut"].evaluate(era, np.array(abs(mu_h.eta)), np.array(mu_h.pt), syst) 
        sf_muIso_l = np.ones(len(mu_l.eta))
        muIso_h = ak.unflatten(sf_muIso_h, nmu_h)
        muIso_l = ak.unflatten(sf_muIso_l, nmu_l)
        muIso = ak.concatenate((muIso_h,muIso_l),axis = 1)

    elif(wp=="Loose"):
        sf_muID_h = ceval["NUM_LooseID_DEN_genTracks"].evaluate(era, np.array(abs(mu_h.eta)), np.array(mu_h.pt), syst)
        sf_muID_l = np.ones(len(mu_l.eta))
        muID_h = ak.unflatten(sf_muID_h, nmu_h)
        muID_l = ak.unflatten(sf_muID_l, nmu_l)
        muID = ak.concatenate((muID_h,muID_l),axis = 1)

        sf_muIso = ceval["NUM_LooseRelIso_DEN_TightIDandIPCut"].evaluate(era, np.array(abs(mu.eta)), np.array(mu.pt), syst)
        sf_muIso_l = np.ones(len(mu_l.eta))
        muIso_h = ak.unflatten(sf_muIso_h, nmu_h)
        muIso_l = ak.unflatten(sf_muIso_l, nmu_l)
        muIso = ak.concatenate((muIso_h,muIso_l),axis = 1)
    else:
        raise Exception(f"Error: Unknown WP \"{wp}\".")

    return muID, muIso


def muontrigSFs(muon, syst="nominal", era='2018'):
    mu, nmu = ak.flatten(muon), ak.num(muon)
    mu_l = muon[muon.pt<30]
    mu_m = muon[muon.pt<200]
    mu_m = mu_m[mu_m.pt>30]
    mu_h = muon[muon.pt>200]
    mu_l, nmu_l = ak.flatten(mu_l), ak.num(mu_l)
    mu_m, nmu_m = ak.flatten(mu_m), ak.num(mu_m)
    mu_h, nmu_h = ak.flatten(mu_h), ak.num(mu_h)


    if (era=="2018"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/Muons/UL/2018/2018_trigger/Efficiencies_muon_generalTracks_Z_Run2018_UL_SingleMuonTriggers_schemaV2.json')
        key = "NUM_IsoMu24_DEN_CutBasedIdTight_and_PFIsoTight"
    elif (era=="2017"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/Muons/UL/2017/2017_trigger/Efficiencies_muon_generalTracks_Z_Run2017_UL_SingleMuonTriggers_schemaV2.json')
        key = "NUM_IsoMu27_DEN_CutBasedIdTight_and_PFIsoTight"
    elif (era=="2016post"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/Muons/UL/2016_postVFP/2016_postVFP_trigger/Efficiencies_muon_generalTracks_Z_Run2016_UL_SingleMuonTriggers_schemaV2.json')
        key = "NUM_IsoMu24_or_IsoTkMu24_DEN_CutBasedIdTight_and_PFIsoTight"
    elif (era=="2016pre"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/Muons/UL/2016_preVFP/2016_preVFP_trigger/Efficiencies_muon_generalTracks_Z_Run2016_UL_HIPM_SingleMuonTriggers_schemaV2.json')
        key = "NUM_IsoMu24_or_IsoTkMu24_DEN_CutBasedIdTight_and_PFIsoTight"
    else:
        raise Exception(f"Error: Unknown era \"{era}\".")


    sf_mu = ceval[key].evaluate(np.array(abs(mu_m.eta)), np.array(mu_m.pt), syst)
    sfmu = ak.unflatten(sf_mu, nmu_m)
    sfmu = ak.prod(sfmu,axis=-1)

    return sfmu

def electronSFs(elec, wp, syst="sf",era="2018"):

    el, nel = ak.flatten(elec), ak.num(elec)
    elec_l = elec[elec.pt<20]
    elec_h = elec[elec.pt>=20]
    el_l, nel_l = ak.flatten(elec_l), ak.num(elec_l)
    el_h, nel_h = ak.flatten(elec_h), ak.num(elec_h)
    
    if (era=="2018"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/EGM/2018_UL/electron.json')
        list(ceval.keys())
    elif (era=="2017"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/EGM/2017_UL/electron.json')
    elif (era=="2016post"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/EGM/2016postVFP_UL/electron.json')
        era = era+'VFP'
    elif (era=="2016pre"):
        ceval = correctionlib.CorrectionSet.from_file('vll/scalefactors/EleMu/EGM/2016preVFP_UL/'+'electron.json')
        era = era+'VFP'
    else:
        raise Exception(f"Error: Unknown era \"{era}\".")

    sf_elReco_l = ceval["UL-Electron-ID-SF"].evaluate(era,syst,"RecoBelow20",np.array(abs(el_l.eta)), np.array(el_l.pt))
    sf_elReco_h = ceval["UL-Electron-ID-SF"].evaluate(era,syst,"RecoAbove20",np.array(abs(el_h.eta)), np.array(el_h.pt))
    elReco_l = ak.unflatten(sf_elReco_l, nel_l)
    elReco_h = ak.unflatten(sf_elReco_h, nel_h)
    elReco = ak.concatenate((elReco_l,elReco_h),axis = 1)

    sf_elID = ceval["UL-Electron-ID-SF"].evaluate(era,syst,wp,np.array(abs(el.eta)), np.array(el.pt))
    elID = ak.unflatten(sf_elID, nel)

    return elReco, elID

def selectMuons(events):

    # Twiki link: https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideMuonSelection

    muPassDXY = abs(events.Muon.dxy) < 0.5
    muPassDZ = abs(events.Muon.dz) < 0.2

    muonSelectTight = (
        (events.Muon.pt > 10.) & (abs(events.Muon.eta) < 2.4)
        & ((events.Muon.isPFcand) & (events.Muon.isTracker | events.Muon.isGlobal))
        & (events.Muon.pfRelIso04_all < 0.25) 
        & (events.Muon.tightId)
        & muPassDXY
        & muPassDZ
    )  

    muonSelectLoose = (
        (events.Muon.pt > 10) & (abs(events.Muon.eta) < 2.4)
        & ((events.Muon.isPFcand) & (events.Muon.isTracker | events.Muon.isGlobal))
        & (events.Muon.pfRelIso04_all < 0.25) 
        & (events.Muon.looseId) & np.invert(muonSelectTight)
        & muPassDXY
        & muPassDZ
    )

    return events.Muon[muonSelectTight], events.Muon[muonSelectLoose]


def selectMuonsMVA(events):

    muPassDXY = abs(events.Muon.dxy) < 0.5
    muPassDZ = abs(events.Muon.dz) < 0.2

    muonSelectTight = (
        (events.Muon.pt > 10.) & (abs(events.Muon.eta) < 2.4)
        & ((events.Muon.isPFcand) & (events.Muon.isTracker | events.Muon.isGlobal))
        & (events.Muon.pfRelIso04_all < 0.25) 
        & (events.Muon.mvaId > 1)
        & muPassDXY
        & muPassDZ
    )  

    muonSelectLoose = (
        (events.Muon.pt > 10) & (abs(events.Muon.eta) < 2.4)
        & ((events.Muon.isPFcand) & (events.Muon.isTracker | events.Muon.isGlobal))
        & (events.Muon.pfRelIso04_all < 0.25) 
        & (events.Muon.mvaId > 0) & np.invert(muonSelectTight)
        & muPassDXY
        & muPassDZ
    )

    return events.Muon[muonSelectTight], events.Muon[muonSelectLoose]


def selectElectrons(events):

    # Twiki link: https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedElectronIdentificationRun2#Offline_selection_criteria_for_V  
    eleEtaGap = (abs(events.Electron.eta) < 1.4442) | (abs(events.Electron.eta) > 1.566)
    elePassDXY = (abs(events.Electron.eta) < 1.479) & (abs(events.Electron.dxy) < 0.05) | (abs(events.Electron.eta) > 1.479) & (abs(events.Electron.dxy) < 0.1)
    elePassDZ = (abs(events.Electron.eta) < 1.479) & (abs(events.Electron.dz) < 0.1) | (abs(events.Electron.eta) > 1.479) & (abs(events.Electron.dz) < 0.2)

    # select tight electrons
    electronSelectTight = (
        (events.Electron.pt > 10)
        & (abs(events.Electron.eta) < 2.5)
        & (events.Electron.cutBased >= 4)
        & eleEtaGap
        & elePassDXY
        & elePassDZ
    ) 

    # select loose electrons
    electronSelectLoose = (
        (events.Electron.pt > 10)
        & (abs(events.Electron.eta) < 2.4)
        & eleEtaGap
        & (events.Electron.cutBased >= 1)
        & elePassDXY
        & elePassDZ
        & np.invert(electronSelectTight)
    )

    return events.Electron[electronSelectTight], events.Electron[electronSelectLoose]

def selectElectronsMVA(events):
    eleEtaGap = (abs(events.Electron.eta) < 1.4442) | (abs(events.Electron.eta) > 1.566)
    elePassDXY = (abs(events.Electron.eta) < 1.479) & (abs(events.Electron.dxy) < 0.05) | (abs(events.Electron.eta) > 1.479) & (abs(events.Electron.dxy) < 0.1)
    elePassDZ = (abs(events.Electron.eta) < 1.479) & (abs(events.Electron.dz) < 0.1) | (abs(events.Electron.eta) > 1.479) & (abs(events.Electron.dz) < 0.2)

    # select tight electrons
    electronSelectTight = (
        (events.Electron.pt > 10)
        & (abs(events.Electron.eta) < 2.5)
        & (events.Electron.mvaFall17V2Iso_WP80)
        & eleEtaGap
        & elePassDXY
        & elePassDZ
    ) 

    # select loose electrons
    electronSelectLoose = (
        (events.Electron.pt > 10)
        & (abs(events.Electron.eta) < 2.5)
        & eleEtaGap
        & (events.Electron.mvaFall17V2Iso_WPL)
        & elePassDXY
        & elePassDZ
        & np.invert(electronSelectTight)
    )

    return events.Electron[electronSelectTight], events.Electron[electronSelectLoose]

def selectTaus(events):

    # bitmask 1 = VVVLoose, 2 = VVLoose, 4 = VLoose, 8 = Loose, 16 = Medium, 32 = Tight, 64 = VTight, 128 = VVTight 

    tauSelectionMedium = (
        (events.Tau.pt > 20.) & (abs(events.Tau.eta) < 2.1)
        & (events.Tau.idDeepTau2017v2p1VSe > 1) 
        & (events.Tau.idDeepTau2017v2p1VSmu > 1) 
        & (events.Tau.idDeepTau2017v2p1VSjet > 15)       
        & (abs(events.Tau.dz) < 0.2)
    ) 

    tauSelectionLoose = (
        (events.Tau.pt > 20.) & (abs(events.Tau.eta) < 2.1)
        & (events.Tau.idDeepTau2017v2p1VSe > 1) 
        & (events.Tau.idDeepTau2017v2p1VSmu > 1) 
        & (events.Tau.idDeepTau2017v2p1VSjet > 7)
        & (abs(events.Tau.dz) < 0.2)
        & np.invert(tauSelectionMedium)
    ) 

    tauSelectionVLoose = (
        (events.Tau.pt > 20.) & (abs(events.Tau.eta) < 2.3)
        & (events.Tau.idDeepTau2017v2p1VSe > 1) 
        & (events.Tau.idDeepTau2017v2p1VSmu > 1) 
        & (events.Tau.idDeepTau2017v2p1VSjet > 3)        
        & (abs(events.Tau.dz) < 0.2)
        & np.invert(tauSelectionLoose)
    )  

    return events.Tau[tauSelectionVLoose], events.Tau[tauSelectionLoose], events.Tau[tauSelectionMedium]
