#!/usr/bin/env python
# coding: utf-8

# Skim Processor
import time
import coffea.processor as processor
import hist
import awkward as ak
import numpy as np
import uproot
import os
import re
import XRootD
import XRootD.client
from functools import partial
from coffea.analysis_tools import PackedSelection

import vector as vec
from coffea.nanoevents.methods import vector

from vll.corrections import selectMuons,selectElectronsMVA
from vll.scalefactors.jerjesCorrections import HEM_veto

ak.behavior.update(vector.behavior)
vec.register_awkward()
np.set_printoptions(precision=16, suppress=False)

def is_rootcompat(a):
    """Is it a flat or 1-d jagged array?"""
    t = ak.type(a)
    if isinstance(t, ak._ext.ArrayType):

        if isinstance(t.type, ak._ext.PrimitiveType):
            return True
        if isinstance(t.type, ak._ext.ListType) and isinstance(t.type.type, ak._ext.PrimitiveType):
            return True

    return False

def uproot_writeable(events, bnames):
    ev = {}

    for bname in bnames.keys():
        if hasattr(events, bname):
            if not events[bname].fields:
                # If no fields are present, keep the array as is
                ev[bname] = ak.packed(ak.without_parameters(events[bname]))
            else:
                b_fields = bnames[bname]
                if b_fields is None:
                    b_fields = events[bname].fields
                
                # Handle fields with their specific data types
                ev[bname] = ak.zip(
                    {
                        n: ak.packed(ak.without_parameters(events[bname][n]))
                        for n in b_fields
                        if hasattr(events[bname], n) and is_rootcompat(events[bname][n])
                    }
                )

            # Specifically check and preserve float32 fields
            for field_name in events[bname].fields:
                field_data = events[bname][field_name]
                if isinstance(field_data, ak.Array):
                    # Check if the element type is float32
                    if isinstance(field_data.type, ak.types.PrimitiveType) and field_data.type == ak.types.PrimitiveType('float32'):
                        # Convert to numpy float32 to ensure precision
                        ev[bname][field_name] = ak.Array(np.array(field_data, dtype=np.float32))
        else:
            print(f"ATTENTION!!! '{bname}' is not available in the field names.")

    return ev



def debug(debugModeON, *args):
    if debugModeON: print(*args)


class VLLSkim(processor.ProcessorABC):
    def __init__(self, debugMode=False, isMC=True, era='2018'):
        
        self.debugMode = debugMode
        self.isMC=isMC
        self.era=era
        
        dataset_axis = hist.axis.StrCategory([], growth=True, name="dataset", label="Dataset")
        cat_axis = hist.axis.StrCategory([], growth=True, name="cat", label="Catagory")
        pt_axis = hist.axis.Regular(20, 0.0, 200, name="pt", label=r"$p_{T}$ [GeV]")

        
        self.make_output = lambda: {
            "cutflow" : processor.defaultdict_accumulator( partial(processor.defaultdict_accumulator, int)),
        }

        
    @property
    def accumulator(self):
        return self._accumulator

    # @profile
    def process(self, events):

        dataset = events.metadata['dataset']
        output  = self.make_output()        
        
        sumgenW=0
        nproc=0
        output['cutflow'][dataset]['all'] += len(events)
        nproc=len(events)
        if self.isMC:
            output["cutflow"][dataset]["sum_genW"]+= ak.sum(events.genWeight, axis=0) 
            sumgenW= ak.sum(events.genWeight, axis=0) 
            events = ak.with_field(events, output["cutflow"][dataset]["sum_genW"], "sum_genW")

        ## ---------------------------------- Event Veto --------------------------------- #

        selection = PackedSelection()

        jets = events.Jet
        looseJet = jets[
            (abs(jets.eta) < 2.4)
            & (jets.pt > 15) 
            & ((jets.jetId >> 2 & 1) == 1) # tightLepVetoID
        ]

        cutjets = jets[ (abs(jets.eta) < 2.4) & (jets.pt > 15)]

        # Apply the HEM event veto 

        HEM_MCbool_1 = np.ones(round(len(events.MET.pt)*0.647724485), dtype=bool)
        HEM_MCbool_0 = ~np.ones(round(len(events.MET.pt)*(1.0-0.647724485)), dtype=bool)
        HEM_MCbool = np.concatenate((HEM_MCbool_1, HEM_MCbool_0))
        HEM_MCbool = ak.singletons(HEM_MCbool)

        if self.era == "2018":
            if self.isMC:
                HEM_cut = HEM_veto(isMC=self.isMC, nrun=np.ones(len(events)), HEMaffected=HEM_MCbool, obj=looseJet)
            else:
                HEM_cut = HEM_veto(isMC=self.isMC, nrun=events.run, HEMaffected=False, obj=looseJet)
        else:
            HEM_cut = np.ones(len(events.MET.pt), dtype=bool)

        del looseJet, HEM_MCbool_1, HEM_MCbool_0, HEM_MCbool

        #  Trigger
        if self.era == "2016pre":
            elehlt = events.HLT.Ele27_WPTight_Gsf
        elif self.era == "2016post":
            elehlt = events.HLT.Ele27_WPTight_Gsf
        elif self.era == "2017":
            elehlt = events.HLT.Ele32_WPTight_Gsf_L1DoubleEG
        else:            
            elehlt = events.HLT.Ele32_WPTight_Gsf

        if self.era == "2017":
            muhlt = events.HLT.IsoMu27
        elif self.era == "2018":
            muhlt = events.HLT.IsoMu24
        else:
            muhlt = events.HLT.IsoTkMu24 | events.HLT.IsoMu24
            
        elehltVeto = ak.fill_none(elehlt == False, True)

        #  Leptons
        tightMuons, looseMuons = selectMuons(events)
        tightElectrons, looseElectrons = selectElectronsMVA(events)

        # Selections for skim
        selection.add("trigger",( ((muhlt | elehlt) & (self.isMC)) | ( (elehlt & (not(self.isMC)) & (not "Mu" in dataset)) | (muhlt & elehltVeto & (not(self.isMC)) & ("Mu" in dataset)) ) ))        
        selection.add("twoLeps", ( (ak.num(tightMuons) + ak.num(tightElectrons)) > 1)) 
        selection.add("atLestFourJets", (ak.num(cutjets) > 3))
        selection.add("HEM_veto", HEM_cut)

        skimtest =  {"trigger","twoLeps","HEM_veto","atLestFourJets"}

        selection.add("skim", selection.all(*skimtest))  

        output["cutflow"][dataset]['trigger']+= selection.all('trigger').sum()
        output["cutflow"][dataset]['allCuts']+= selection.all('skim').sum()

        selectEvents = selection.all('skim')

        events = events[selectEvents]
        HEMveto = HEM_cut[selectEvents]

        events = ak.with_field(events, HEMveto , "HEMveto")
        events = ak.with_field(events, output['cutflow'][dataset]['all'], "n_processed")

        if (len(events) < 1 ):
            print("No events passed the cuts.")
            return  {dataset:output}

        del selectEvents
        
        output['cutflow'][dataset]['allCutslen'] += len(events)
        
        ## -------------------------- Write root TTree ---------------------------#    

        if self.isMC:
            keys={
                'Electron': ['charge', 'dxy', 'dz', 'eta','mass', 'phi', 'pt','mvaFall17V2Iso_WP90', 'mvaFall17V2Iso_WPL','mvaFall17V2Iso_WP80','cutBased','pfRelIso03_all'],
                'Muon':['charge', 'dxy', 'dxyErr', 'dxybs', 'dz', 'dzErr', 'eta', 'mvaId', 'isGlobal', 'isPFcand', 'isTracker', 'mass', 'mediumId','looseId','pdgId', 'pfRelIso04_all','phi', 'pt', 'ptErr', 'tightId'],
                'Jet':['pt', 'phi', 'eta', 'mass', 'jetId','btagDeepFlavB','rawFactor','area','hadronFlavour','genJetIdx','puId'],
                'GenJet': ['pt','eta', 'phi','mass','partonFlavour','hadronFlavour'],
                'MET': ['pt','phi','MetUnclustEnUpDeltaX','MetUnclustEnUpDeltaY'],
                'HLT':["Ele27_WPTight_Gsf", "Ele32_WPTight_Gsf", "Ele32_WPTight_Gsf_L1DoubleEG" ,"IsoTkMu24", "IsoMu27", "IsoMu24" ],
                'fixedGridRhoFastjetAll': None,                
                'PV': None,
                'Flag':None,
                'event': None,
                'luminosityBlock': None,
                'run': None,
                'GenPart': None,
                'Pileup': None,
                'genWeight': None,
                'PSWeight': None,
                'LHEWeight': None,
                'Generator': None,
                'LHEPdfWeight': None,
                'genTtbarId':None,
                'L1PreFiringWeight':None,
                'LHE': ['Njets'],
                'n_processed':None,
                'sum_genW':None,
                'HEMveto':None,
            }
        else:
            keys = {
                'Electron': ['charge', 'dxy', 'dz', 'eta','mass', 'phi', 'pt','mvaFall17V2Iso_WP90', 'mvaFall17V2Iso_WPL','mvaFall17V2Iso_WP80','cutBased','pfRelIso03_all'],
                'Muon':['charge', 'dxy', 'dxyErr', 'dxybs', 'dz', 'dzErr', 'eta', 'mvaId', 'isGlobal', 'isPFcand', 'isTracker', 'mass', 'mediumId','looseId','pdgId', 'pfRelIso04_all','phi', 'pt', 'ptErr', 'tightId'],
                'Jet':['pt', 'phi', 'eta', 'mass', 'jetId','btagDeepFlavB','rawFactor','area','puId'],
                'HLT':["Ele27_WPTight_Gsf", "Ele32_WPTight_Gsf", "Ele32_WPTight_Gsf_L1DoubleEG" ,"IsoTkMu24", "IsoMu27", "IsoMu24" ],
                'fixedGridRhoFastjetAll': None,
                'MET': ['pt','phi','MetUnclustEnUpDeltaX','MetUnclustEnUpDeltaY'],
                'PV': None,
                'Flag':None,
                'event': None,
                'luminosityBlock': None,
                'run': None,
                'n_processed':None,
                'HEMveto':None,
            }

        filename= events.metadata['filename'].split('/')[-1].split('.')[0]+ '_' + str(time.time()) + '.root'
        print("filename: ", filename)
        subdirs = []
        subdirs.append(f'{events.metadata["dataset"]}')

        location = "root://cmsxrootd.hep.wisc.edu//store/user/ckoraka/vll/skims/"+self.era+"/"
        xrootd=True
        merged_subdirs = "/".join(subdirs) if xrootd else os.path.sep.join(subdirs)
        destination = (
            location + merged_subdirs + f"/{filename}"
            if xrootd
            else os.path.join(location, os.path.join(merged_subdirs, fname))
        )


        with uproot.create(filename, compression=None) as file:
            file["Events"] = uproot_writeable(events, keys)
            
        xrd_prefix = "root://"
        pfx_len = len(xrd_prefix)
        copyproc = XRootD.client.CopyProcess()

        copyproc.add_job(source = os.path.abspath(os.path.join(".", filename)),
                         target = destination, force=True)
        copyproc.prepare()
        copyproc.run()
        client = XRootD.client.FileSystem("root://cmsxrootd.hep.wisc.edu/")

        status = client.stat(
            destination[destination[pfx_len:].find("/") + pfx_len + 1 :],
            XRootD.client.flags.OpenFlags.READ,
        )

        assert status[0].ok
        del client
        del copyproc
        del file
        del keys
        
        try:
            os.remove(os.path.abspath(os.path.join(".", filename)))
            print("% s removed successfully" % os.path.abspath(os.path.join(".", filename)))
        except OSError as error:
            print(error)
            print("File path can not be removed")

        return  {dataset:output}
        
    def postprocess(self, accumulator):
        return accumulator

