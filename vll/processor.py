from typing import Any, Dict, List, Optional
import pathlib
import shutil

import coffea.processor as processor
from coffea.analysis_tools import PackedSelection, Weights
from coffea.lumi_tools import LumiMask
from coffea.nanoevents import NanoAODSchema
from coffea.nanoevents.methods import nanoaod

import hist

NanoAODSchema.warn_missing_crossrefs = False

import awkward as ak
import numpy as np
import pandas

from vll.corrections import *

from mt2 import mt2
import tensorflow as tf

class VLLProcessor(processor.ProcessorABC):

    # ############################################################################################
    # Taken from https://github.com/lgray/hgg-coffea/blob/master/src/hgg_coffea/workflows/base.py 
    # Adapted to the analysis
    # Used to store variables of interest for data-driven background prediction and debugging
    # ############################################################################################

    def dump_awkward(
        self,
        pddf: ak.Array,
        fname: str,
        location: str,
        subdirs: Optional[List[str]] = None,
    ) -> None:
        subdirs = subdirs or []
        xrd_prefix = "root://"
        pfx_len = len(xrd_prefix)
        xrootd = False
        if xrd_prefix in location:
            try:
                import XRootD
                import XRootD.client

                xrootd = True
            except ImportError as err:
                raise ImportError(
                    "Install XRootD python bindings with: conda install -c conda-forge xroot"
                ) from err
        local_file = (
            os.path.abspath(os.path.join(".", fname))
            if xrootd
            else os.path.join(".", fname)
        )
        merged_subdirs = "/".join(subdirs) if xrootd else os.path.sep.join(subdirs)
        destination = (
            location + merged_subdirs + f"/{fname}"
            if xrootd
            else os.path.join(location, os.path.join(merged_subdirs, fname))
        )

        ak.to_parquet(pddf,local_file)

        if xrootd:
            copyproc = XRootD.client.CopyProcess()
            copyproc.add_job(local_file, destination)
            copyproc.prepare()
            copyproc.run()
            client = XRootD.client.FileSystem(
                location[: location[pfx_len:].find("/") + pfx_len]
            )
            status = client.locate(
                destination[destination[pfx_len:].find("/") + pfx_len + 1 :],
                XRootD.client.flags.OpenFlags.READ,
            )
            assert status[0].ok
            del client
            del copyproc
        else:
            dirname = os.path.dirname(destination)
            if not os.path.exists(dirname):
                pathlib.Path(dirname).mkdir(parents=True, exist_ok=True)
            shutil.copy(local_file, destination)
            assert os.path.isfile(destination)
        pathlib.Path(local_file).unlink()


    def __init__(self, isMC=False, era="2017"):
        ################################
        # INITIALIZE COFFEA PROCESSOR
        ################################ 

        ak.behavior.update(nanoaod.behavior)
        self.isMC = isMC
        self.era = era

        # Categories
        dataset_axis = hist.axis.StrCategory([], growth=True, name="dataset", label="Dataset")
        lep_axis = hist.axis.StrCategory([], growth=True, name="lepFlavor", label="Lepton Flavor")
        btag_axis = hist.axis.StrCategory([], growth=True,name="bJetMultiplicity", label="bJet Multiplicity")
        systematic_axis = hist.axis.StrCategory([], growth=True, name="systematic", label="Systematic Uncertainty")
        vll_mass_axis = hist.axis.StrCategory([], growth=True, name="VLLmass", label="VLL mass")
        tau_axis = hist.axis.StrCategory([], growth=True, name="tauMultiplicity", label="Tau Multiplicity")

        # ht_axis = hist.axis.StrCategory([], growth=True,name="HTregion", label="HT region")
        # vll_dnn_axis = hist.axis.StrCategory([], growth=True, name="dnnReg", label="DNN region")
        # lepcharge_axis = hist.axis.StrCategory([], growth=True,name="dileptonCharge", label="Dilepton Charge")

        # Variables
        pt_axis = hist.axis.Regular(150, 0.0, 1500, name="pt", label=r"$p_{T}$ [GeV]",underflow=False, overflow=True)
        dr_axis = hist.axis.Regular(60, 0.0, 6.0, name="deltaR", label=r"$DR_{l,l}$ [GeV]",underflow=False, overflow=True)
        met_pt_axis = hist.axis.Regular(70, 0.0, 700, name="met", label=r"MET [GeV]",underflow=False, overflow=True)
        met_phi_axis = hist.axis.Regular(64, -3.2, 3.2, name="metPhi", label=r"MET phi",underflow=False, overflow=True)
        lep_pt_axis = hist.axis.Regular(80, 0.0, 800, name="lep_pt", label=r"$p_{T}$ [GeV]",underflow=False, overflow=True)
        lep_mll_axis = hist.axis.Regular(80, 0.0, 800, name="lep_mass", label=r"$m_{ll}$ [GeV]",underflow=False, overflow=True)
        event_HT_axis = hist.axis.Regular(300, 0.0, 3000, name="event_HT", label=r"$H_{T}$ [GeV]",underflow=False, overflow=True)
        event_MT_axis = hist.axis.Regular(200, 0.0, 200, name="event_MT", label=r"$M_{T}$ [GeV]",underflow=False, overflow=True)
        multiplicity_axis = hist.axis.Regular(12, 0.0, 12, name="njets", label=r"Number of jets",underflow=False, overflow=True)
        ratio_axis = hist.axis.Regular(100, 0.0, 1., name="ratio", label=r"Ratio",underflow=False, overflow=True)
        dnn_axis = hist.axis.Regular(100, 0.0, 1., name="dnn", label=r"DNN",underflow=False, overflow=True)
        deltaR_bjetLep_axis = hist.axis.Regular(60, 0.0, 6.0, name="deltaR_bjetLep", label=r"$\Delta R(b, \ell)$", underflow=False, overflow=True)
        min_dr_lepb_axis = hist.axis.Regular(60, 0.0, 6, name="min_dr_lepb", label=r"$\min(\Delta\phi(b, \ell))$", underflow=False, overflow=True)
        max_dr_lepb_axis = hist.axis.Regular(60, 0.0, 6, name="max_dr_lepb", label=r"$\max(\Delta\phi(b, \ell))$", underflow=False, overflow=True)
        lepCharge_axis = hist.axis.Regular(5, -2, 2, name="lepCharge", label="Lepton Charge Sum", underflow=False, overflow=True)
    
        cut = hist.axis.Regular(14, 0, 14, name="cut", label=r"Cutflow")

        # Book histos
        self.make_output = lambda: {
            "EventCount": processor.value_accumulator(int),
            "MET_pt": hist.Hist(dataset_axis, met_pt_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "MET_phi": hist.Hist(dataset_axis, met_phi_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "lead_lep_pt": hist.Hist(dataset_axis,lep_pt_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "sublead_lep_pt": hist.Hist(dataset_axis,lep_pt_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "lep_mll": hist.Hist(dataset_axis,lep_mll_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "dilep_dr": hist.Hist(dataset_axis,dr_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "event_HT": hist.Hist(dataset_axis,event_HT_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "event_bjet_HT": hist.Hist(dataset_axis,event_HT_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "event_MT": hist.Hist(dataset_axis,event_MT_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "event_ratioJetHT": hist.Hist(dataset_axis,ratio_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "event_deltaPhiJetMet": hist.Hist(dataset_axis,dr_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "nJets": hist.Hist(dataset_axis,multiplicity_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "nbJets": hist.Hist(dataset_axis,multiplicity_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "lead_jet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "sub_jet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "lead_bjet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "sub_bjet_pT": hist.Hist(dataset_axis,pt_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "event_ST": hist.Hist(dataset_axis,event_HT_axis,lep_axis,btag_axis,tau_axis,systematic_axis,),
            "deltaR_bjetLep": hist.Hist(dataset_axis, deltaR_bjetLep_axis, lep_axis, btag_axis,tau_axis,systematic_axis),
            "min_dr_lepb": hist.Hist(dataset_axis, min_dr_lepb_axis, lep_axis, btag_axis,tau_axis,systematic_axis),
            "max_dr_lepb": hist.Hist(dataset_axis, max_dr_lepb_axis, lep_axis, btag_axis, tau_axis,systematic_axis),
            "lepCharge": hist.Hist(dataset_axis, lepCharge_axis, lep_axis, btag_axis,tau_axis, systematic_axis),

            "dnn_vll": hist.Hist(dataset_axis,dnn_axis,lep_axis,btag_axis,vll_mass_axis,tau_axis,systematic_axis,),

            "CutFlow": hist.Hist(dataset_axis,cut,),
        }

    @property
    def accumulator(self):
        return self._accumulator

    def process(self, events):
        shift_systs = [None]
        dataset = events.metadata["dataset"]

        if not self.isMC:
            if "runA_2018" in dataset: RunEra="RunA"
            elif "runB_2018" in dataset: RunEra="RunB"
            elif "runC_2018" in dataset: RunEra="RunC"
            elif "runD_2018" in dataset: RunEra="RunD"
            elif "runB_2017" in dataset: RunEra="RunB"
            elif "runC_2017" in dataset: RunEra="RunC"
            elif "runD_2017" in dataset: RunEra="RunD"
            elif "runE_2017" in dataset: RunEra="RunE"
            elif "runF_2017" in dataset: RunEra="RunF"
            elif "runBver2_2016" in dataset or "runBver1_2016" in dataset or "runC_2016" in dataset or "runD_2016" in dataset: RunEra="RunBCD"
            elif "runE_2016" in dataset or "runF-HIMP_2016" in dataset: RunEra="RunEF"
            elif "runF_2016" in dataset or "runG_2016" in dataset or "runH_2016" in dataset: RunEra="RunFGH"
        else:
            RunEra=None    

        events["Jet", "pt_raw"] = (1 - events.Jet.rawFactor) * events.Jet.pt
        events["Jet", "mass_raw"] = (1 - events.Jet.rawFactor) * events.Jet.mass
        events["Jet", "rho"] = ak.broadcast_arrays(events.fixedGridRhoFastjetAll, events.Jet.pt)[0]
        if self.isMC:
            events["Jet", "pt_gen"] = ak.values_astype(ak.fill_none(events.Jet.matched_gen.pt, 0), np.float32)
        events_cache = events.caches[0]

        # Corrected jets
        jet_factory = jerjesCorrections.get_jet_factory(isMC=self.isMC, era=self.era, RUN=RunEra, split_JECunc_sources=True, giveMetVar=False)
        corrected_jets = jet_factory.build(events.Jet, lazy_cache=events_cache)
        del jet_factory

        # Chose which MET to use
        # https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD#MET
        # https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETRun2Corrections#Type_I_Correction_Propagation_of
        # https://github.com/CoffeaTeam/coffea/blob/master/src/coffea/jetmet_tools/CorrectedMETFactory.py
        met_factory = jerjesCorrections.get_jet_factory(isMC=self.isMC, era=self.era, RUN=RunEra, split_JECunc_sources=True, giveMetVar=True)
        metCorrJets = corrected_jets[((corrected_jets.pt >15.) & (corrected_jets.jetId>2))]
        corrected_met = met_factory.build(events.MET, metCorrJets, lazy_cache=events_cache)
        del met_factory, metCorrJets


        if False:
            print(" pt ", events.Jet.pt[0], "  pt corrected ", jets.pt[0])
            print(" ")

        # if self.isMC:
            # shift_systs += ["JESUp", "JESDown", "JERUp", "JERDown"]
            # shift_systs += ["JES_AbsoluteUp", "JES_BBEC1Up", "JES_EC2Up", "JES_FlavorQCDUp", "JES_HFUp", "JES_RelativeBalUp",
            #               "JES_AbsoluteDown", "JES_BBEC1Down", "JES_EC2Down", "JES_FlavorQCDDown", "JES_HFDown", "JES_RelativeBalDown",
            #               "JES_Absolute_"+self.era+"Up", "JES_BBEC1_"+self.era+"Up", "JES_EC2_"+self.era+"Up", "JES_HF_"+self.era+"Up", "JES_RelativeSample_"+self.era+"Up", 
            #                "JES_Absolute_"+self.era+"Down", "JES_BBEC1_"+self.era+"Down", "JES_EC2_"+self.era+"Down", "JES_HF_"+self.era+"Down", "JES_RelativeSample_"+self.era+"Down",]

        model = tf.keras.models.load_model('vll/data/model.h5')

        return processor.accumulate(self.process_shift(events, corrected_jets, corrected_met, model, name) for name in shift_systs)

    def process_shift(self, events, corrected_jets, corrected_met, model, shift_syst=None):

        dataset = events.metadata["dataset"]
        output = self.make_output()
        era = self.era

        if shift_syst is None:
            output["EventCount"] = len(events)

        # Create a processor Weights object, with the same length as the number of events in the chunk
        weights = Weights(len(events))
        weights.add("NoWeight", weight=np.ones(len(events)))

        ##################
        # OBJECT SELECTION
        ##################

        ## MET object
        met = corrected_met

        # muon and electron selections are broken out into standalone functions
        tightMuons, looseMuons = selectMuons(events)
        # tightElectrons, looseElectrons = selectElectrons(events)
        # Check efficiency with MVA
        #tightMuons, looseMuons = selectMuonsMVA(events)
        tightElectrons, looseElectrons = selectElectronsMVA(events)
        mediumTaus, looseTaus, verylooseTaus = selectTaus(events)

        ## Variations
        if shift_syst == "JERUp":
            jets = corrected_jets.JER.up 
            met = corrected_met.JER.up
        elif shift_syst == "JERDown":
            jets = corrected_jets.JER.down
            met = corrected_met.JER.down
        elif shift_syst == "JESUp":
            jets = corrected_jets.JES_jes.up
            met = corrected_met.JES_jes.up           
        elif shift_syst == "JESDown":
            jets = corrected_jets.JES_jes.down
            met = corrected_met.JES_jes.down
        #--------------------
        # JES 11 sources:
        #--------------------
        elif shift_syst == "JES_AbsoluteUp":
            jets = corrected_jets.JES_Absolute.up
            met = corrected_met.JES_Absolute.up
        elif shift_syst == "JES_AbsoluteDown":
            jets = corrected_jets.JES_Absolute.down
            met = corrected_met.JES_Absolute.down
        elif shift_syst == "JES_BBEC1Up":
            jets = corrected_jets.JES_BBEC1.up
            met = corrected_met.JES_BBEC1.up
        elif shift_syst == "JES_BBEC1Down":
            jets = corrected_jets.JES_BBEC1.down
            met = corrected_met.JES_BBEC1.down
        elif shift_syst == "JES_EC2Up":
            jets = corrected_jets.JES_EC2.up
            met = corrected_met.JES_EC2.up
        elif shift_syst == "JES_EC2Down":
            jets = corrected_jets.JES_EC2.down
            met = corrected_met.JES_EC2.down
        elif shift_syst == "JES_FlavorQCDUp":
            jets = corrected_jets.JES_FlavorQCD.up
            met = corrected_met.JES_FlavorQCD.up
        elif shift_syst == "JES_FlavorQCDDown":
            jets = corrected_jets.JES_FlavorQCD.down
            met = corrected_met.JES_FlavorQCD.down
        elif shift_syst == "JES_HFUp":
            jets = corrected_jets.JES_HF.up
            met = corrected_met.JES_HF.up
        elif shift_syst == "JES_HFDown":
            jets = corrected_jets.JES_HF.down
            met = corrected_met.JES_HF.down
        elif shift_syst == "JES_RelativeBalUp":
            jets = corrected_jets.JES_RelativeBal.up
            met = corrected_met.JES_RelativeBal.up
        elif shift_syst == "JES_RelativeBalDown":
            jets = corrected_jets.JES_RelativeBal.down
            met = corrected_met.JES_RelativeBal.down

        #Uncorrelated sources (5 per year) 
        elif shift_syst == "JES_Absolute_"+era+"Up":
            if era == "2017":
                jets = corrected_jets.JES_Absolute_2017.up
                met = corrected_met.JES_Absolute_2017.up
            elif era == "2018":
                jets = corrected_jets.JES_Absolute_2018.up
                met = corrected_met.JES_Absolute_2018.up
            elif era == "2016pre":
                jets = corrected_jets.JES_Absolute_2016.up
                met = corrected_met.JES_Absolute_2016.up
            elif era == "2016post":
                jets = corrected_jets.JES_Absolute_2016.up
                met = corrected_met.JES_Absolute_2016.up

        elif shift_syst == "JES_Absolute_"+era+"Down":
            if era == "2017":
                jets = corrected_jets.JES_Absolute_2017.down
                met = corrected_met.JES_Absolute_2017.down
            elif era == "2018":
                jets = corrected_jets.JES_Absolute_2018.down
                met = corrected_met.JES_Absolute_2018.down
            elif era == "2016pre":
                jets = corrected_jets.JES_Absolute_2016.down
                met = corrected_met.JES_Absolute_2016.down
            elif era == "2016post":
                jets = corrected_jets.JES_Absolute_2016.down
                met = corrected_met.JES_Absolute_2016.down

        elif shift_syst == "JES_BBEC1_"+era+"Up":
            if era == "2017":
                jets = corrected_jets.JES_BBEC1_2017.up
                met = corrected_met.JES_BBEC1_2017.up
            elif era == "2018":
                jets = corrected_jets.JES_BBEC1_2018.up
                met = corrected_met.JES_BBEC1_2018.up
            elif era == "2016pre":
                jets = corrected_jets.JES_BBEC1_2016.up
                met = corrected_met.JES_BBEC1_2016.up
            elif era == "2016post":
                jets = corrected_jets.JES_BBEC1_2016.up
                met = corrected_met.JES_BBEC1_2016.up

        elif shift_syst == "JES_BBEC1_"+era+"Down":
            if era == "2017":
                jets = corrected_jets.JES_BBEC1_2017.down
                met = corrected_met.JES_BBEC1_2017.down
            elif era == "2018":
                jets = corrected_jets.JES_BBEC1_2018.down
                met = corrected_met.JES_BBEC1_2018.down
            elif era == "2016pre":
                jets = corrected_jets.JES_BBEC1_2016.down
                met = corrected_met.JES_BBEC1_2016.down
            elif era == "2016post":
                jets = corrected_jets.JES_BBEC1_2016.down
                met = corrected_met.JES_BBEC1_2016.down

        elif shift_syst == "JES_EC2_"+era+"Up":
            if era == "2017":
                jets = corrected_jets.JES_EC2_2017.up
                met = corrected_met.JES_EC2_2017.up
            elif era == "2018":
                jets = corrected_jets.JES_EC2_2018.up
                met = corrected_met.JES_EC2_2018.up
            elif era == "2016pre":
                jets = corrected_jets.JES_EC2_2016.up
                met = corrected_met.JES_EC2_2016.up
            elif era == "2016post":
                jets = corrected_jets.JES_EC2_2016.up
                met = corrected_met.JES_EC2_2016.up

        elif shift_syst == "JES_EC2_"+era+"Down":
            if era == "2017":
                jets = corrected_jets.JES_EC2_2017.down
                met = corrected_met.JES_EC2_2017.down
            elif era == "2018":
                jets = corrected_jets.JES_EC2_2018.down
                met = corrected_met.JES_EC2_2018.down
            elif era == "2016pre":
                jets = corrected_jets.JES_EC2_2016.down
                met = corrected_met.JES_EC2_2016.down
            elif era == "2016post":
                jets = corrected_jets.JES_EC2_2016.down
                met = corrected_met.JES_EC2_2016.down

        elif shift_syst == "JES_HF_"+era+"Up":
            if era == "2017":
                jets = corrected_jets.JES_HF_2017.up
                met = corrected_met.JES_HF_2017.up
            elif era == "2018":
                jets = corrected_jets.JES_HF_2018.up
                met = corrected_met.JES_HF_2018.up
            elif era == "2016pre":
                jets = corrected_jets.JES_HF_2016.up
                met = corrected_met.JES_HF_2016.up
            elif era == "2016post":
                jets = corrected_jets.JES_HF_2016.up
                met = corrected_met.JES_HF_2016.up

        elif shift_syst == "JES_HF_"+era+"Down":
            if era == "2017":
                jets = corrected_jets.JES_HF_2017.down
                met = corrected_met.JES_HF_2017.down
            elif era == "2018":
                jets = corrected_jets.JES_HF_2018.down
                met = corrected_met.JES_HF_2018.down   
            elif era == "2016pre":
                jets = corrected_jets.JES_HF_2016.down
                met = corrected_met.JES_HF_2016.down
            elif era == "2016post":
                jets = corrected_jets.JES_HF_2016.down
                met = corrected_met.JES_HF_2016.down   

        elif shift_syst == "JES_RelativeSample_"+era+"Up":
            if era == "2017":
                jets = corrected_jets.JES_RelativeSample_2017.up
                met = corrected_met.JES_RelativeSample_2017.up
            elif era == "2018":
                jets = corrected_jets.JES_RelativeSample_2018.up
                met = corrected_met.JES_RelativeSample_2018.up
            elif era == "2016pre":
                jets = corrected_jets.JES_RelativeSample_2016.up
                met = corrected_met.JES_RelativeSample_2016.up
            elif era == "2016post":
                jets = corrected_jets.JES_RelativeSample_2016.up
                met = corrected_met.JES_RelativeSample_2016.up

        elif shift_syst == "JES_RelativeSample_"+era+"Down":
            if era == "2017":
                jets = corrected_jets.JES_RelativeSample_2017.down
                met = corrected_met.JES_RelativeSample_2017.down
            elif era == "2018":
                jets = corrected_jets.JES_RelativeSample_2018.down
                met = corrected_met.JES_RelativeSample_2018.down
            elif era == "2016pre":
                jets = corrected_jets.JES_RelativeSample_2016.down
                met = corrected_met.JES_RelativeSample_2016.down
            elif era == "2016post":
                jets = corrected_jets.JES_RelativeSample_2016.down
                met = corrected_met.JES_RelativeSample_2016.down
        else:
            jets = corrected_jets
            met = corrected_met

        # For Data and MC, apply MET Phi modulation correction
        # Twiki link: https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETRun2Corrections#xy_Shift_Correction_MET_phi_modu 

        if self.isMC==False:
            corr_MET_pt, corr_MET_phi = jerjesCorrections.get_polar_corrected_MET(runera=dataset, npv=events.PV.npvs, met_pt=met.pt, met_phi=met.phi)

        if self.isMC==True:
            corr_MET_pt, corr_MET_phi = jerjesCorrections.get_polar_corrected_MET(runera=era, npv=events.PV.npvs, met_pt=met.pt, met_phi=met.phi)

        met["pt"] = corr_MET_pt
        met["phi"] = corr_MET_phi
        del corr_MET_pt, corr_MET_phi

        ############################################################################
        ## More cross-cleaning: check jet does not overlap with our selected leptons
        ############################################################################
        
        jetMuMask = ak.all(jets.metric_table(tightMuons) > 0.4, axis=-1)
        jetEleMask = ak.all(jets.metric_table(tightElectrons) > 0.4, axis=-1)
        jetTauMask = ak.all(jets.metric_table(mediumTaus) > 0.4, axis=-1)


        tightJet = jets[(abs(jets.eta) < 2.4)
            & (jets.pt > 30) 
            & ((jets.jetId >> 1 & 1) == 1) # tightID
            & (jetMuMask)
            & (jetEleMask)
            & (jetTauMask)  # tau
            & ((jets.pt > 50) | (jets.puId>0))
        ]

        looseJet = jets[(abs(jets.eta) < 2.4)
            & (jets.pt > 15) 
            & ((jets.jetId >> 2 & 1) == 1) # tightLepVetoID
        ]

        del jets

        # label the subset of tightJet which pass the DeepJet tagger
        # https://btv-wiki.docs.cern.ch/ScaleFactors/#sf-campaigns

        bTagWP = 0.3040  # 2017 DeepJet working point
        bTagWP_T = 0.7476
        if self.era == "2018":
            bTagWP = 0.2783 
            bTagWP_T = 0.7100  
        if self.era == "2016pre":
            bTagWP = 0.2598 
            bTagWP_T = 0.6502
        if self.era == "2016post":
            bTagWP = 0.2489  
            bTagWP_T = 0.6377  

        tightJet["btagged"] = tightJet.btagDeepFlavB > bTagWP 
        tightJet["btagged_T"] = tightJet.btagDeepFlavB > bTagWP_T 

        # Sorted leptons pT 
        leadingMuon = ak.firsts(tightMuons)
        leadingElectron = ak.firsts(tightElectrons)
        leptons = ak.concatenate((tightMuons,tightElectrons),axis = 1)
        leptons = leptons[ak.argsort(-leptons.pt, axis=1)]
        dileptons = leptons.mask[ak.num(leptons) >= 2][:,0] + leptons.mask[ak.num(leptons) >= 2][:,1]

        # Usefull jet dict for applying cuts
        fourJets = tightJet.mask[ak.num(tightJet) >= 4]
        bjets = tightJet[tightJet.btagged]
        bjets_T = tightJet[tightJet.btagged_T]
        leadingbjet = ak.firsts(bjets)

        ##################
        # EVENT VARIABLES
        ##################
        event_HT = ak.sum(tightJet.pt,-1)
        event_bHT = ak.sum(tightJet.pt[tightJet.btagged],-1)
        event_ST = ak.sum(tightJet.pt,-1) + met.pt + ak.sum(leptons.pt,-1)
        l1 = leptons.mask[ak.num(leptons) >= 2][:,0]
        l2 = leptons.mask[ak.num(leptons) >= 2][:,1]
        dilep_DR = np.sqrt((l1.eta - l2.eta) ** 2 + (l1.phi - l2.phi) ** 2,where=True,)
        nbJetsT = ak.num(bjets_T)

        # Transverse angle difference between each object and MET
        # delta_phi_leptons = np.abs(leptons.phi - met.phi)
        # delta_phi_leptons = np.minimum(delta_phi_leptons, 2 * np.pi - delta_phi_leptons)
        # delta_phi_jets = np.abs(tightJet.phi - met.phi)
        # delta_phi_jets = np.minimum(delta_phi_jets, 2 * np.pi - delta_phi_jets)
        # sum_cos_leptons = ak.sum(leptons.pt * np.cos(delta_phi_leptons), axis=-1) 
        # sum_cos_jets = ak.sum(tightJet.pt * np.cos(delta_phi_jets), axis=-1)
        # event_ETM = np.sqrt(event_ST**2 - (sum_cos_leptons+sum_cos_jets)**2)

        # mt2ll 
        event_MLL = mt2(np.abs(l1.mass), l1.pt*np.cos(l1.phi), l1.pt * np.sin(l1.phi),
                        np.abs(l2.mass), l2.pt*np.cos(l2.phi), l2.pt * np.sin(l2.phi),
                        met.pt * np.cos(met.phi), met.pt * np.sin(met.phi), 
                        0,0)  

        j1 = bjets.mask[ak.num(bjets) >= 2][:,0]
        j2 = bjets.mask[ak.num(bjets) >= 2][:,1]
        event_mt2_jj = mt2(np.abs(j1.mass), j1.pt*np.cos(j1.phi), j1.pt * np.sin(j1.phi),
                        np.abs(j2.mass), j2.pt*np.cos(j2.phi), j2.pt * np.sin(j2.phi),
                        met.pt * np.cos(met.phi), met.pt * np.sin(met.phi), 
                        0,0)          
        del j1,j2

        # Ratio of leading b-jet pT over HT
        ratioJetHT = leadingbjet.pt / event_HT
        ratioMETHT =  met.pt / event_HT

        # DeltaPhi b-jet and MET
        deltaPhibJetMet = np.abs(leadingbjet.phi - met.phi)
        deltaPhibJetMet = np.minimum(deltaPhibJetMet, 2 * np.pi - deltaPhibJetMet)

        # Minimum DR between b-jets
        pairs = ak.combinations(bjets, 2)
        phi_1 = pairs['0'].phi 
        phi_2 = pairs['1'].phi  
        eta_1 = pairs['0'].eta 
        eta_2 = pairs['1'].eta  
        phi_diff = phi_1 - phi_2
        eta_diff = eta_1 - eta_2
        delta_phi = np.arctan2(np.sin(phi_diff), np.cos(phi_diff))
        delta_r = np.sqrt(eta_diff**2 + delta_phi**2)
        min_dr = ak.min(delta_r, axis=-1) 
      
        # DR between leading lepton and leading b-jet 
        phi_diff = leadingbjet.phi - l1.phi
        eta_diff = leadingbjet.eta - l1.eta
        delta_phi = np.arctan2(np.sin(phi_diff), np.cos(phi_diff))
        deltaR_bjetLep = np.sqrt(eta_diff**2 + delta_phi**2)

        # DR min and max between b-jet and lepton
        pairs = ak.cartesian([bjets, leptons])
        phi_1 = pairs['0'].phi 
        phi_2 = pairs['1'].phi  
        eta_1 = pairs['0'].eta 
        eta_2 = pairs['1'].eta  
        phi_diff = phi_1 - phi_2        
        delta_phi = np.arctan2(np.sin(phi_diff), np.cos(phi_diff))
        eta_diff = eta_1 - eta_2
        delta_r = np.sqrt(eta_diff**2 + delta_phi**2)
        min_dr_lepb = ak.min(delta_r, axis=-1) 
        max_dr_lepb = ak.max(delta_r, axis=-1) 

        del phi_1, phi_2, eta_1, eta_2, phi_diff, delta_phi

        # Total charge 
        lepCharge = ak.sum(leptons.charge,axis=-1)

        #####################
        # DNN EVALUATION
        #####################
        min_bjet_dr = min_dr
        dileptonMass = dileptons.mass
        event_MET = met.pt
        #evaluation_branches = ["event_HT", "event_bHT", "event_MLL", "ratioJetHT","deltaPhibJetMet", "min_bjet_dr", "deltaR_bjetLep","min_dr_lepb", "max_dr_lepb", "lepCharge","dileptonMass", "event_MET"]
        #evaluation_branches = ["event_HT","event_bHT", "event_MLL", "ratioJetHT","deltaPhibJetMet", "min_bjet_dr", "deltaR_bjetLep", "min_dr_lepb", "max_dr_lepb", "nJets","dileptonMass", "event_MET",]
        evaluation_branches = ["event_HT","event_bHT", "event_MLL", "ratioJetHT","deltaPhibJetMet", "min_bjet_dr", "deltaR_bjetLep", "min_dr_lepb", "max_dr_lepb", "nJets","dileptonMass", "event_MET", "mass_point"]

        event_HT_np = ak.to_numpy(event_HT, allow_missing=True)
        event_bHT_np = ak.to_numpy(event_bHT, allow_missing=True)
        event_MLL_np = ak.to_numpy(event_MLL, allow_missing=True)
        ratioJetHT_np = ak.to_numpy(ratioJetHT, allow_missing=True)
        deltaPhibJetMet_np = ak.to_numpy(deltaPhibJetMet, allow_missing=True)
        min_bjet_dr_np = ak.to_numpy(min_bjet_dr, allow_missing=True)
        deltaR_bjetLep_np = ak.to_numpy(deltaR_bjetLep, allow_missing=True)
        min_dr_lepb_np = ak.to_numpy(min_dr_lepb, allow_missing=True)
        max_dr_lepb_np = ak.to_numpy(max_dr_lepb, allow_missing=True)
        lepCharge_np = ak.to_numpy(lepCharge, allow_missing=True)
        dileptonMass_np = ak.to_numpy(dileptonMass, allow_missing=True)
        nJets_np = ak.to_numpy(ak.num(tightJet), allow_missing=True)


        all_predictions = {}
        
        mass_values = np.arange(500, 1050, 50)

        for mass in mass_values:
            #x_data = np.column_stack([event_HT, event_bHT_np, event_MLL_np, ratioJetHT_np, deltaPhibJetMet_np,min_bjet_dr_np, deltaR_bjetLep_np, min_dr_lepb_np, max_dr_lepb_np, lepCharge_np, dileptonMass_np, event_MET])
            #x_data = np.column_stack([event_HT_np, event_bHT_np, event_MLL_np, ratioJetHT_np, deltaPhibJetMet_np, min_bjet_dr_np, deltaR_bjetLep_np, min_dr_lepb_np, max_dr_lepb_np, nJets_np, dileptonMass_np, event_MET])
            x_data = np.column_stack([event_HT_np, event_bHT_np, event_MLL_np, ratioJetHT_np, deltaPhibJetMet_np, min_bjet_dr_np, deltaR_bjetLep_np, min_dr_lepb_np, max_dr_lepb_np, nJets_np, dileptonMass_np, event_MET, np.full_like(event_HT_np, mass)])

            x_data_df = pandas.DataFrame(x_data, columns=evaluation_branches)
            norm_data = pandas.read_csv("vll/data/variable_norm.csv")
            for variable in norm_data['Unnamed: 0']:
                mean = norm_data[norm_data['Unnamed: 0'] == variable]['mu'].values[0]
                std = norm_data[norm_data['Unnamed: 0'] == variable]['std'].values[0]
                x_data_df[variable] = (x_data_df[variable] - mean) / std

            predictions = model.predict(x_data_df,verbose=0)
            predictions = predictions.flatten()
            predictions.squeeze()
            all_predictions[mass] = predictions

        del event_bHT_np, event_MLL_np, ratioJetHT_np, deltaPhibJetMet_np, min_bjet_dr_np, deltaR_bjetLep_np, min_dr_lepb_np, max_dr_lepb_np, lepCharge_np, dileptonMass_np, min_bjet_dr, nJets_np, dileptonMass, event_MET, evaluation_branches, event_HT_np
        del norm_data, x_data, x_data_df, nbJetsT

        #####################
        # EVENT SELECTION
        #####################

        selection = PackedSelection()

        ## Golden JSON
        if not self.isMC:
            if era == "2016post" or era == "2016pre":
                golden_json_path = "vll/data/goldenJsons/Cert_271036-284044_13TeV_Legacy2016_Collisions16_JSON.txt"
            elif era == "2017":
                golden_json_path = "vll/data/goldenJsons/Cert_294927-306462_13TeV_UL2017_Collisions17_GoldenJSON.txt"
            elif era == "2018":
                golden_json_path = "vll/data/goldenJsons/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt"
            else:
                raise ValueError(f"Error: Unknown year \"{era}\".")
            lumi_mask = LumiMask(golden_json_path)(events.run,events.luminosityBlock)
        else:
            lumi_mask = ak.ones_like(events.run, dtype=bool)

        selection.add("lumiMask", ((self.isMC) | ((not(self.isMC)) & lumi_mask)) )

        ## MET filters 
        # Twiki link: https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETOptionalFiltersRun2
        selection.add("filters", ( (events.Flag.goodVertices) 
                                      & (events.Flag.globalSuperTightHalo2016Filter)
                                      & (events.Flag.HBHENoiseFilter)
                                      & (events.Flag.HBHENoiseIsoFilter)
                                      & (events.Flag.EcalDeadCellTriggerPrimitiveFilter)
                                      & (events.Flag.BadPFMuonFilter)
                                      & (events.Flag.eeBadScFilter)
                                      & ( ((era=="2016pre") | (era=="2016post")) | (events.Flag.ecalBadCalibFilter) ))
        )


        ##################
        # HEM cleaning
        ##################
        # veto events if any jet present in HEM affected region
        # For HEM cleaning, separate the MC events into affected and non-affected
        # fraction of lumi from affected 2018 Data runs is 0.647724485 (from brilcalc
        if hasattr(events, 'HEMveto'):
            HEM_cut = np.ones(len(events.MET.pt), dtype=bool)
        else:
            HEM_MCbool_1 = np.ones(round(len(events.MET.pt)*0.647724485), dtype=bool)
            HEM_MCbool_0 = ~np.ones(round(len(events.MET.pt)*(1.0-0.647724485)), dtype=bool)
            HEM_MCbool = np.concatenate((HEM_MCbool_1, HEM_MCbool_0))
            HEM_MCbool = ak.singletons(HEM_MCbool)

            if era == "2018":
                if self.isMC:
                    HEM_cut = HEM_veto(isMC=self.isMC, nrun=np.ones(len(events)), HEMaffected=HEM_MCbool, obj=looseJet)
                else:
                    HEM_cut = HEM_veto(isMC=self.isMC, nrun=events.run, HEMaffected=False, obj=looseJet)
            else:
                HEM_cut = np.ones(len(events.MET.pt), dtype=bool)

            del looseJet, HEM_MCbool_1, HEM_MCbool_0, HEM_MCbool

        ## Trigger selection
        # Select events from the SingleElectron dataset with the single e HLT path
        # Select events from the SingleMuon dataset with the single mu HLT path & veto the single e HLT path
        # https://twiki.cern.ch/twiki/bin/viewauth/CMS/EgHLTScaleFactorMeasurements
        if self.era == "2016pre":
            elehlt = events.HLT.Ele27_WPTight_Gsf
        elif self.era == "2016post":
            elehlt = events.HLT.Ele27_WPTight_Gsf
        elif self.era == "2017":
            elehlt = events.HLT.Ele32_WPTight_Gsf_L1DoubleEG
        else:            
            elehlt = events.HLT.Ele32_WPTight_Gsf

        if self.era == "2017":
            muhlt = events.HLT.IsoMu27
        elif self.era == "2018":
            muhlt = events.HLT.IsoMu24
        else:
            muhlt = events.HLT.IsoTkMu24 | events.HLT.IsoMu24
            
        elehltVeto = ak.fill_none(elehlt == False, True)

        selection.add("trigger",( ((muhlt | elehlt) & (self.isMC)) | 
                                ( (elehlt & (not(self.isMC)) & (not "Mu" in dataset)) | 
                                (muhlt & elehltVeto & (not(self.isMC)) & ("Mu" in dataset)) ) )
                      )

        # (the ak.num() method returns the number of objects in each row of a jagged array)
        selection.add("twoMuon", (ak.num(tightMuons) == 2) & (leadingMuon.pt > 30.) )
        selection.add("oneMuon", (ak.num(tightMuons) == 1) & ( (leadingElectron.pt > 30) | (leadingMuon.pt > 30) ) )
        selection.add("zeroMuon", ak.num(tightMuons) == 0)

        # similar selections will be needed for electrons
        selection.add("twoEle", ak.num(tightElectrons) == 2) 
        selection.add("oneEle", (ak.num(tightElectrons) == 1) & ( (leadingElectron.pt > 30) | (leadingMuon.pt > 30) ) )  
        selection.add("zeroEle", ak.num(tightElectrons) == 0) 

        # similar selections will be needed for taus
        selection.add("oneTau", ak.num(mediumTaus) == 1)  
        selection.add("zeroTau", ak.num(mediumTaus) == 0)  

        # Z peak selection for OS same flavor leptons 
        selection.add("zpeak",(ak.num(leptons) == 2) & (dileptons.charge==0) & ((dileptons.mass <76.) | (dileptons.mass >106.))) 

        # di-lepton categories
        dimuon_cat = {"twoMuon","zeroEle"}
        diele_cat = {"twoEle","zeroMuon"}
        elemuon_cat = {"oneEle","oneMuon"}

        selection.add("eeSel", selection.all(*diele_cat))
        selection.add("emuSel", selection.all(*elemuon_cat))
        selection.add("mumuSel", selection.all(*dimuon_cat))

        # OS and SS leptons 
        # selection.add("OS",(dileptons.charge==0))
        # selection.add("SS",((dileptons.charge==2) |(dileptons.charge==-2)) )

        #  Jet selection: One which selects events with at least 4 tightJet and at least # b-tagged jet
        selection.add("jetSel_4j2b",(ak.num(tightJet) >= 4) & (ak.sum(tightJet.btagged, axis=-1) == 2),)
        selection.add("jetSel_4j3b",(ak.num(tightJet) >= 4) & (ak.sum(tightJet.btagged, axis=-1) == 3),)
        selection.add("jetSel_4j4b",(ak.num(tightJet) >= 4) & (ak.sum(tightJet.btagged, axis=-1) >= 4),)

        # Additional categories with larger jet multiplicities (EN and NN)
        #selection.add("jetSel_4j2b",(ak.num(tightJet) == 4) & (ak.sum(tightJet.btagged, axis=-1) == 2),)
        #selection.add("jetSel_4j3b",(ak.num(tightJet) == 4) & (ak.sum(tightJet.btagged, axis=-1) == 3),)
        #selection.add("jetSel_4j4b",(ak.num(tightJet) == 4) & (ak.sum(tightJet.btagged, axis=-1) >= 4),)
        selection.add("jetSel_5j2b",(ak.num(tightJet) == 5) & (ak.sum(tightJet.btagged, axis=-1) == 2),)
        selection.add("jetSel_5j3b",(ak.num(tightJet) == 5) & (ak.sum(tightJet.btagged, axis=-1) == 3),)
        selection.add("jetSel_5j4b",(ak.num(tightJet) == 5) & (ak.sum(tightJet.btagged, axis=-1) >= 4),)
        selection.add("jetSel_6j2b",(ak.num(tightJet) > 5) & (ak.sum(tightJet.btagged, axis=-1) == 2),)
        selection.add("jetSel_6j3b",(ak.num(tightJet) > 5) & (ak.sum(tightJet.btagged, axis=-1) == 3),)
        selection.add("jetSel_6j4b",(ak.num(tightJet) > 5) & (ak.sum(tightJet.btagged, axis=-1) >= 4),)

        # Additional category for HT
        # selection.add("HT_low",(event_HT <300.),)
        # selection.add("HT_high",(event_HT >=300.),)

        # Additional category for DNN
        # selection.add("DNN_low",(predictions <0.6),)
        # selection.add("DNN_high",(predictions >=0.6),)

        # HEM veto 
        selection.add("HEM_veto", HEM_cut) 

        selection.add("evt_sel",((selection.all("filters")) & (selection.all("trigger")) & (selection.all("lumiMask")) 
                                & (met.pt>40.) & (ak.num(leptons) == 2) & (dileptons.mass >20.)
                                & (event_HT >300.) & (fourJets.pt[:,0]>100.) & (fourJets.pt[:,1]>80.) & (selection.all("HEM_veto"))),)

        # For rescaling b-tag corrections 
        selection.add("sel_btagSF_scaling",(selection.all("evt_sel")) & ( (selection.all("zpeak") & (selection.all("eeSel") | selection.all("mumuSel"))) | selection.all("emuSel")) )

        # useful debugger for selection efficiency
        if False and shift_syst is None:
            print(dataset)
            for n in selection.names:
                print("- Cut ",n," pass ", selection.all(n).sum()," of ", len(events)," events")

        ##################################
        # WRITE OUT VARS IN PARQUET FILE
        ##################################
        if shift_syst is None:

            #if writeOutParquet:
            if True:


                selection.add("sel_NN",(selection.all("evt_sel")) & ( (selection.all("zpeak") & (selection.all("eeSel") | selection.all("mumuSel"))) | selection.all("emuSel")) 
                               & ((selection.all("jetSel_4j2b")) | (selection.all("jetSel_4j3b")) | (selection.all("jetSel_4j4b"))) ) 
                
                events_nn = events[selection.all("sel_NN")]
                tj = tightJet[selection.all("sel_NN")]
                var = ak.zip(
                            {
                                "EventNumber": events_nn.event,
                                "event_HT": event_HT[selection.all("sel_NN")],
                                "event_bHT": event_bHT[selection.all("sel_NN")],
                                "event_MLL": event_MLL[selection.all("sel_NN")],
                                "ratioJetHT": ratioJetHT[selection.all("sel_NN")],
                                "deltaPhibJetMet": deltaPhibJetMet[selection.all("sel_NN")],
                                "min_bjet_dr": min_dr[selection.all("sel_NN")],
                                "deltaR_bjetLep": deltaR_bjetLep[selection.all("sel_NN")],
                                "min_dr_lepb": min_dr_lepb[selection.all("sel_NN")],
                                "max_dr_lepb": max_dr_lepb[selection.all("sel_NN")],
                                "lepCharge": lepCharge[selection.all("sel_NN")],
                                "dileptonMass": dileptons[selection.all("sel_NN")].mass,
                                "event_MET": met[selection.all("sel_NN")].pt,
                                "nJets": ak.num(tightJet.pt[selection.all("sel_NN")]),
                                "nbJets": ak.num(bjets.pt[selection.all("sel_NN")]),
                                "nbJetsT": ak.num(bjets_T.pt[selection.all("sel_NN")]),
                                "nTaus": ak.num(mediumTaus.pt[selection.all("sel_NN")]),
                                "ptJet4": tightJet.pt[selection.all("sel_NN")][:,3],
                                "ratioMETHT": ratioMETHT[selection.all("sel_NN")],
                                "event_mt2_jj": event_mt2_jj[selection.all("sel_NN")],
                                # "pt": tj["pt"],
                                # "eta": tj["eta"],
                                # "phi": tj["phi"],
                                # "mass": tj["mass"],
                                # "btagDeepFlavB": tj["btagDeepFlavB"],
                                # "btagDeepFlavCvB": tj["btagDeepFlavCvB"],
                                # "btagDeepFlavCvL": tj["btagDeepFlavCvL"],
                                # "hadronFlavour": tj["hadronFlavour"],
                            },
                        )

                fname = (events.behavior["__events_factory__"]._partition_key.replace("/", "_") + ".parquet")

                subdirs = []
                if "dataset" in events.metadata:
                    subdirs.append(f'dataset_{events.metadata["dataset"]}')
                self.dump_awkward(var, fname,'root://cmsxrootd.hep.wisc.edu///store/user/ckoraka/TTbar-VLL/v3/'+era+'/', subdirs)

        #####################
        # WEIGHTS AND SFS
        #####################

        if self.isMC:

            ########################
            # Change ttbb component
            #######################
            if "TTbar" in dataset:
                # https://github.com/cms-sw/cmssw/blob/master/TopQuarkAnalysis/TopTools/plugins/GenTtbarCategorizer.cc            
                ttbar_condition = (dataset == 'TTbarPowheg_Dilepton')
                ttbb_condition = (events.genTtbarId % 100 >= 52)
                ttbbWeight = np.ones(len(events.genTtbarId))
                ttbbWeight[ttbar_condition] = np.where(ttbb_condition, 1.3, 1.0)
                weights.add('ttbbWeight', weight=ttbbWeight)

                #######################
                # Top pt re-weighting 
                #######################
                # https://twiki.cern.ch/twiki/bin/view/CMS/TopPtReweighting
                top =  events.GenPart[events.GenPart.pdgId==6 ]
                top = top[(top.statusFlags >> 13) & 1 ==1 ] 
                top = ak.flatten(top)
                antitop =  events.GenPart[events.GenPart.pdgId==-6]
                antitop = antitop[(antitop.statusFlags >> 13) & 1==1]
                antitop = ak.flatten(antitop)
                topPtWgt = np.sqrt(np.exp(0.0615 - 0.0005*top.pt) * np.exp(0.0615 - 0.0005 *antitop.pt))
                topPtWeight = np.ones(len(events.genTtbarId))
                topPtWeight[ttbar_condition] = topPtWgt
                # top = events.GenPart[(events.GenPart.pdgId == 6) & (events.GenPart.status == 62)]
                # antitop = events.GenPart[(events.GenPart.pdgId == -6) & (events.GenPart.status == 62)]
                # SF_top = 0.103*np.exp(-0.0118*top.pt) - 0.000134*top.pt + 0.973
                # SF_antitop = 0.103*np.exp(-0.0118*antitop.pt) - 0.000134*antitop.pt + 0.973
                # topPtWeight = ak.firsts(np.sqrt( SF_top*SF_antitop ))
                weights.add('topPtWeight', weight=topPtWeight)
                del top, antitop, topPtWeight
            else:
                weights.add('ttbbWeight', weight=np.ones(len(met.pt)))
                weights.add('topPtWeight', weight=np.ones(len(met.pt)))


            ##################
            # Muon SFs
            ##################
            # For cut-based ID : Tight
            # For MVA ID : Medium = wp80iso  
            muID_sf, muIso_sf = muonSFs(tightMuons, "Tight", "sf", era)
            muID_up, muIso_up = muonSFs(tightMuons, "Tight", "systup", era) 
            muID_down, muIso_down = muonSFs(tightMuons, "Tight", "systdown", era) 

            muSF = ak.prod(muID_sf * muIso_sf, axis=-1) 
            muSF_up = ak.prod(muID_up * muIso_up, axis=-1)
            muSF_down = ak.prod(muID_down * muIso_down, axis=-1)
            weights.add("muEffWeight", weight=muSF, weightUp=muSF_up, weightDown=muSF_down)

            del muID_sf, muIso_sf, muID_up, muIso_up, muID_down, muIso_down, muSF, muSF_up, muSF_down

            ##################
            # Electron SFs
            ##################
            # For cut-based ID : Tight
            # For MVA ID : Medium = wp80iso  
            eleID_sf, eleReco_sf = electronSFs(tightElectrons, "wp80iso", "sf", era)
            eleID_up, eleReco_up = electronSFs(tightElectrons, "wp80iso", "sfup", era) 
            eleID_down, eleReco_down = electronSFs(tightElectrons, "wp80iso", "sfdown", era) 

            eleSF = ak.prod(eleID_sf * eleReco_sf, axis=-1) 
            eleSF_up = ak.prod(eleID_up * eleReco_up, axis=-1)
            eleSF_down = ak.prod(eleID_down * eleReco_down, axis=-1)
            weights.add("eleEffWeight", weight=eleSF, weightUp=eleSF_up, weightDown=eleSF_down)  

            if(False):
                print('eleSF: ',eleSF,'eleSF_up: ', eleSF_up,' eleSF_down: ',eleSF_down)

            del eleSF, eleSF_up, eleSF_down, eleID_sf, eleReco_sf, eleID_up, eleReco_up, eleID_down, eleReco_down

            ##################
            # Trigger SFs
            ##################
            # For electrons:
            # Taken from https://twiki.cern.ch/twiki/bin/view/CMS/EgHLTScaleFactorMeasurements
            # Using SFs derived by Ka Wa Ho (approximate since selection is a bit different)
            # For muons :
            # https://gitlab.cern.ch/cms-muonPOG/muonefficiencies/-/tree/master/Run2/UL/2018

            mu_trigSF = muontrigSFs(tightMuons, "nominal", era) 
            mu_trigSF_stat = muontrigSFs(tightMuons, "stat", era) 
            mu_trigSF_syst = muontrigSFs(tightMuons, "syst", era) 
            mu_trigSF_unc = np.sqrt(mu_trigSF_stat*mu_trigSF_stat + mu_trigSF_syst*mu_trigSF_syst)
            mu_trigSF_unc = np.where(mu_trigSF == 1, 0, mu_trigSF_unc)

            if(era=="2018"):
                eleTrigSF = ele_trig_sf_2018(leadingElectron.eta,leadingElectron.pt)
                eleTrigSFerr = (ele_trig_err_statData_2018(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_statMC_2018(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2018(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altSignalModel_2018(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altTagSelection_2018(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2018(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altBkgModel_2018(leadingElectron.eta,leadingElectron.pt)**2 )**0.5
            elif(era=="2017"):
                eleTrigSF = ele_trig_sf_2017(leadingElectron.eta,leadingElectron.pt)
                eleTrigSFerr = (ele_trig_err_statData_2017(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_statMC_2017(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2017(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altSignalModel_2017(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altTagSelection_2017(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2017(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altBkgModel_2017(leadingElectron.eta,leadingElectron.pt)**2 )**0.5
            elif(era=="2016post"):
                eleTrigSF = ele_trig_sf_2016post(leadingElectron.eta,leadingElectron.pt)
                eleTrigSFerr = (ele_trig_err_statData_2016post(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_statMC_2016post(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2016post(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altSignalModel_2016post(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altTagSelection_2016post(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2016post(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altBkgModel_2016post(leadingElectron.eta,leadingElectron.pt)**2 )**0.5
            else:
                eleTrigSF = ele_trig_sf_2016pre(leadingElectron.eta,leadingElectron.pt)
                eleTrigSFerr = (ele_trig_err_statData_2016pre(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_statMC_2016pre(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2016pre(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altSignalModel_2016pre(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altTagSelection_2016pre(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altMCEff_2016pre(leadingElectron.eta,leadingElectron.pt)**2 + 
                                ele_trig_err_altBkgModel_2016pre(leadingElectron.eta,leadingElectron.pt)**2 )**0.5

            eleTrigSF = ak.fill_none(eleTrigSF, 1)
            eleTrigSFerr = np.where(eleTrigSF == 1, 0, eleTrigSFerr)

            # Apply trigger SF based on leading lepton 
            elepT = ak.fill_none(leadingElectron.pt, 0)
            mupT = ak.fill_none(leadingMuon.pt,0)
            trigSF = np.where(elepT<mupT, mu_trigSF, eleTrigSF)
            trigSFunc = np.where(elepT<mupT, mu_trigSF_unc, eleTrigSFerr)

            if(False):
                print('triSF: ',trigSF,' trigSFunc: ', trigSFunc)

            weights.add("TrigWeight", weight=trigSF, weightUp=trigSFunc, weightDown=None, shift=True)

            del eleTrigSF, eleTrigSFerr, elepT, mupT, trigSF, trigSFunc, mu_trigSF_unc ,mu_trigSF_syst, mu_trigSF_stat, mu_trigSF

            ##################
            # btag SFs
            ##################
            # Taken from here 
            # https://cms-nanoaod-integration.web.cern.ch/commonJSONSFs/summaries/BTV_2016preVFP_UL_btagging.html
            # sel_btagSF_scaling

            jetSFs = btagSF(tightJet, syst="central",era=era)
            btagWeight = ak.prod(jetSFs, axis=-1)

            ##############################
            # Re-scaling of event weight #
            ##############################
            w1 = len(btagWeight[selection.all("sel_btagSF_scaling")])
            w2 = ak.sum(btagWeight[selection.all("sel_btagSF_scaling")],axis=-1)

            if(w2==0 | w1==0):
                w2=1
                w1=1

            btagWeight = (w1/w2)*btagWeight

            ##############################

            btag_syts = ['cferr1', 'cferr2', 'hf', 'hfstats1', 'hfstats2', 'lf', 'lfstats1', 'lfstats2'] 
            
            btagVariationName = []
            btagVariationUp = []
            btagVariationDown = []

            for bsyst in btag_syts:
                j = []
                if(bsyst=='cferr1' or bsyst=='cferr2'):
                    j = tightJet[tightJet.hadronFlavour==4]
                else:
                    j = tightJet[tightJet.hadronFlavour != 4]

                if(False):
                    print('Nominal ' ,btagSF(j, syst="central",era = era), '  Up: ',btagSF(j, syst="up_"+bsyst,era = era), ' Down ', btagSF(j, syst="down_"+bsyst,era = era))

                jetSFs_up = btagSF(j, syst="up_"+bsyst,era = era)
                jetSFs_down = btagSF(j, syst="down_"+bsyst,era = era)
                partial_btagWeight_up = ak.prod(jetSFs_up, axis=-1)
                partial_btagWeight_down = ak.prod(jetSFs_down, axis=-1)                

                if(bsyst=='cferr1' or bsyst=='cferr2'):
                    mask = (partial_btagWeight_up == 1)
                    partial_btagWeight_up = ak.where(mask, (w2/w1)*btagWeight, partial_btagWeight_up)
                    mask = (partial_btagWeight_down == 1)
                    partial_btagWeight_down = ak.where(mask, (w2/w1)*btagWeight, partial_btagWeight_down)

                btagVariationName.append(bsyst)
                btagVariationUp.append((w1/w2)*partial_btagWeight_up)
                btagVariationDown.append((w1/w2)*partial_btagWeight_down)


            weights.add_multivariation("btagVariation",weight=btagWeight,modifierNames=btagVariationName,weightsUp=btagVariationUp,weightsDown=btagVariationDown,) # Add the SF as up and down variation
            
            del btagVariationName, btagVariationUp, btagVariationDown, btagWeight, w1, w2, jetSFs

            ##################
            # PU SFs
            ##################
            if False:
                print(GetPUSF(events.Pileup.nTrueInt,self.era,'nominal'))
            puWeight = GetPUSF(events.Pileup.nTrueInt,self.era,'nominal')
            puWeight_Up = GetPUSF(events.Pileup.nTrueInt,self.era,'up')
            puWeight_Down = GetPUSF(events.Pileup.nTrueInt,self.era,'down')
            weights.add("puWeight",weight=puWeight,weightUp=puWeight_Up,weightDown=puWeight_Down,)
            del puWeight, puWeight_Up, puWeight_Down

            #########################
            # L1PreFiringWeight
            #########################
            if era=="2017" or era=="2016pre" or era=="2016post":
                weights.add('L1prefiringWeight', weight=events.L1PreFiringWeight.Nom, weightUp=events.L1PreFiringWeight.Up , weightDown=events.L1PreFiringWeight.Dn )
            else:
                weights.add('L1prefiringWeight', weight=np.ones(len(met.pt)), weightUp=np.ones(len(met.pt)), weightDown=np.ones(len(met.pt)))

            #########################
            # THEORY UNCERTAINTIES
            #########################

            # This section sets up some of the weight shifts related to theory uncertainties
            # in some samples, generator systematics are not available, in those case the systematic weights of 1. are used

            if hasattr(events, 'genWeight'):
                weights.add("genWeight",weight=events.genWeight)

            if ak.mean(ak.num(events.PSWeight)) == 1:
                weights.add(
                    "ISR",
                    weight=np.ones(len(events)),
                    weightUp=np.ones(len(events)),
                    weightDown=np.ones(len(events)),
                )
                weights.add(
                    "FSR",
                    weight=np.ones(len(events)),
                    weightUp=np.ones(len(events)),
                    weightDown=np.ones(len(events)),
                )
                weights.add(
                    "PDF",
                    weight=np.ones(len(events)),
                    weightUp=np.ones(len(events)),
                    weightDown=np.ones(len(events)),
                )

            # Otherwise, calculate the weights and systematic variations
            else:
                if hasattr(events, 'LHEPdfWeight'):
                    # PDF Uncertainty weights
                    # avoid errors from 0/0 division
                    LHEPdfWeight_0 = ak.where(
                        events.LHEPdfWeight[:, 0] == 0, 1, events.LHEPdfWeight[:, 0]
                    )
                    LHEPdfVariation = events.LHEPdfWeight / LHEPdfWeight_0
                    weights.add(
                        "PDF",
                        weight=np.ones(len(events)),
                        weightUp=ak.max(LHEPdfVariation, axis=1),
                        weightDown=ak.min(LHEPdfVariation, axis=1),
                    )
                else:
                    weights.add(
                        "PDF",
                        weight=np.ones(len(events)),
                        weightUp=np.ones(len(events)),
                        weightDown=np.ones(len(events)),
                    )
                # ISR / FSR uncertainty weights
                if hasattr(events, 'Generator') and hasattr(events, 'LHEWeight'):
                    if not ak.all(
                        events.Generator.weight == events.LHEWeight.originalXWGTUP
                    ):
                        psWeights = (
                            events.PSWeight
                            * events.LHEWeight.originalXWGTUP
                            / events.Generator.weight
                        )
                    else:
                        psWeights = events.PSWeight
                    weights.add(
                        "ISR",
                        weight=np.ones(len(events)),
                        weightUp=psWeights[:, 2],
                        weightDown=psWeights[:, 0],
                    )
                    weights.add(
                        "FSR",
                        weight=np.ones(len(events)),
                        weightUp=psWeights[:, 3],
                        weightDown=psWeights[:, 1],
                    )
                else:
                    weights.add(
                        "ISR",
                        weight=np.ones(len(events)),
                        weightUp=np.ones(len(events)),
                        weightDown=np.ones(len(events)),
                    )
                    weights.add(
                        "FSR",
                        weight=np.ones(len(events)),
                        weightUp=np.ones(len(events)),
                        weightDown=np.ones(len(events)),
                    )            

        ###################
        # FILL HISTOGRAMS
        ###################
        
        if shift_syst is None:
            if self.isMC:
                evtWeight = weights.weight(None)
            else:
                evtWeight = np.ones(len(events))
            selection1 = PackedSelection()        
            selection1.add("0", (selection.all("trigger")) & (selection.all("HEM_veto"))) # Selection applied at skimming 
            selection1.add("1", (selection1.all("0")) & (ak.num(tightJet) >= 4))
            selection1.add("2", (selection1.all("1")) & (ak.num(leptons) == 2))
            selection1.add("3", (selection1.all("2")) & (selection.all("filters")))
            selection1.add("4", (selection1.all("3")) & (selection.all("lumiMask")))
            selection1.add("5", (selection1.all("4")) & (dileptons.mass >20.))
            selection1.add("6", (selection1.all("5")) & ((selection.all("zpeak") & (selection.all("eeSel") | selection.all("mumuSel"))) | selection.all("emuSel")))
            selection1.add("7", (selection1.all("6")) & (met.pt>40.))
            selection1.add("8", (selection1.all("7")) & (fourJets.pt[:,0]>100.))
            selection1.add("9", (selection1.all("8")) & (fourJets.pt[:,1]>80.))
            selection1.add("10", (selection1.all("9")) & (event_HT >300.))
            selection1.add("11", (selection1.all("10")) & (ak.sum(tightJet.btagged, axis=-1) >= 2))  

            bin=0
            for n in selection1.names:
                output["CutFlow"].fill(
                    dataset=dataset,
                    cut=np.asarray(bin),
                    weight=evtWeight[selection1.all(n)].sum(),
                )
                bin = bin+1 
                if False:
                    print('w/: ', evtWeight[selection1.all(n)].sum(),' w/o: ', selection1.all(n).sum())

        systList = []
        if self.isMC:
            if shift_syst is None:
                systList = [
                    "nominal",
                    "muEffWeightUp",
                    "muEffWeightDown",
                    "eleEffWeightUp",
                    "eleEffWeightDown",
                    "TrigWeightUp",
                    "TrigWeightDown",
                    "puWeightUp",
                    "puWeightDown",
                    "btagVariation_cferr1Up",
                    "btagVariation_cferr1Down",
                    "btagVariation_cferr2Up",
                    "btagVariation_cferr2Down",
                    "btagVariation_hfUp",
                    "btagVariation_hfDown",
                    "btagVariation_hfstats1Up",
                    "btagVariation_hfstats1Down",
                    "btagVariation_hfstats2Up",
                    "btagVariation_hfstats2Down",
                    "btagVariation_lfUp",
                    "btagVariation_lfDown",
                    "btagVariation_lfstats1Up",
                    "btagVariation_lfstats1Down",
                    "btagVariation_lfstats2Up",
                    "btagVariation_lfstats2Down",
                    "L1prefiringWeightUp",
                    "L1prefiringWeightDown",
                    "ISRUp",
                    "ISRDown",
                    "FSRUp",
                    "FSRDown",
                    "PDFUp",
                    "PDFDown",
                ]
            else:
                systList = [shift_syst]
        else:
            systList = ["noweight"]

        ######################
        # Fill histograms
        ######################

        
        for syst in systList:

            # find the event weight to be used when filling the histograms
            weightSyst = syst

            # in the case of 'nominal', or the jet energy systematics, no weight systematic variation is used (weightSyst=None)
            #if syst in  ["nominal","JESUp", "JESDown", "JERUp", "JERDown"]:
            if syst in  ["nominal","JESUp", "JESDown", "JERUp", "JERDown","JES_AbsoluteUp", "JES_BBEC1Up", "JES_EC2Up", "JES_FlavorQCDUp", "JES_HFUp", "JES_RelativeBalUp","JES_AbsoluteDown", "JES_BBEC1Down", "JES_EC2Down", "JES_FlavorQCDDown", "JES_HFDown", "JES_RelativeBalDown","JES_Absolute_"+self.era+"Up", "JES_BBEC1_"+self.era+"Up", "JES_EC2_"+self.era+"Up", "JES_HF_"+self.era+"Up", "JES_RelativeSample_"+self.era+"Up", "JES_Absolute_"+self.era+"Down", "JES_BBEC1_"+self.era+"Down", "JES_EC2_"+self.era+"Down", "JES_HF_"+self.era+"Down", "JES_RelativeSample_"+self.era+"Down"]:
                weightSyst = None

            if syst == "noweight":
                evtWeight = np.ones(len(events))
            else:
                # call weights.weight() with the name of the systematic to be varied
                evtWeight = weights.weight(weightSyst)
                
            # Removed tau axis
            for tauSelec in ["zeroTau","oneTau"]:
                # for dilepCharge in ["OS","SS"]:
                # for dnnReg in ["DNN_low","DNN_high"]:    

                for jetSel in ["jetSel_4j2b","jetSel_4j3b","jetSel_4j4b","jetSel_5j2b","jetSel_5j3b","jetSel_5j4b","jetSel_6j2b","jetSel_6j3b","jetSel_6j4b"]:
                # for jetSel in ["jetSel_4j2b","jetSel_4j3b","jetSel_4j4b"]:
                # for jetSel in ["jetSel_4j2b","jetSel_4j3b","jetSel_4j4b","jetSel_5j4b"]:

                    # for htRegion in ["HT_low","HT_high"]:
                    evtsel= {  'ee': selection.all("eeSel", "zpeak", jetSel, "evt_sel", tauSelec),
                               'emu': selection.all("emuSel", jetSel, "evt_sel", tauSelec),
                               'mumu': selection.all("mumuSel","zpeak", jetSel, "evt_sel", tauSelec),
                            }

                    for lepton in evtsel.keys():

                        for mass in mass_values:
                            output["dnn_vll"].fill(dataset=dataset, dnn=np.asarray(all_predictions[mass][evtsel[lepton]]) , lepFlavor=lepton, bJetMultiplicity=jetSel, VLLmass=str(mass), tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])

                        #if(syst in ['nominal','noweight']):
                        output["event_ST"].fill(dataset=dataset,event_HT=np.asarray(event_ST[evtsel[lepton]]),lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst,weight=evtWeight[evtsel[lepton]]) 
                        output["event_MT"].fill(dataset=dataset, event_MT=np.asarray(event_MLL[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["event_HT"].fill(dataset=dataset, event_HT=np.asarray(event_HT[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["event_bjet_HT"].fill(dataset=dataset, event_HT=np.asarray(event_bHT[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["lead_jet_pT"].fill(dataset=dataset, pt=np.asarray(tightJet.pt[evtsel[lepton]][:,0]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["sub_jet_pT"].fill(dataset=dataset, pt=np.asarray(tightJet.pt[evtsel[lepton]][:,1]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["lead_bjet_pT"].fill(dataset=dataset, pt=np.asarray(bjets.pt[evtsel[lepton]][:,0]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["sub_bjet_pT"].fill(dataset=dataset, pt=np.asarray(bjets.pt[evtsel[lepton]][:,1]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["nJets"].fill(dataset=dataset, njets=ak.num(tightJet.pt[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["nbJets"].fill(dataset=dataset, njets=ak.num(bjets.pt[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["MET_pt"].fill(dataset=dataset, met=np.asarray(met.pt[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["MET_phi"].fill(dataset=dataset, metPhi=np.asarray(met.phi[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["lead_lep_pt"].fill(dataset=dataset, lep_pt=np.asarray(leptons.pt[evtsel[lepton]][:,0]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["sublead_lep_pt"].fill(dataset=dataset, lep_pt=np.asarray(leptons.pt[evtsel[lepton]][:,1]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["dilep_dr"].fill(dataset=dataset, deltaR=np.asarray(dilep_DR[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["event_ratioJetHT"].fill(dataset=dataset, ratio=np.asarray(ratioJetHT[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["event_deltaPhiJetMet"].fill(dataset=dataset, deltaR=np.asarray(min_dr[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["lep_mll"].fill(dataset=dataset, lep_mass=np.asarray(dileptons[evtsel[lepton]].mass), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["deltaR_bjetLep"].fill(dataset=dataset, deltaR_bjetLep=np.asarray(deltaR_bjetLep[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["min_dr_lepb"].fill(dataset=dataset, min_dr_lepb=np.asarray(abs(min_dr_lepb[evtsel[lepton]])), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["max_dr_lepb"].fill(dataset=dataset, max_dr_lepb=np.asarray(abs(max_dr_lepb[evtsel[lepton]])), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])
                        output["lepCharge"].fill(dataset=dataset, lepCharge=np.asarray(lepCharge[evtsel[lepton]]), lepFlavor=lepton, bJetMultiplicity=jetSel, tauMultiplicity=tauSelec, systematic=syst, weight=evtWeight[evtsel[lepton]])

                        # For debugging
                        if False:
                            print(np.asarray( leptons.pt[evtsel[lepton]][:,0]) )
                            print(leptons[evtsel[lepton]][:,0] )
                            print('1: ',(leptons[evtsel[lepton]][:,0] + leptons[evtsel[lepton]][:,1]).mass)
                            print('2: ',dileptons[evtsel[lepton]].mass)
                            
        return {dataset:output}
        
    def postprocess(self, accumulator):
        return accumulator
