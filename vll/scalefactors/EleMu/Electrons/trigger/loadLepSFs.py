from coffea.lookup_tools import dense_lookup
from coffea import util
import uproot3 as uproot

eleTrigSFfiles = {
    "2016pre": "trig_2016preVFP.root",
    "2016post": "trig_2016postVFP.root",
    "2017": "trig_2017.root",
    "2018": "trig_2018.root",
}

def getEleSF_lookups(year):

    ele_file = uproot.open(eleTrigSFfiles[year])
    ele_sf = dense_lookup.dense_lookup(
        ele_file["EGamma_SF2D"].values, ele_file["EGamma_SF2D"].edges
    )
    ele_err_0 = dense_lookup.dense_lookup(
        ele_file["statData"].values, ele_file["statData"].edges
    )
    ele_err_1 = dense_lookup.dense_lookup(
        ele_file["statMC"].values, ele_file["statMC"].edges
    )
    ele_err_2 = dense_lookup.dense_lookup(
        ele_file["altBkgModel"].values, ele_file["altBkgModel"].edges
    )
    ele_err_3 = dense_lookup.dense_lookup(
        ele_file["altSignalModel"].values, ele_file["altSignalModel"].edges
    )
    ele_err_4 = dense_lookup.dense_lookup(
        ele_file["altMCEff"].values, ele_file["altMCEff"].edges
    )
    ele_err_5 = dense_lookup.dense_lookup(
        ele_file["altTagSelection"].values, ele_file["altTagSelection"].edges
    )

    return ele_sf, ele_err_0, ele_err_1,ele_err_2, ele_err_3, ele_err_4, ele_err_5

for year in ["2016pre","2016post","2017","2018"]:
    ele_sf, ele_err_0, ele_err_1,ele_err_2, ele_err_3, ele_err_4, ele_err_5 = getEleSF_lookups(year)
    util.save(ele_sf, "ele_trig_sf_"+year+".coffea")
    util.save(ele_err_0, "ele_trig_statData_"+year+".coffea")
    util.save(ele_err_1, "ele_trig_statMC_"+year+".coffea")
    util.save(ele_err_2, "ele_trig_altBkgModel_"+year+".coffea")
    util.save(ele_err_3, "ele_trig_altSignalModel_"+year+".coffea")
    util.save(ele_err_4, "ele_trig_altMCEff_"+year+".coffea")
    util.save(ele_err_5, "ele_trig_altTagSelection_"+year+".coffea")
