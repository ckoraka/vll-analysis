import os.path
import correctionlib

from coffea import util
from coffea.jetmet_tools import CorrectedJetsFactory,CorrectedMETFactory,JECStack
from coffea.lookup_tools import extractor

import awkward as ak
import numpy as np


# Implementation re applying corrections to data stolen from Shivani
def get_jet_factory(isMC, era, RUN=None, split_JECunc_sources=False, giveMetVar=False):

    cwd = os.path.dirname(__file__)

    if era=="2017": 
        jec_tag='Summer19UL17_V5'; jer_tag='Summer19UL17_JRV2'
        jec_tag_data={
            "RunB": "Summer19UL17_RunB_V5_DATA",
            "RunC": "Summer19UL17_RunC_V5_DATA",
            "RunD": "Summer19UL17_RunD_V5_DATA",
            "RunE": "Summer19UL17_RunE_V5_DATA",
            "RunF": "Summer19UL17_RunF_V5_DATA",
        }        
    elif era=="2018": 
        jec_tag='Summer19UL18_V5'; jer_tag='Summer19UL18_JRV2'
        jec_tag_data={
            "RunA": "Summer19UL18_RunA_V5_DATA",
            "RunB": "Summer19UL18_RunB_V5_DATA",
            "RunC": "Summer19UL18_RunC_V5_DATA",
            "RunD": "Summer19UL18_RunD_V5_DATA",
        }
    elif era=="2016pre": 
        jec_tag='Summer19UL16APV_V7'; jer_tag='Summer20UL16APV_JRV3'
        jec_tag_data={
            "RunBCD": "Summer19UL16APV_RunBCD_V7_DATA",
            "RunEF": "Summer19UL16APV_RunEF_V7_DATA",
        }          
        
    elif era=="2016post": 
        jec_tag='Summer19UL16_V7'; jer_tag='Summer20UL16_JRV3'
        jec_tag_data={
            "RunFGH": "Summer19UL16_RunFGH_V7_DATA",
        }  
    else:
       raise Exception(f"Error: Unknown era \"{era}\".")

    
    # UL JEC/JER files taken from :
    # https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC
    # https://twiki.cern.ch/twiki/bin/view/CMS/JetResolution

    Jetext = extractor()
    if isMC:
        # For MC
        Jetext.add_weight_sets(
            [
                f"* * {cwd}/JEC/"+era+"/"+jec_tag+"_MC_L1FastJet_AK4PFchs.jec.txt",
                f"* * {cwd}/JEC/"+era+"/"+jec_tag+"_MC_L2Relative_AK4PFchs.jec.txt",
                f"* * {cwd}/JEC/"+era+"/"+jec_tag+"_MC_Uncertainty_AK4PFchs.junc.txt",
                f"* * {cwd}/JEC/"+era+"/RegroupedV2_"+jec_tag+"_MC_UncertaintySources_AK4PFchs.junc.txt",
                f"* * {cwd}/JEC/"+era+"/"+jer_tag+"_MC_PtResolution_AK4PFchs.jr.txt",
                f"* * {cwd}/JEC/"+era+"/"+jer_tag+"_MC_SF_AK4PFchs.jersf.txt",
            ]
        )
    else:
        # For Data
        #ensure we dont duplicate (Thanks Garvita!)
        tags_done = []
        for run, tag in jec_tag_data.items():
            if not (tag in tags_done):
                Jetext.add_weight_sets([
                    f"* * {cwd}/JEC/"+era+"/"+tag+"_L1FastJet_AK4PFchs.jec.txt",
                    f"* * {cwd}/JEC/"+era+"/"+tag+"_L2Relative_AK4PFchs.jec.txt",
                    f"* * {cwd}/JEC/"+era+"/"+tag+"_L3Absolute_AK4PFchs.jec.txt",
                    f"* * {cwd}/JEC/"+era+"/"+tag+"_L2L3Residual_AK4PFchs.jec.txt",                    
                ])
                tags_done += [tag]
                
    Jetext.finalize()
    Jetevaluator = Jetext.make_evaluator()

    if isMC: 
        # For MC
        jec_names = [
            jec_tag+"_MC_L1FastJet_AK4PFchs",
            jec_tag+"_MC_L2Relative_AK4PFchs",
            jec_tag+"_MC_Uncertainty_AK4PFchs",
            jer_tag+"_MC_PtResolution_AK4PFchs",
            jer_tag+"_MC_SF_AK4PFchs",
        ]
        # Using default recommendation Twiki link: https://cms-jerc.web.cern.ch/Recommendations/#case-ii-default
        # correlated across years : ['FlavorQCD', 'RelativeBal', 'HF', 'BBEC1', 'EC2', 'Absolute']
        # uncorrelated: ['Absolute_2018', 'HF_2018', 'EC2_2018', 'BBEC1_2018', 'RelativeSample_2018']
        # total of 11 sources per year
        # using the RegroupedV2 txt files
        Era = era
        if(era == "2016pre"):
            Era = "2016"
        if(era == "2016post"):
            Era = "2016"
        jec_sources = ['Absolute_'+Era, 'BBEC1_'+Era, 'EC2_'+Era, 'HF_'+Era, 'RelativeSample_'+Era, 'FlavorQCD', 'RelativeBal', 'HF', 'BBEC1', 'EC2', 'Absolute' ]
        
        if split_JECunc_sources:
            for jec_source in jec_sources:
                jec_names.append("RegroupedV2_"+jec_tag+"_MC_UncertaintySources_AK4PFchs_"+str(jec_source))
        
    else:
        # For Data
        jec_names={}
        for run, tag in jec_tag_data.items():
            jec_names[run] = [
                tag+"_L1FastJet_AK4PFchs",
                tag+"_L2Relative_AK4PFchs",
                tag+"_L3Absolute_AK4PFchs",
                tag+"_L2L3Residual_AK4PFchs",
            ]

    if isMC:
        jec_inputs = {name: Jetevaluator[name] for name in jec_names}
    else:
        jec_inputs = {name: Jetevaluator[name] for name in jec_names[RUN]}
        
    jec_stack = JECStack(jec_inputs)

    name_map = jec_stack.blank_name_map
    name_map["JetPt"] = "pt"
    name_map["JetEta"] = "eta"
    name_map["JetPhi"] = "phi"
    name_map["JetMass"] = "mass"
    name_map["JetA"] = "area"
    name_map["ptGenJet"] = "pt_gen"
    name_map["ptRaw"] = "pt_raw"
    name_map["massRaw"] = "mass_raw"
    name_map["Rho"] = "rho"

    name_map['METpt'] = 'pt'
    name_map['METphi'] = 'phi'
    name_map['UnClusteredEnergyDeltaX'] = 'MetUnclustEnUpDeltaX'
    name_map['UnClusteredEnergyDeltaY'] = 'MetUnclustEnUpDeltaY'

    if giveMetVar: return CorrectedMETFactory(name_map)
    else: return CorrectedJetsFactory(name_map, jec_stack)


# Twiki link: https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETRun2Corrections#xy_Shift_Correction_MET_phi_modu
# Corr values from https://lathomas.web.cern.ch/lathomas/METStuff/XYCorrections/XYMETCorrection_withUL17andUL18andUL16.h  
def get_polar_corrected_MET(runera, npv, met_pt, met_phi):
    
    # depends on era and npv, npv = number of primary vertices
    ### for Data ###
    #2018:
    if "runA_2018" in runera: 
        xcorr, ycorr = -(0.263733*npv +-1.91115), -(0.0431304*npv +-0.112043)
    elif "runB_2018" in runera: 
        xcorr, ycorr = -(0.400466*npv +-3.05914), -(0.146125*npv +-0.533233)
    elif "runC_2018" in runera: 
        xcorr, ycorr = -(0.430911*npv +-1.42865), -(0.0620083*npv +-1.46021)
    elif "runD_2018" in runera: 
        xcorr, ycorr = -(0.457327*npv +-1.56856), -(0.0684071*npv +-0.928372)
    #2017:
    elif "runB_2017" in runera: 
        xcorr, ycorr = -(-0.211161*npv +0.419333), -(0.251789*npv +-1.28089)
    elif "runC_2017" in runera: 
        xcorr, ycorr = -(-0.185184*npv +-0.164009), -(0.200941*npv +-0.56853)
    elif "runD_2017" in runera: 
        xcorr, ycorr = -(-0.201606*npv +0.426502), -(0.188208*npv +-0.58313)
    elif "runE_2017" in runera: 
        xcorr, ycorr = -(-0.162472*npv +0.176329), -(0.138076*npv +-0.250239)
    elif "runF_2017" in runera: 
        xcorr, ycorr = -(-0.210639*npv +0.72934), -(0.198626*npv +1.028)
    #2016:
    elif "runBver2_2016" in runera: 
        xcorr, ycorr = -(-0.0214894*npv +-0.188255), -(0.0876624*npv +0.812885)
    elif "runBver1_2016" in runera: 
        xcorr, ycorr = -(-0.0214894*npv +-0.188255), -(0.0876624*npv +0.812885)
    elif "runC_2016" in runera: 
        xcorr, ycorr = -(-0.032209*npv +0.067288), -(0.113917*npv +0.743906)
    elif "runD_2016" in runera: 
        xcorr, ycorr = -(-0.0293663*npv +0.21106), -(0.11331*npv +0.815787)        
    elif "runE_2016" in runera: 
        xcorr, ycorr = -(-0.0132046*npv +0.20073), -(0.134809*npv +0.679068)
    elif "runF_2016" in runera: 
        xcorr, ycorr = -(-0.0543566*npv +0.816597), -(0.114225*npv +1.17266)
    elif "runF-HIMP_2016" in runera: 
        xcorr, ycorr = -(0.134616*npv +-0.89965), -(0.0397736*npv +1.0385)
    elif "runG_2016" in runera: 
        xcorr, ycorr = -(0.121809*npv +-0.584893), -(0.0558974*npv +0.891234)
    elif "runH_2016" in runera: 
        xcorr, ycorr = -(0.0868828*npv +-0.703489), -(0.0888774*npv +0.902632)

    ### for MC ###
    elif(runera=="2017"):
        xcorr, ycorr = -(-0.300155*npv +1.90608), -(0.300213*npv +-2.02232)
    elif(runera=="2018"):
        xcorr, ycorr = -(0.183518*npv +0.546754), -(0.192263*npv +-0.42121)
    elif(runera=="2016pre"):
        xcorr, ycorr = -(-0.188743*npv +0.136539), -(0.0127927*npv +0.117747)
    elif(runera=="2016post"):
        xcorr, ycorr = -(-0.153497*npv +-0.231751), -(0.00731978*npv +0.243323)

    #Add met correction factor to uncorrected component
    x = met_pt * np.cos(met_phi) + xcorr
    y = met_pt * np.sin(met_phi) + ycorr

    # return corrected MET pt and phi
    return np.hypot(x, y), np.arctan2(y, x)


# In 2018 data taking, endcaps of HCAL were not functioning (from runs>=319077) (last of 2018B and full of 2018CD). 
# HCAL deposits were not recorded for region: eta=[-3.0, -1.3] and phi=[-1.57, -0.87], hence reco jets pT is under-measured, so MET is over-measured.
# We veto jets in this region to remove fake MET 
# Reference: https://hypernews.cern.ch/HyperNews/CMS/get/JetMET/2000.html

def HEM_veto(isMC, nrun, HEMaffected, obj):

    isHEM = ak.ones_like(obj.pt)

    #for affected runs, ensure object is not in HEM region, and for unaffected runs, do nothing
    #For Data we use run number 'nrun' to veto event
    if isMC == False:
        passHEM =  ( 
            ( ((obj.eta <= -3.0) | (obj.eta >= -1.3) | (obj.phi <= -1.57) | (obj.phi >= -0.87)) & (nrun >= 319077) ) 
            | ( (obj.pt > 0) & (nrun < 319077) ) 
        )
    else:
        # For MC, separate the events as affected and not affected; and scale accordingly
        passHEM_1 = ( ( (obj.eta <= -3.0) | (obj.eta >= -1.3) | (obj.phi <= -1.57) | (obj.phi >= -0.87) ) )
        passHEM_0 = ( (obj.pt > 0) )
        passHEM = ak.where(ak.any(HEMaffected, axis=1), passHEM_1, passHEM_0)

    # for a jet in affected region, passHEM will be False
    # we select event if all objects passHEM
    passHEM = (ak.sum(passHEM == False, axis=1)==0)

    return passHEM

