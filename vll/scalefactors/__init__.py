"""Scale factors for the analysis
This module loads and sets up the scale factor objects
"""
import os.path
import correctionlib

from coffea import util
from coffea.btag_tools import BTagScaleFactor
from coffea.jetmet_tools import CorrectedJetsFactory,CorrectedMETFactory,JECStack
from coffea.lookup_tools import extractor

cwd = os.path.dirname(__file__)

ceval2016pre = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2016preVFP_UL/btagging.json")
ceval2016post = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2016postVFP_UL/btagging.json")
ceval2017 = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2017_UL/btagging.json")
ceval2018 = correctionlib.CorrectionSet.from_file(f"{cwd}/BTag/2018_UL/btagging.json")

ele_trig_sf_2016pre = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_sf_2016pre.coffea")
ele_trig_err_statData_2016pre = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statData_2016pre.coffea")
ele_trig_err_statMC_2016pre = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statMC_2016pre.coffea")
ele_trig_err_altBkgModel_2016pre = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altBkgModel_2016pre.coffea")
ele_trig_err_altMCEff_2016pre = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altMCEff_2016pre.coffea")
ele_trig_err_altSignalModel_2016pre = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altSignalModel_2016pre.coffea")
ele_trig_err_altTagSelection_2016pre = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altTagSelection_2016pre.coffea")

ele_trig_sf_2016post = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_sf_2016post.coffea")
ele_trig_err_statData_2016post = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statData_2016post.coffea")
ele_trig_err_statMC_2016post = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statMC_2016post.coffea")
ele_trig_err_altBkgModel_2016post = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altBkgModel_2016post.coffea")
ele_trig_err_altMCEff_2016post = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altMCEff_2016post.coffea")
ele_trig_err_altSignalModel_2016post = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altSignalModel_2016post.coffea")
ele_trig_err_altTagSelection_2016post = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altTagSelection_2016post.coffea")

ele_trig_sf_2017 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_sf_2017.coffea")
ele_trig_err_statData_2017 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statData_2017.coffea")
ele_trig_err_statMC_2017 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statMC_2017.coffea")
ele_trig_err_altBkgModel_2017 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altBkgModel_2017.coffea")
ele_trig_err_altMCEff_2017 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altMCEff_2017.coffea")
ele_trig_err_altSignalModel_2017 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altSignalModel_2017.coffea")
ele_trig_err_altTagSelection_2017 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altTagSelection_2017.coffea")

ele_trig_sf_2018 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_sf_2018.coffea")
ele_trig_err_statData_2018 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statData_2018.coffea")
ele_trig_err_statMC_2018 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_statMC_2018.coffea")
ele_trig_err_altBkgModel_2018 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altBkgModel_2018.coffea")
ele_trig_err_altMCEff_2018 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altMCEff_2018.coffea")
ele_trig_err_altSignalModel_2018 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altSignalModel_2018.coffea")
ele_trig_err_altTagSelection_2018 = util.load(f"{cwd}/EleMu/Electrons/trigger/ele_trig_altTagSelection_2018.coffea")

