import uproot
from coffea.lookup_tools import dense_lookup


eleTrigSFfiles = {
    "2016pre": "trig_2016preVFP.root",
    "2016post": "trig_2016preVFP.root",
    "2017": "trig_2016preVFP.root",
    "2018": "trig_2016preVFP.root",
}

def getEleSF_lookups(year):

    ele_file = uproot.open(eleTrigSFfiles[year])
    ele_sf = dense_lookup.dense_lookup(
        ele_file["EGamma_SF2D"].values, ele_file["EGamma_SF2D"].axis(0).edges(), ele_file["EGamma_SF2D"].axis(1).edges()
    )
    ele_err_0 = dense_lookup.dense_lookup(
        ele_file["statData"].values, ele_file["statData"].edges
    )
    ele_err_1 = dense_lookup.dense_lookup(
        ele_file["statMC"].values, ele_file["statMC"].edges
    )
    ele_err_2 = dense_lookup.dense_lookup(
        ele_file["altBkgModel"].values, ele_file["altBkgModel"].edges
    )
    ele_err_3 = dense_lookup.dense_lookup(
        ele_file["altSignalModel"].values, ele_file["altSignalModel"].edges
    )
    ele_err_4 = dense_lookup.dense_lookup(
        ele_file["altMCEff"].values, ele_file["altMCEff"].edges
    )
    ele_err_5 = dense_lookup.dense_lookup(
        ele_file["altTagSelection"].values, ele_file["altTagSelection"].edges
    )

    return ele_sf, ele_err_0, ele_err_1,ele_err_2, ele_err_3, ele_err_4, ele_err_5 

def getEleSF(pt, eta, year):

    ele_id_sf, ele_id_err, ele_reco_sf, ele_reco_err = getEleSF_lookups(year)

    eleTrigSF = ele_sf(eta, pt)
    eleTrigSFerr_0 = ele_err_0(eta, pt)
    eleTrigSFerr_1 = ele_err_1(eta, pt)
    eleTrigSFerr_2 = ele_err_2(eta, pt)
    eleTrigSFerr_3 = ele_err_3(eta, pt)
    eleTrigSFerr_4 = ele_err_4(eta, pt)
    eleTrigSFerr_5 = ele_err_5(eta, pt)

    total_err = (eleTrigSFerr_0**2+eleTrigSFerr_1**2+eleTrigSFerr_2**2+eleTrigSFerr_3**2+eleTrigSFerr_4**2+eleTrigSFerr_5**2)**0.5

    return eleTrigSF,total_err

    