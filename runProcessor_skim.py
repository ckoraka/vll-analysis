from coffea import util, processor
from coffea.nanoevents import NanoAODSchema
from vll.skim_processor import VLLSkim
import time
import uproot
from filelists.fileset import *

uproot.open.defaults["xrootd_handler"] = uproot.source.xrootd.MultithreadedXRootDSource


# Initialize these :
#---
era = "2018"
listOfFiles = filesetSignal #filesetTTbarPowheg_Dilepton2017  #filesetSignal data2018
isMC = True
#---

tstart = time.time()
outfile = "test.coffea"
print('Will create : ',outfile)

output = processor.run_uproot_job(
    listOfFiles,
    "Events",
    VLLSkim(isMC=isMC,era=era),
    processor.futures_executor, # iterative_executor, futures_executor : Run "multithreaded"
    executor_args={'schema': NanoAODSchema,'workers': 4,'skipbadfiles': True,'xrootdtimeout':300},
    chunksize=1000,
    maxchunks=None,
)


elapsed = time.time() - tstart
print("Total time: %.1f seconds"%elapsed)
util.save(output, outfile)
print(f"Saved output to {outfile}")
