import os
import uproot
from vll.utils.crossSections import nevents

# Define the list of sample names
samples = [
    "VLL_EE_M-500GeV", "VLL_EN_M-500GeV", "VLL_NN_M-500GeV",
    "VLL_EE_M-550GeV", "VLL_EN_M-550GeV", "VLL_NN_M-550GeV",
    "VLL_EE_M-600GeV", "VLL_EN_M-600GeV", "VLL_NN_M-600GeV",
    "VLL_EE_M-650GeV", "VLL_EN_M-650GeV", "VLL_NN_M-650GeV",
    "VLL_EE_M-700GeV", "VLL_EN_M-700GeV", "VLL_NN_M-700GeV",
    "VLL_EE_M-750GeV", "VLL_EN_M-750GeV", "VLL_NN_M-750GeV",
    "VLL_EE_M-800GeV", "VLL_EN_M-800GeV", "VLL_NN_M-800GeV",
    "VLL_EE_M-850GeV", "VLL_EN_M-850GeV", "VLL_NN_M-850GeV",
    "VLL_EE_M-900GeV", "VLL_EN_M-900GeV", "VLL_NN_M-900GeV",
    "VLL_EE_M-950GeV", "VLL_EN_M-950GeV", "VLL_NN_M-950GeV",
    "VLL_EE_M-1000GeV", "VLL_EN_M-1000GeV", "VLL_NN_M-1000GeV",
    "TTbarPowheg_Dilepton", "TTbarPowheg_Semilept", "TTbarPowheg_Hadronic",
    "W4jets", "DYjetsM50", "TTW", "TTZ", "WW", "WZ", "ZZ",
    "WWW", "WWZ", "WZZ", "ZZZ", "TTTT", "TTHToNonBB", "TTHToBB",
    "TTWW", "TTWZ", "TTWH", "TTZZ", "TTZH", "TTHH", "ST_tW_channel",
    "ST_tbarW_channel", "ST_t-channel_T", "ST_t-channel_antiT", "ST_s_channel",
    "DYjetsM10to50", "Data_SingleEle_runA_2018", "Data_SingleEle_runC_2018",
    "Data_SingleEle_runB_2018", "Data_SingleEle_runD_2018", "Data_SingleMu_runA_2018",
    "Data_SingleMu_runB_2018", "Data_SingleMu_runC_2018", "Data_SingleMu_runD_2018",
    "Data_SingleEle_runB_2017", "Data_SingleEle_runC_2017", "Data_SingleEle_runD_2017",
    "Data_SingleEle_runE_2017", "Data_SingleEle_runF_2017",
    "Data_SingleMu_runB_2017", "Data_SingleMu_runC_2017", "Data_SingleMu_runD_2017",
    "Data_SingleMu_runE_2017", "Data_SingleMu_runF_2017",
    "Data_SingleMu_runBver2_2016","Data_SingleMu_runBver1_2016",
    "Data_SingleEle_runBver2_2016","Data_SingleEle_runBver1_2016","Data_SingleMu_runC_2016","Data_SingleMu_runD_2016",
    "Data_SingleEle_runC_2016","Data_SingleEle_runD_2016","Data_SingleEle_runF-HIMP_2016","Data_SingleMu_runF-HIMP_2016",
    "Data_SingleEle_runF_2016","Data_SingleEle_runG_2016","Data_SingleEle_runH_2016",
    "Data_SingleMu_runF_2016","Data_SingleMu_runG_2016","Data_SingleMu_runH_2016"
]



base_dir = "/hdfs/store/user/ckoraka/vll/skims/2017" 

def get_event_data(root_file):
    """Retrieve the number of entries (raw), processed events (skimmed), and sum_genW from a root file."""
    try:
        file = uproot.open(root_file)
        tree = file["Events"]

        # Get the total number of entries (raw events)
        total_entries = tree.num_entries

        # Get the processed events (skimmed events)
        n_processed = tree["n_processed"].array()
        skimmed_events = n_processed[0] if len(n_processed) > 0 else 0

        # Check if the 'sum_genW' branch exists in the tree before trying to access it
        if "sum_genW" in tree:
            sum_genW = tree["sum_genW"].array()
            gen_weight_sum = sum_genW[0] if len(sum_genW) > 0 else 0
        else:
            gen_weight_sum = 0  # Return 0 if the branch does not exist

        return total_entries, skimmed_events, gen_weight_sum
    except Exception as e:
        print(f"Error processing file {root_file}: {e}")
        return 0, 0, 0  # Return 0 for all if there's an error

def process_sample(sample, era):
    """Process each sample and return both total entries, processed (skimmed) events, and sum_genW."""
    sample_dir = os.path.join(base_dir, sample)

    if not os.path.isdir(sample_dir):
        print(f"Sample directory for {sample} does not exist. Skipping.")
        return {"total_entries": 0, "skimmed_events": 0, "sum_genW": 0}

    total_entries = 0
    skimmed_events = 0
    sum_genW = 0
    for root_file in os.listdir(sample_dir):
        if root_file.endswith('.root'):
            root_file_path = os.path.join(sample_dir, root_file)
            file_entries, file_skimmed_events, file_gen_weight_sum = get_event_data(root_file_path)
            total_entries += file_entries
            skimmed_events += file_skimmed_events
            sum_genW += file_gen_weight_sum

    expected_events = nevents[era].get(sample, None)

    if expected_events is not None:
        print(f"Sample: {sample} | Total entries: {total_entries} | Skimmed events: {skimmed_events} | Sum GenW: {sum_genW} | Expected events: {expected_events}")
        if skimmed_events != expected_events:
            print(f"Warning: Skimmed events for {sample} does not match expected events!")
    else:
        print(f"Sample: {sample} | Total entries: {total_entries} | Skimmed events: {skimmed_events} | Sum GenW: {sum_genW} | Expected events: N/A (sample not found for {era})")

    return {"total_entries": total_entries, "skimmed_events": skimmed_events, "sum_genW": sum_genW}

def write_event_data_to_file(nevents_dict, nentries_dict, sum_genW_dict, filename="eventData.py"):
    """Write the total entries, skimmed events, and sum_genW to a Python file."""
    with open(filename, 'w') as f:
        f.write("neventsSkimmed = {\n")
        for sample, skimmed_events in nevents_dict.items():
            f.write(f'    "{sample}": {skimmed_events},\n')
        f.write("}\n\n")

        f.write("nentries = {\n")
        for sample, total_entries in nentries_dict.items():
            f.write(f'    "{sample}": {total_entries},\n')
        f.write("}\n\n")

        f.write("sum_genW = {\n")
        for sample, gen_weight_sum in sum_genW_dict.items():
            f.write(f'    "{sample}": {gen_weight_sum},\n')
        f.write("}\n")

def compare_event_counts(era):
    """Compare event counts for each sample and write the results to a file."""
    nevents_dict = {}
    nentries_dict = {}
    sum_genW_dict = {}

    for sample in samples:
        sample_data = process_sample(sample, era)
        nevents_dict[sample] = sample_data["skimmed_events"]
        nentries_dict[sample] = sample_data["total_entries"]
        sum_genW_dict[sample] = sample_data["sum_genW"]

    # Write the event counts to a file
    write_event_data_to_file(nevents_dict, nentries_dict, sum_genW_dict)
    print(f"Event counts have been written to eventData.py")

if __name__ == "__main__":
    era = "2017"
    compare_event_counts(era)
