#!/bin/bash

for mass in {500..1000..50}
do
    combineTool.py -M T2W -i datacards/datacard_2018_VLL${mass}GeV.txt -m ${mass} -o workspace_${mass}GeV.root
    combineTool.py -M AsymptoticLimits -d  datacards/workspace_${mass}GeV.root --there -n .limit -m ${mass} --run blind
done

combineTool.py -M CollectLimits datacards/*.limit.* -o limits.json
