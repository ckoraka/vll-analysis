from ROOT import TH2D,TGraph,TGraphAsymmErrors,TMultiGraph,TCanvas,TLegend,TLatex,TLine,kRed, kGreen, kYellow,gStyle,gROOT
from array import *
import ctypes

limits_dict = {
  "500.0": {
    "exp+1": 15.681977272033691,
    "exp+2": 25.013566970825195,
    "exp-1": 5.33953857421875,
    "exp-2": 3.8671875,
    "exp0": 8.25
  },
  "550.0": {
    "exp+1": 25.910356521606445,
    "exp+2": 39.047550201416016,
    "exp-1": 7.459468841552734,
    "exp-2": 5.1298828125,
    "exp0": 12.875
  },
  "600.0": {
    "exp+1": 31.067272186279297,
    "exp+2": 46.81915283203125,
    "exp-1": 7.997650146484375,
    "exp-2": 5.306640625,
    "exp0": 15.4375
  },
  "650.0": {
    "exp+1": 79.4919662475586,
    "exp+2": 119.79637145996094,
    "exp-1": 12.800613403320312,
    "exp-2": 7.560546875,
    "exp0": 39.5
  },
  "700.0": {
    "exp+1": 6024.90966796875,
    "exp+2": 12115.458984375,
    "exp-1": 17.802446365356445,
    "exp-2": 9.27734375,
    "exp0": 4000.0
  },
  "750.0": {
    "exp+1": 6024.90966796875,
    "exp+2": 12115.458984375,
    "exp-1": 34.11149978637695,
    "exp-2": 12.6953125,
    "exp0": 4000.0
  },
}

limit_keys = ['exp0','exp-1','exp-2','exp+1','exp+2','exp1','exp2']
xlist = []; ylist = []; ylist_exp_minus1 = [] ; ylist_exp_minus2 = [] ; ylist_exp_plus1 = [] ; ylist_exp_plus2 = []
for mv in limits_dict.keys():
        x = float(mv); y = limits_dict[mv]['exp0']; y_exp_minus1 = limits_dict[mv]['exp-1']; y_exp_minus2 = limits_dict[mv]['exp-2']; y_exp_plus1 = limits_dict[mv]['exp+1']; y_exp_plus2 = limits_dict[mv]['exp+2']
        xlist.append(x); ylist.append(y); ylist_exp_minus1.append(y_exp_minus1); ylist_exp_minus2.append(y_exp_minus2); ylist_exp_plus1.append(y_exp_plus1); ylist_exp_plus2.append(y_exp_plus2)

xlist = array('d',xlist)
ylists= [ylist,ylist_exp_minus1,ylist_exp_minus2,ylist_exp_plus1,ylist_exp_plus2]
merged_list = list(zip(ylists,limit_keys))
limits_to_plot = {}
for i, my_list in enumerate(ylists):
        my_list = array('d',my_list); print(type(my_list))
        my_tag = merged_list[i][-1]; print(my_tag)
        limits_to_plot[my_tag] = TGraph(len(xlist),xlist,my_list)

def get_band(up_y,down_y,name,xlist=xlist,central_y=array('d',ylist)):
        print("####") ; print(up_y) ; print("####")
        print("####") ; print(down_y) ; print("####")
        print("####") ; print(name) ; print("####")
        print("####") ; print(xlist) ; print("####")
        print("####") ; print(central_y) ; print("####")
        yvals_lo = [a_i - b_i for a_i,b_i in zip(central_y,down_y)]
        yvals_hi = [a_i - b_i for a_i,b_i in zip(up_y,central_y)]
        print("$$$$") ; print(yvals_lo) ; print("$$$$")
        print("$$$$") ; print(yvals_hi) ; print("$$$$")
        limits_to_plot[name] = TGraphAsymmErrors(len(xlist),xlist,central_y,array('d', [0]),array('d', [0]),array('d', yvals_lo), array('d', yvals_hi)); limits_to_plot[name].Sort()

get_band(ylist_exp_plus1,ylist_exp_minus1,'exp1')    
get_band(ylist_exp_plus2,ylist_exp_minus2,'exp2')      

outdir_base = './'
def make_plot(limits_to_plot):
        year = '2018'
        print(limits_to_plot) 
        class Bounds:
                def __init__(self):
                        self.xmax = None
                        self.xmin = None
                        self.ymax = None
                        self.ymin = None
                def setBounds(self,x,y):
                        if self.xmax is None: self.xmax = x
                        if self.xmin is None: self.xmin = x
                        if self.ymax is None: self.ymax = y
                        if self.ymin is None: self.ymin = y

                        self.xmax = max(self.xmax,x)
                        self.xmin = min(self.xmin,x)
                        self.ymax = max(self.ymax,y)
                        self.ymin = min(self.ymin,y)
                def getBounds(self): return self.xmin,self.xmax,self.ymin,self.ymax
                def __str__(self): return 'x: [%f-%f] y: [%f-%f]' % (self.xmin,self.xmax,self.ymin,self.ymax)
        bounds = Bounds()
        for graph_type,graph in limits_to_plot.iteritems():
                x,y = ctypes.c_double(0),ctypes.c_double(0)
                for i in range(graph.GetN()):
                        graph.GetPoint(i,x,y)
                        bounds.setBounds(x.value,y.value)

        minX,maxX,minY,maxY = bounds.getBounds()
        print(bounds)
        ######################################################################
        draw=['exp2', 'exp1', 'exp0']
        #draw=['exp-2', 'exp-1', 'exp0','exp+1', 'exp+2']
        style_dict = {
                #'obs' : { 'LineWidth' : 2, 'LineStyle': 9, 'MarkerSize': 1.5 ,'MarkerStyle': 20},
                'exp0' : { 'LineWidth' : 2, 'LineColor' : kRed, 'LineStyle': 1, 'MarkerSize': 2 ,'MarkerStyle': 1},
                'exp1' : { 'FillColor' : kGreen},
                'exp2' : { 'FillColor' : kYellow},
                'exp-1' : { 'LineWidth' : 2, 'LineColor' : kGreen, 'LineStyle': 1},
                'exp+1' : { 'LineWidth' : 2, 'LineColor' : kGreen, 'LineStyle': 1},
                'exp-2' : { 'LineWidth' : 2, 'LineColor' : kYellow, 'LineStyle': 1},
                'exp+2' : { 'LineWidth' : 2, 'LineColor' : kYellow, 'LineStyle': 1}
        }

        legend_dict = {
                #'obs' : { 'Label' : 'Observed', 'LegendStyle' : 'LP', 'DrawStyle' : 'PLSAME'},
                'exp0' : { 'Label' : 'Expected', 'LegendStyle' : 'LP', 'DrawStyle' : 'PLSAME'},
                'exp1' : { 'Label' : '#pm1#sigma Expected', 'LegendStyle' : 'F', 'DrawStyle' : '3SAME'},
                'exp2' : { 'Label' : '#pm2#sigma Expected', 'LegendStyle' : 'F', 'DrawStyle' : '3SAME'},
                'exp-1' : { 'Label' : '-1 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
                'exp+1' : { 'Label' : '+1 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
                'exp-2' : { 'Label' : '-2 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
                'exp+2' : { 'Label' : '+2 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'}

        }

        c = TCanvas("c","c",800,800)
        c.SetLogy()#; c.SetLogx()
        # c.SetMargin(0.15,0.15,0.15,0.08)
        gStyle.SetOptStat(0);
        gStyle.SetLegendBorderSize(0);
        # gStyle.SetPalette(kRainBow)

        limits = TMultiGraph()
        legend = TLegend(0.6,0.05,0.9,0.25,"")#x1,y1,x2,y2
        legend.SetTextSize(0.02)
        legend.SetFillColor(0)
        for key in draw:
                if key in limits_to_plot:
                        print(key)
                        limits_to_plot[key].Sort()
                        legend.AddEntry(limits_to_plot[key],legend_dict[key]['Label'],legend_dict[key]['LegendStyle'])
                        if 'LineWidth' in style_dict[key]: limits_to_plot[key].SetLineWidth(style_dict[key]['LineWidth'])
                        if 'LineColor' in style_dict[key]: limits_to_plot[key].SetLineColor(style_dict[key]['LineColor'])
                        if 'FillColor' in style_dict[key]: limits_to_plot[key].SetFillColor(style_dict[key]['FillColor'])
                        if 'LineStyle' in style_dict[key]: limits_to_plot[key].SetLineStyle(style_dict[key]['LineStyle'])
                        if 'MarkerSize' in style_dict[key]: limits_to_plot[key].SetMarkerSize(style_dict[key]['MarkerSize'])
                        if 'MarkerStyle' in style_dict[key]: limits_to_plot[key].SetMarkerStyle(style_dict[key]['MarkerStyle'])
                        limits.Add(limits_to_plot[key],legend_dict[key]['DrawStyle'])

        limits.Draw('a')
        limits.GetXaxis().SetLimits(minX,maxX) #limits.GetXaxis().SetRangeUser(minX,maxX)
        limits.GetYaxis().SetRangeUser(5*10**-1,2*10**4) #SetRangeUser(10**-3.5,10**5.5)
        limits.GetXaxis().SetTitle("m_{VLL} (GeV)")
        limits.GetYaxis().SetTitle("95% CL limit on #sigma/#sigma_{theor}")
        limits.GetXaxis().SetTitleSize(0.04)
        limits.GetYaxis().SetTitleSize(0.04)
        limits.GetXaxis().SetTitleOffset(0.92)
        limits.GetYaxis().SetTitleOffset(0.92)
        limits.GetXaxis().SetLabelSize(0.03)
        limits.GetYaxis().SetLabelSize(0.03)            
        ################################################################

        lumi_label = '58.7 fb^{-1}'#'%s' % float('%.3g' % (lumi/1000.)) + " fb^{-1}"
        texS = TLatex(0.20,0.837173,("#it{Work in progress}"))
        texS.SetNDC()
        texS.SetTextFont(42)
        texS.SetTextSize(0.040)
        texS.Draw('same')
        texS1 = TLatex(0.12092,0.907173,"#bf{CMS} #it{Preliminary} ("+year+")        "+lumi_label+" #sqrt{s} = 13 TeV")
        texS1.SetNDC()
        texS1.SetTextFont(42)
        texS1.SetTextSize(0.040)
        texS1.Draw('same')

        legend.Draw('same')

        line = TLine(minX,1,maxX,1)
        line.SetLineStyle(8)
        line.Draw('same')

        c.Modified()
        c.Update()
        gROOT.GetListOfCanvases().Draw()
        outdir = outdir_base# % year
        fname = 'limits_%s_VLL.png'%(year)
        c.SaveAs( '%s/%s' % (outdir,fname) )

make_plot(limits_to_plot)        

