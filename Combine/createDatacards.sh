#!/bin/bash

for mass in {500..1000..50}
do

datacard="datacard_2018_VLL"${mass}"GeV.txt"
echo creating datacard ${datacard}

cat > $datacard <<@EOI
imax 1
jmax 1
kmax *
---------------

shapes * *  Shapes.root  \$PROCESS \$SYSTEMATIC

---------------
bin SR_4j4b
observation -1
------------------------------
bin             SR_4j4b           SR_4j4b
process         VLL_M-${mass}GeV   ttbar
process         0                   1
rate            -1                 -1
--------------------------------

--------------------------------
# syst / theoretical
epsilon_2018                     lnN         -          1.1

--------------------------------
# syst / experimental

# Background
ttbar_JES                        shape       -          1.0
ttbar_JER                        shape       -          1.0
ttbar_puWeight                   shape       -          1.0
ttbar_muEffWeight                shape       -          1.0
ttbar_eleEffWeight               shape       -          1.0


# signal
lumi_13TeV_2018                 lnN        1.025        -

--------------------------------
# [channel] autoMCStats [gauss/poisson threshold] [include-signal = 0] [hist-mode = 1]
* autoMCStats 10 0 1
@EOI


done
