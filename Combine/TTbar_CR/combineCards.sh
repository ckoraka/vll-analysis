#!/bin/bash

INPUT_DIR=$1
OUTPUT_DIR=$2

if [ ! -d ${OUTPUT_DIR} ]
then
    mkdir -p ${OUTPUT_DIR}
fi

for mass in {500..1000..50}; do

    # Initialize variables
    LIST_CR=""
    LIST_SR=""
    DATACARD_LIST=""

    # Loop over the bins 
    for i in {0..10}; do
        LIST_CR+="${INPUT_DIR}/VLL_M-${mass}GeV_VLL-2l_ST_bin_${i}_CR_region.txt "
        LIST_SR+="${INPUT_DIR}/VLL_M-${mass}GeV_VLL-2l_ST_bin_${i}_SR_region.txt "
    done

    # Combine lists of CR and SR into DATACARD_LIST
    DATACARD_LIST="${LIST_CR}${LIST_SR}"
	 echo $DATACARD_LIST
    # Combine datacards using combineCards.py
    combineCards.py ${DATACARD_LIST} > ${OUTPUT_DIR}/datacard_VLL_M-${mass}GeV_2018.txt
done

echo "Datacards combined successfully!"

