**HOW TO SET UP AND RUN HIGGS COMBINE :**

Follow the set-up instructions found here :
<https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/>


## Create the datacards

To create the datacards :
```
python datacard.py --year 2018
```

The root files containing the histograms used as inputs from the datacards are produced using the macro **createHistos.py** that is in the base directory.

After the histos and datacards are created make sure to properly set up CMSSW and cd to the src directory. Also dont forget to cmsenv.

## Combine the cards

```
source combineCards.sh directory_where _cards_are_located  directory_to_write_out_combined_cards
```

## Run the limits

**Note** Make sure the directory structure is proper otherwise combine wont be able to find the histograms.


To run the workflow to produce limits:
```
source runAnalysis.sh 
```

To create the limit plot : 
```
plotLimits.py
```
**Note:** Change the dictionairy with the expected limit values inside the script with the values from the created json file.

## Look at the impacts

Example script that produces the impacts (blinded fit) for r=0 and r=1 hypotheses for the 00 GeV mass point. 
```
runImpacts.sh
```

Other helpful commands:

1. By adding -v3 OR -v2 at the end of any combine command , additional information is printed - helful for debugging

2. combineCards.py Era_2016=datacard_2016.txt Era_2017=datacard_2017.txt Era_2018=datacard_2018.txt > datacard_Run2.txt #is used to combine datacards


