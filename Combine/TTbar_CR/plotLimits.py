from ROOT import TH2D,TGraph,TGraphAsymmErrors,TMultiGraph,TCanvas,TLegend,TLatex,TLine,kRed, kGreen, kYellow,gStyle,gROOT,TSpline3
from array import *
import ctypes

limits_dict = {
  "1000.0": {
    "exp+1": 2.7785701751708984,
    "exp+2": 5.01877498626709,
    "exp-1": 1.0857009887695312,
    "exp-2": 0.7763671875,
    "exp0": 1.65625
  },
  "500.0": {
    "exp+1": 25.0299072265625,
    "exp+2": 37.7206916809082,
    "exp-1": 6.815422058105469,
    "exp-2": 4.615478515625,
    "exp0": 12.4375
  },
  "550.0": {
    "exp+1": 21.50811195373535,
    "exp+2": 32.41325759887695,
    "exp-1": 5.83428955078125,
    "exp-2": 4.0078125,
    "exp0": 10.6875
  },
  "600.0": {
    "exp+1": 13.143845558166504,
    "exp+2": 19.808103561401367,
    "exp-1": 3.8703556060791016,
    "exp-2": 2.704345703125,
    "exp0": 6.53125
  },
  "650.0": {
    "exp+1": 10.754055976867676,
    "exp+2": 16.206628799438477,
    "exp-1": 3.219818115234375,
    "exp-2": 2.25439453125,
    "exp0": 5.34375
  },
  "700.0": {
    "exp+1": 8.867379188537598,
    "exp+2": 13.363361358642578,
    "exp-1": 2.709935188293457,
    "exp-2": 1.9105224609375,
    "exp0": 4.40625
  },
  "750.0": {
    "exp+1": 6.749971866607666,
    "exp+2": 10.376863479614258,
    "exp-1": 2.146608352661133,
    "exp-2": 1.5238037109375,
    "exp0": 3.421875
  },
  "800.0": {
    "exp+1": 5.321254730224609,
    "exp+2": 8.668584823608398,
    "exp-1": 1.8209667205810547,
    "exp-2": 1.295654296875,
    "exp0": 2.859375
  },
  "850.0": {
    "exp+1": 3.7070424556732178,
    "exp+2": 6.5110063552856445,
    "exp-1": 1.4038801193237305,
    "exp-2": 1.007080078125,
    "exp0": 2.1484375
  },
  "900.0": {
    "exp+1": 3.624530553817749,
    "exp+2": 6.251079559326172,
    "exp-1": 1.3381576538085938,
    "exp-2": 0.958740234375,
    "exp0": 2.0625
  },
  "950.0": {
    "exp+1": 3.292341947555542,
    "exp+2": 5.729920864105225,
    "exp-1": 1.2217979431152344,
    "exp-2": 0.8714599609375,
    "exp0": 1.890625
  },
}

limit_keys = ['exp0','exp-1','exp-2','exp+1','exp+2','exp1','exp2']
xlist = []; ylist = []; ylist_exp_minus1 = [] ; ylist_exp_minus2 = [] ; ylist_exp_plus1 = [] ; ylist_exp_plus2 = []
for mv in limits_dict.keys():
        x = float(mv); y = limits_dict[mv]['exp0']; y_exp_minus1 = limits_dict[mv]['exp-1']; y_exp_minus2 = limits_dict[mv]['exp-2']; y_exp_plus1 = limits_dict[mv]['exp+1']; y_exp_plus2 = limits_dict[mv]['exp+2']
        xlist.append(x); ylist.append(5*y); ylist_exp_minus1.append(5*y_exp_minus1); ylist_exp_minus2.append(5*y_exp_minus2); ylist_exp_plus1.append(5*y_exp_plus1); ylist_exp_plus2.append(5*y_exp_plus2)

xlist = array('d',xlist)
ylists= [ylist,ylist_exp_minus1,ylist_exp_minus2,ylist_exp_plus1,ylist_exp_plus2]
merged_list = list(zip(ylists,limit_keys))
limits_to_plot = {}
for i, my_list in enumerate(ylists):
        my_list = array('d',my_list); print(type(my_list))
        my_tag = merged_list[i][-1]; print(my_tag)
        limits_to_plot[my_tag] = TGraph(len(xlist),xlist,my_list)

def get_band(up_y,down_y,name,xlist=xlist,central_y=array('d',ylist)):
        print("####") ; print(up_y) ; print("####")
        print("####") ; print(down_y) ; print("####")
        print("####") ; print(name) ; print("####")
        print("####") ; print(xlist) ; print("####")
        print("####") ; print(central_y) ; print("####")
        yvals_lo = [a_i - b_i for a_i,b_i in zip(central_y,down_y)]
        yvals_hi = [a_i - b_i for a_i,b_i in zip(up_y,central_y)]
        print("$$$$") ; print(yvals_lo) ; print("$$$$")
        print("$$$$") ; print(yvals_hi) ; print("$$$$")
        limits_to_plot[name] = TGraphAsymmErrors(len(xlist),xlist,central_y,array('d', [0]),array('d', [0]),array('d', yvals_lo), array('d', yvals_hi)); limits_to_plot[name].Sort()

get_band(ylist_exp_plus1,ylist_exp_minus1,'exp1')    
get_band(ylist_exp_plus2,ylist_exp_minus2,'exp2')      

outdir_base = './'
def make_plot(limits_to_plot):
        year = '2018'
        print(limits_to_plot) 
        class Bounds:
                def __init__(self):
                        self.xmax = None
                        self.xmin = None
                        self.ymax = None
                        self.ymin = None
                def setBounds(self,x,y):
                        if self.xmax is None: self.xmax = x
                        if self.xmin is None: self.xmin = x
                        if self.ymax is None: self.ymax = y
                        if self.ymin is None: self.ymin = y

                        self.xmax = max(self.xmax,x)
                        self.xmin = min(self.xmin,x)
                        self.ymax = max(self.ymax,y)
                        self.ymin = min(self.ymin,y)
                def getBounds(self): return self.xmin,self.xmax,self.ymin,self.ymax
                def __str__(self): return 'x: [%f-%f] y: [%f-%f]' % (self.xmin,self.xmax,self.ymin,self.ymax)
        bounds = Bounds()
        for graph_type,graph in limits_to_plot.iteritems():
                x,y = ctypes.c_double(0),ctypes.c_double(0)
                for i in range(graph.GetN()):
                        graph.GetPoint(i,x,y)
                        bounds.setBounds(x.value,y.value)

        minX,maxX,minY,maxY = bounds.getBounds()
        print(bounds)

        xsecs = array('d',[27.09202,14.381918,9.378963,6.379769,4.3529904,3.0312503,2.1412405,1.5076611,1.0972377,0.7933595,0.5764625])
        massPoints = array('d',[500,550,600,650,700,750,800,850,900,950,1000])
        gr = TGraph( len(xsecs), massPoints, xsecs )
        gr.SetLineColor( 2 )
        gr.SetLineWidth( 2 )
        spl = TSpline3("gr",gr)
        spl.SetLineColor(kRed)
        spl.SetLineWidth(2)

        ######################################################################
        draw=['exp2', 'exp1', 'exp0']
        #draw=['exp-2', 'exp-1', 'exp0','exp+1', 'exp+2']
        style_dict = {
                #'obs' : { 'LineWidth' : 2, 'LineStyle': 9, 'MarkerSize': 1.5 ,'MarkerStyle': 20},
                'exp0' : { 'LineWidth' : 2, 'LineColor' : 1, 'LineStyle': 2, 'MarkerSize': 2 ,'MarkerStyle': 1},
                'exp1' : { 'FillColor' : kGreen},
                'exp2' : { 'FillColor' : kYellow},
                'exp-1' : { 'LineWidth' : 2, 'LineColor' : kGreen, 'LineStyle': 1},
                'exp+1' : { 'LineWidth' : 2, 'LineColor' : kGreen, 'LineStyle': 1},
                'exp-2' : { 'LineWidth' : 2, 'LineColor' : kYellow, 'LineStyle': 1},
                'exp+2' : { 'LineWidth' : 2, 'LineColor' : kYellow, 'LineStyle': 1}
        }

        legend_dict = {
                #'obs' : { 'Label' : 'Observed', 'LegendStyle' : 'LP', 'DrawStyle' : 'PLSAME'},
                'exp0' : { 'Label' : 'Asymptotic 95% CL_{s} expected', 'LegendStyle' : 'LP', 'DrawStyle' : 'PLSAME'},
                'exp1' : { 'Label' : '#pm1 std. deviation', 'LegendStyle' : 'F', 'DrawStyle' : '3SAME'},
                'exp2' : { 'Label' : '#pm2 std. deviation', 'LegendStyle' : 'F', 'DrawStyle' : '3SAME'},
                'spl'  : { 'Label' : 'Theory - Electroweak production', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
                'exp-1' : { 'Label' : '-1 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
                'exp+1' : { 'Label' : '+1 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
                'exp-2' : { 'Label' : '-2 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'},
                'exp+2' : { 'Label' : '+2 sigma Expected', 'LegendStyle' : 'L', 'DrawStyle' : 'LSAME'}

        }

        c = TCanvas("c","c",800,800)
        c.SetLogy()#; c.SetLogx()
        # c.SetMargin(0.15,0.15,0.15,0.08)
        gStyle.SetOptStat(0);
        gStyle.SetLegendBorderSize(0);
        # gStyle.SetPalette(kRainBow)

        limits = TMultiGraph()
        legend = TLegend(0.55,0.55,0.85,0.85,"")#x1,y1,x2,y2
        legend.SetTextSize(0.02)
        legend.SetFillColor(0)
        for key in draw:
                if key in limits_to_plot:
                        print(key)
                        limits_to_plot[key].Sort()
                        legend.AddEntry(limits_to_plot[key],legend_dict[key]['Label'],legend_dict[key]['LegendStyle'])
                        if 'LineWidth' in style_dict[key]: limits_to_plot[key].SetLineWidth(style_dict[key]['LineWidth'])
                        if 'LineColor' in style_dict[key]: limits_to_plot[key].SetLineColor(style_dict[key]['LineColor'])
                        if 'FillColor' in style_dict[key]: limits_to_plot[key].SetFillColor(style_dict[key]['FillColor'])
                        if 'LineStyle' in style_dict[key]: limits_to_plot[key].SetLineStyle(style_dict[key]['LineStyle'])
                        if 'MarkerSize' in style_dict[key]: limits_to_plot[key].SetMarkerSize(style_dict[key]['MarkerSize'])
                        if 'MarkerStyle' in style_dict[key]: limits_to_plot[key].SetMarkerStyle(style_dict[key]['MarkerStyle'])
                        limits.Add(limits_to_plot[key],legend_dict[key]['DrawStyle'])
        spl.SetLineColor(kRed)
        legend.AddEntry('spl','Electroweak production', 'L')
        limits.Draw('a')
        limits.GetXaxis().SetLimits(minX,maxX) #limits.GetXaxis().SetRangeUser(minX,maxX)
        limits.GetYaxis().SetRangeUser(8*10**-1,8*10**2) #SetRangeUser(10**-3.5,10**5.5)
        limits.GetXaxis().SetTitle("VLL mass [GeV]")
        limits.GetYaxis().SetTitle("#sigma B [fb]")
        limits.GetXaxis().SetTitleSize(0.04)
        limits.GetYaxis().SetTitleSize(0.04)
        limits.GetXaxis().SetTitleOffset(0.92)
        limits.GetYaxis().SetTitleOffset(0.92)
        limits.GetXaxis().SetLabelSize(0.03)
        limits.GetYaxis().SetLabelSize(0.03)            
        ################################################################

        lumi_label = '58.7 fb^{-1}'#'%s' % float('%.3g' % (lumi/1000.)) + " fb^{-1}"
        texS = TLatex(0.20,0.837173,("#it{Work in progress}"))
        texS.SetNDC()
        texS.SetTextFont(42)
        texS.SetTextSize(0.040)
        texS.Draw('same')
        texS1 = TLatex(0.12092,0.907173,"#bf{CMS} #it{Preliminary} ("+year+")        "+lumi_label+" #sqrt{s} = 13 TeV")
        texS1.SetNDC()
        texS1.SetTextFont(42)
        texS1.SetTextSize(0.040)
        texS1.Draw('same')
        spl.Draw('same')
        legend.Draw('same')
        gStyle.SetPadTickY(1);

        line = TLine(minX,1,maxX,1)
        line.SetLineStyle(8)
        line.Draw('same')

        c.Modified()
        c.Update()

        gROOT.GetListOfCanvases().Draw()
        outdir = outdir_base# % year
        fname = 'limits_%s_VLL.png'%(year)
        c.SaveAs( '%s/%s' % (outdir,fname) )

make_plot(limits_to_plot)        

