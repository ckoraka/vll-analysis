#! /usr/bin/env python

import os, sys, getopt, multiprocessing
import copy
import math
from array import array
from ROOT import gROOT, TFile, TH1F

import optparse
usage = "usage: %prog [options]"
parser = optparse.OptionParser(usage)
parser.add_option("-c", "--cutcount", action="store_true", default=False, dest="isCutAndCount")
parser.add_option("-v", "--verbose", action="store_true", default=False, dest="verbose")
parser.add_option("-N", "--name", action="store", type="string", dest="name", default="test")
parser.add_option("-Y", "--year", action="store", type="string", default="2016", dest="year")
(options, args) = parser.parse_args()

isShape = not options.isCutAndCount
verbose = options.verbose
year = options.year

jobs = []

#greenShape = ['CMS_eff_b', 'CMS_scale_pu', 'preFire', 'QCDscale_ren_TT', 'QCDscale_fac_TT', 'QCDscale_ren_VV', 'QCDscale_fac_VV']

greenShape = ['muEffWeight','eleEffWeight','puWeight','JES','JER',"btagVariation_cferr1","btagVariation_cferr2","btagVariation_hf","btagVariation_hfstats1","btagVariation_hfstats2","btagVariation_lf","btagVariation_lfstats1","btagVariation_lfstats2"]

#greenShape = []

categories = []
back = []
sign = []

shape = []
jesUnc = ['AbsoluteMPFBias', 'AbsoluteScale', 'AbsoluteStat', 'FlavorQCD', 'Fragmentation', 'PileUpDataMC', 'PileUpPtBB', 'PileUpPtEC1', 'PileUpPtEC2', 'PileUpPtHF', 'PileUpPtRef', 'RelativeFSR', 'RelativeJEREC1', 'RelativeJEREC2', 'RelativeJERHF', 'RelativePtBB', 'RelativePtEC1', 'RelativePtEC2', 'RelativePtHF', 'RelativeBal', 'RelativeSample', 'RelativeStatEC', 'RelativeStatFSR', 'RelativeStatHF', 'SinglePionECAL', 'SinglePionHCAL', 'TimePtEta']
#for unc in jesUnc:
    #greenShape.append('CMS_scale'+unc+'_j')

norm = {
    "ST_xsec"    : {"ST" : 1.200},
}

#Add lumi norm uncertainties based on year
if year == "2016":
    norm["lumi16_13TeV"] = {"VV" : 1.012, "VLL" : 1.012}
elif year == "2017":
    norm["lumi17_13TeV"] = {"VV" : 1.023, "VLL" : 1.023}
elif year == "2018":
    norm["lumi18_13TeV"] = {"VV" : 1.025, "VLL" : 1.025}

freenorm = {
}


rateparam = {
    'ttbar' : 'ttbar',
}

bins = [300,450,550,650,750,850,950,1050,1200,1400,1700,3000]

def datacard(cat, sign):
    
    if verbose: print "  Starting datacard for category", cat, "and mass", sign, "..."
        
    ### Header, Categories and Processes ###
    space = " "*50
    hline = "-"*100 + "\n"
    
    card =  "imax *   number of channels\n"
    card += "jmax *   number of backgrounds\n"
    card += "kmax *   number of nuisance parameters\n"
    card += hline
    card += "bin        " + space + ("%-25s" % cat) + "\n"
    card += "observation" + space + ("%-25.0f" % -1.) + "\n"
    card += hline
    card += "shapes * * " + space + "rootfiles_"+options.name+"/$CHANNEL.root          $PROCESS          $SYSTEMATIC/$PROCESS\n"
    card += hline
    card += "bin        " + space
    for i, s in enumerate([sign] + back):
        card += "%-30s" % cat
    card += "\n"
    card += "process    " + space
    for i, s in enumerate([sign] + back):
        card += "%-30s" % s
    card += "\n"
    card += "process    " + space
    for i, s in enumerate([sign] + back):
        card += "%-30d" % i
    card += "\n"
    card += "rate       " + space
    for i, s in enumerate([sign] + back):
        card += "%-25.6f" % getNumber(cat, s) #-1.
    card += "\n"
    card += hline
    
    if verbose: print "  Header done, now shape uncertainties..."
    
    ### Systematic Uncertainties ###
    if verbose: print "  MC statistics..."
    # MC statistics
    #card += "* autoMCStats 0.1\n"
    
    # Shape
    if isShape:
        for i, h in enumerate(shape):
            card += "%-50s shape     " % h
            for i, s in enumerate([sign] + back):
                if not checkShape(cat, s, h): card += ("%-25.0f" % 1)
                else: card += "%-25s" % "-"
            card += "\n"
    
    # Cut&count++
    else:
        for i, h in enumerate(shape):
            card += "%-50s lnN       " % h
            for c in categories:
                for b in range(nbin[c] if not isShape else 1):
                    for i, s in enumerate([sign] + back): #
                        #print c, s, h, syst[c][s][h][1].GetBinContent(b+1)
                        if syst[c][s][h][0].GetBinContent(b+1) != 0:
                            up = max(min(syst[c][s][h][1].GetBinContent(b+1)/syst[c][s][h][0].GetBinContent(b+1), 2), 0.5)
                            down = max(min(syst[c][s][h][-1].GetBinContent(b+1)/syst[c][s][h][0].GetBinContent(b+1), 2), 0.5)
                            string = "%.3f/%.3f" % (up, down)
                        else:
                            string = "-"
                        card += "%-25s" % string
            card += "\n"
    
    if verbose: print "  Shapes done, now normalization..."
    
    # Normalization
    for k in sorted(norm.keys()):
        card += "%-50s lnN       " % k
        for i, s in enumerate([sign] + back):
            issyst = False
            for n, nn in norm[k].iteritems():
                if n in s and norm[k][n]>0:
                    #print "--test ", n, s, cat
                    #if k=='CMS_eff_met_trigger' and not cat.startswith('AH0l'): continue
                    if 'preFire_1f' in k and not('1f' in cat): continue
                    if k=='pdf_accept_W' and (not('1m' in cat or '1e' in cat) or ('1e1m' in cat)): continue
                    card += ("%-25.3f" % norm[k][n])
                    issyst = True
            if not issyst: card += "%-25s" % "-"
        card += "\n"
    
    if verbose: print "  Normalization done, now minor backgrounds..."
    
    # Free backgrounds
    for k in sorted(freenorm.keys()):
        if any([True for x in back if x in k]):
            card += "%-50s lnU       " % k
            for i, s in enumerate([sign] + back):
                if s in k: card += ("%-25.3f" % freenorm[k])
                else: card += "%-25s" % "-"
            card += "\n"
    

    if verbose: print "  Rate params..."
    for p, m in rateparam.iteritems():
        print(cat)
        if ('ttbar' in m):
            if "bin" in cat:
                p = p + cat[cat.find("bin")-1:cat.find("bin")+4] + str(bins[int(cat[int(cat.find("bin"))+4])]) +'-'+ str(bins[int(cat[int(cat.find("bin"))+4])+1])
            #paper
            #print '--> p',p,'cat',cat,'m',m
            #card += "%-25s%-20s%-20s\t%-20s          1.   [0.1,2]\n" % (p, 'rateParam', cat, m, )
            #card += "%-25s%-20s%-20s\t%-20s          1.   \n" % (p, 'rateParam', cat, m, )
            card += "%-25s%-20s%-20s\t%-20s          1.   [0.1,3]\n" % (p+'_'+year+' ', 'rateParam', cat, m, )
    
    if verbose: print "  MC statistics..."
    # MC statistics
    ##paper
    card += "* autoMCStats 10\n"
    
    if verbose: print "  Done. Writing to file..."
    
    # Write to file
    try: os.stat("datacards_"+options.name) 
    except: os.mkdir("datacards_"+options.name)
    
    outname = "datacards_"+options.name+"/" + sign + '_' + cat + ".txt"
    cardfile = open(outname, 'w')
    cardfile.write(card)
    cardfile.close()
    print "Datacard for ", sign, "saved in", outname
    
    # Avoid closing the file, directly remove the TFile from internal list
    # https://root.cern.ch/phpBB3/viewtopic.php?t=14450
    #gROOT.GetListOfFiles().Remove(inFile)

def getNumber(cat, s, syst=''):
    f = TFile("rootfiles_"+options.name+"/"+cat+".root", "READ")
    h = f.Get((syst+'/' if len(syst)>0 else '')+s)
    if h==None: return -1
    n = h.Integral()
    f.Close()
    #print "In category", cat, "background", s, "has", n, "events"
    return n

def checkShape(cat, s, syst=''):
    f = TFile("rootfiles_"+options.name+"/"+cat+".root", "READ")
    h = f.Get(s)
    hUp = f.Get(syst+'Up'+'/'+s)
    hDown = f.Get(syst+'Down'+'/'+s)
    
    if h==None or hUp==None or hDown==None: 
        print "***** WARNING: sys hists do not exists!!"
        return False
    isSame = True
    
    if 'eff_e' in syst and '2m' in cat: 
        return True
    if 'eff_m' in syst and '2e' in cat: 
        return True
    if 'CMS_W' in syst and 'TTbar' in s:
        return True
    if 'CMS_Z' in syst and 'TTbar' in s:
        return True
    if 'CMS_pdf' in syst and 'DM' in s:
        return True
    if 'pdf_accept' in syst and 'DM' in s:
        return True
    if 'CMS_HF_Z' in syst and not(s.startswith('ZJ') or s.startswith('DYJets')):
        return True
    if 'CMS_HF_W' in syst and not(s.startswith('WJ')):
        return True
    if h.Integral()<1.e-20 or hUp.Integral()<1.e-20 or hDown.Integral()<1.e-20: 
        return True
    for i in range(h.GetNbinsX()):
        if not h.GetBinContent(i+1) > 0.: 
            continue
        if abs(hUp.GetBinContent(i+1)-h.GetBinContent(i+1))/h.GetBinContent(i+1) > 1.e-20 or abs(h.GetBinContent(i+1)-hDown.GetBinContent(i+1))/h.GetBinContent(i+1) > 1.e-20:
            isSame = False
            break
    f.Close()
    #if not isSame: print "systematic", syst,"for sample", s, "in cat", cat, "has been degraded"
    # if 'res' in syst or 'QCDscale' in syst:
    #     isSame = False
    return isSame

def fillLists():
    # List files and fill categories
    for c in os.listdir('rootfiles_'+options.name+'/'):
        if c.endswith('root'):
            categories.append(c.replace('.root', ''))
    
    # Read file and histograms
    inFile = TFile("rootfiles_"+options.name+"/"+categories[0]+".root", "READ")
    inFile.cd()
    for key in inFile.GetListOfKeys():
        obj = key.ReadObj()
        if obj.IsA().InheritsFrom("TH1"):
            name = obj.GetName()
            if 'VLL_M-' in name:
                sign.append( name )                
            ##paper
            elif not "data_obs" in name and not "VLL_M-" in name: back.append(name)
        # Categories (directories)
        if obj.IsFolder():
            subdir = obj.GetName()
            subdir = subdir.replace('Up', '').replace('Down', '')
            if not subdir in greenShape: continue
            if not subdir in shape: 
                shape.append(subdir)
    print 'filled shape ', shape
    inFile.Close()
        

fillLists()

print "Categories  :", categories
print "Signal      :", sign
print "Backgrounds :", back
print "Shape unc   :", shape


for cat in categories:
    for s in sign:
        if 'VLL_M-' in s:
            datacard(cat, s)

