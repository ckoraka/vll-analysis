#!/bin/bash

combineTool.py -M Impacts -d workspace_600GeV.root -m 600 --rMin -5 --rMax 5 --robustFit 1 --doInitialFit --expectSignal 1 -t -1 
combineTool.py -M Impacts -d workspace_600GeV.root -m 600 --rMin -5 --rMax 5 --robustFit 1 --doFits --expectSignal 1 -t -1
combineTool.py -M Impacts -d workspace_600GeV.root -m 600 --rMin -5 --rMax 5 --robustFit 1 --output impacts.json -t -1

plotImpacts.py -i impacts.json -o impacts_r1


combineTool.py -M Impacts -d workspace_600GeV.root -m 600 --rMin -5 --rMax 5 --robustFit 1 --doInitialFit --expectSignal 0 -t -1
combineTool.py -M Impacts -d workspace_600GeV.root -m 600 --rMin -5 --rMax 5 --robustFit 1 --doFits --expectSignal 0 -t -1
combineTool.py -M Impacts -d workspace_600GeV.root -m 600 --rMin -5 --rMax 5 --robustFit 1 --output impacts.json -t -1

plotImpacts.py -i impacts.json -o impacts_r0
