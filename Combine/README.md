**HOW TO SET UP AND RUN HIGGS COMBINE :**

Follow the set-up instructions found here :
<https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/>

## For the analysis workflow look under TTbar_CR

## Initial simple checks can be done by : 

**Commands to be used :**

To create the datacards :
```
source createDatacards.sh
```

The root files containing the histograms used as inputs from the datacards are produced using the macro **save_to_root.py** that is in the helperscripts directory.


To run the full workflow :
```
source runAnalysis.sh 
```


To create the limit plot : 
```
plotLimits.py
```


Other helpful commands:


1. By adding -v3 OR -v2 at the end of any combine command , additional information is printed - helful for debugging

2. combineCards.py Era_2016=datacard_2016.txt Era_2017=datacard_2017.txt Era_2018=datacard_2018.txt > datacard_Run2.txt #is used to combine datacards


