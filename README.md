# Vector Like Lepton search 
## Analysis Framework using COFFEA

Vector-Like Lepton (VLL) analysis looking into the phase-space of events with 2 leptons plus jets in the final state. The analysis workflow makes use of the coffea framework. Documentation can be found here :
- [Coffea](https://coffeateam.github.io/coffea/)


# Overview
The current framework utilises a 2 step approach. The first step is a skimming step starting from nanoAOD files. Then the events are further analysed by the main analysis code that performs further skimming, applies corrections etc. and creates the HIST objects which are stored in .coffea files. The analysis code can be viewed in **vll/processor.py**. Modifications to the analysis cutflow and object definitions should be performed there and also synchronized with the skimmer code. Scale factors are stored in the **vll/scalefactors** directory.

## Skimming to to deal with failing jobs when running on bare nanoAODs

The **vll/processor.py** can work directly running on nanoAODs. When running a full scale Run-2 analysis we came accross several issues of failing jobs due to the size and complexity of the code. We decided to add an initial skimming step to reduce the size of the input files before running the full analysis framwework. The skimming processor is found in **vll/skim_processor.py**.

# Instructions

**The first time :**
```
sh bootstrap.sh
```
A script called **shell** is created that contains the environment settings. 


**To produce a list of files to run on querying DAS**

In the **samples** directory there is already a full lists of Run-II UL datasets (both Data and MC). If one want to create it for different samples/campaigns
```
voms-proxy-init -voms cms 
python prepareFilelistJSON.py --inputFile inputlist.json --outputFile outputlist.json
```
**inputlist.json** should have the following format :
{
    "Campaign_name":{
    "My name":"Process_name"
   }
}

where dataset=/Process_name/Campaign_name/dataTier

One should properly configure the outputs produced to match the needen input file style for the analysis code. The redirector has to be added before each file in the outputlist.json file i.e. redirector = "root://cms-xrd-global.cern.ch//" or redirector = "root://cmsxrootd.fnal.gov//". Example of the input format can be found in the file *testData.py*. 

**Note** : The redirector to the wisconsin Tier-2 is *root://cmsxrootd.hep.wisc.edu//*


## Running the skimming step :

**To run interactively :**
```
./shell
python runProcessor_Skim.py
```

Note that the list of inputs files imported that the interactive script runs over is hardcoded (from filelists.fileset import *) 


**For running on condor:**

We can use the **runFullDataset_skim.py** script. First make sure you have a valid grid certificate and export it to your home directory:
```
voms-proxy-init -voms cms 
export X509_USER_PROXY=${HOME}/.x509up_${UID}
```

Then one should run inside the shell environment. Since the mode is interactive it is advised to open a screen session before doing the following : 
```
./shell
/usr/local/bin/python runFullDataset_skim.py MCTTbar2l -e wiscjq --era "2018" --workers 200
```
The script is set up to run over 2018 - simply replace 2018 with 2017, 2016pre or 2016post to run on different eras inside the script.


## Running the analysis step :


**To run interactively :**
```
./shell
python runProcessor.py
```

Note that the list of inputs files imported that the interactive script runs over is hardcoded (from filelists.fileset import *) 


**For running on condor:**

We can use the **runFullDataset.py** script. First make sure you have a valid grid certificate and export it to your home directory:
```
voms-proxy-init -voms cms 
export X509_USER_PROXY=${HOME}/.x509up_${UID}
```

Then one should run inside the shell environment. Since the mode is interactive it is advised to open a screen session before doing the following : 
```
./shell
/usr/local/bin/python runFullDataset.py MCTTbar2l -e wiscjq --era "2018" --workers 200
```
The script is set up to run over 2018 - simply replace 2018 with 2017, 2016pre or 2016post to run on different eras inside the script.
**Note** make sure you always import the correct collection of files.


## Running full datasets
- For running on a full era one can make use of the **run_multipleSkim.sh** or **run_multiple{era}.sh**. Open and configure them properly with the desired number of chunks/datasets/runner file etc. To then run do :

```
source run_multipleSkim.sh
or 
source run_multiple.sh
```

# Important info

Usage of both **runFullDataset_skim.py** and **runFullDataset.py** is described below :
```
runFullDataset.py [-h] [--era {"2018","2017","2016pre","2016post"}] [--chunksize CHUNKSIZE] [--maxchunks MAXCHUNKS] [--workers WORKERS] [--outdir OUTDIR] [--batch] [-e {local,wiscjq,debug}] {MCTTbar1l,MCTTbar2l,TTV,TTVV,MCZJets,DiBoson,TriBoson,MCSingleTop,MCOther,MCAll,Data}

positional arguments:
  {MCTTbar1l,MCTTbar2l,TTV,TTVV,MCZJets,DiBoson,TriBoson,MCSingleTop,MCOther,MCAll,Data}
                        Name of process to run

optional arguments:
  -h, --help            show this help message and exit
  --era {2018,2017,2016pre,2016post}
                        Era to run over
  --chunksize CHUNKSIZE
                        Chunk size
  --maxchunks MAXCHUNKS
                        Max chunks
  --workers WORKERS     Number of workers
  --outdir OUTDIR       Where to put the output files
  --batch               Batch mode (no progress bar)
  -e {local,wiscjq,debug}, --executor {local,wiscjq,debug}
                        How to run the processing

```

**For plotting :**
```
python plotting.py --config_file config.json
```
Structure of the config is the following (example config file is **config_2018.json**): 
```
{
    "input_dir": "/path/to/input/directory",
    "outdir": "/where_to_store_plots",
    "version": "v2", # Subfolder with version can be also empty
    "era": "2018", #can be 216pre,2016post,2017,2018
    "background_mc_files": { # List of coffea files
        "ttbar": ".coffea",
        "DiBoson": ".coffea",
        "SingleTop": ".coffea",
        "ZJets": ".coffea",
        "TriBoson": ".coffea",
        "TTVV": ".coffea",
        "TTV": ".coffea"
    },
    "signal_file": ".coffea",
    "data_files": [
        ".coffea",
        ...
    ]
}
```
**For creating up/down systematic variation plots :**

```
python plot_variations.py --config_file config.json
```

Exampes of pre-defined configs can be found under the *Configs* directory.


**NOTE:** Sometimes coffea images can cause some issues. If you get errors trying to run with the lates image please update to a image inside the shell e.g. : 
```
coffea-dask-cc7:0.7.21-fastjet-3.4.0.1-g6238ea8
```
Or take a look at the available list of images :
https://hub.docker.com/u/coffeateam


# Using custom images
For running with a custom image with additional packages add any additional packages to **singularity.def**. Build the singularity image:
```
singularity build myImage.sif singularity.def
```
Then add this to the submission script as the new image. This is needed for setting proper env in the remote workers. Be careful to store the image in a cvmfs like storage location otherwise it will cause severe slowdowns. **MASTER VERSION NEEDS mt2 AND tensorflow**


# For producing limits 
- Take a look in the **Combine** directory. 
- **createHistos.py** is used to prepare the root files needed as inputs for the datacards. To run the script prepare the nessesary json file with the following structure:
```
{
    "input_dir": "/path/to/input/directory",
    "era": "2016pre",
    "version": "v3",
    "outdir": "/where_to_store",
    "mc_files": [
        "output_MCTTbar2l.coffea",
        ...
        "output_Signal.coffea"
    ],
    "data_files": [
		  "output_Data.coffea"
    ]
}
```
and for running do:
```
python createHistos.py --config_file config.json
```

- Using CMSSW_11_3_4 release for running combine
- Combine documentation : https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/latest/


### Miscellanea
- Helper scripts are stored in the **helperScripts** directory
- **save_to_root.py** stores all 1-d histos in a root file, usefull for debugging
- **plot_variations.py** plots 1-d histos with nominal/up/down variations superimposed. 
- **compare_nevents.py** compares events from DAS vs processed events in skimming step. Creates dictionairies with processed events needed for proper weighting of events. These should be added to the crossections.py file in the utilities directory.



