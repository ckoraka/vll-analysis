import os

redirector = "root://cmsxrootd.hep.wisc.edu//"
base_dir = '/hdfs/store/user/ckoraka/vll/skims/v2/2018'

signal_files = {}
data_files = {}
mc_files = {}

for subdir in os.listdir(base_dir):
    subdir_path = os.path.join(base_dir, subdir)
    if os.path.isdir(subdir_path):
        if "VLL" in subdir:
            signal_files[subdir] = [f'{redirector}{subdir_path.replace("/hdfs", "")}/{f}' for f in os.listdir(subdir_path)]
        elif "Data" in subdir:
            data_files[subdir] = [f'{redirector}{subdir_path.replace("/hdfs", "")}/{f}' for f in os.listdir(subdir_path)]
        else:
            mc_files[subdir] = [f'{redirector}{subdir_path.replace("/hdfs", "")}/{f}' for f in os.listdir(subdir_path)]

def write_filelist(filename, file_dict, header):
    with open(filename, 'w') as f:
        f.write(f'{header}\n')
        f.write("{\n")
        for key, files in file_dict.items():
            f.write(f'    "{key}": [\n')
            for file in files:
                f.write(f'        "{file}",\n')
            f.write(f'    ],\n')
        f.write("}\n")

write_filelist('Data_2018_GT36.py', data_files, 'redirector = "root://cmsxrootd.hep.wisc.edu//"\ndata2018 =')
write_filelist('Signal_2018.py', signal_files, 'redirector = "root://cmsxrootd.hep.wisc.edu//"\nfilesetSignal =')
write_filelist('MC_2018.py', mc_files, 'redirector = "root://cmsxrootd.hep.wisc.edu//"\nfilelist2018 =')

print("Python files generated: Data_2018_GT36.py, Signal_2018.py, MC_2018.py")
