{
    "Run2018A-UL2018_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "339013231"
        ]
    }, 
    "Run2018A-UL2018_MiniAODv2_NanoAODv9-v2": {
        "SingleMuon": [
            "241608232"
        ]
    }, 
    "Run2018C-UL2018_MiniAODv2_NanoAODv9-v2": {
        "SingleMuon": [
            "109986009"
        ]
    }, 
    "Run2018C-UL2018_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "147827904"
        ]
    }, 
    "Run2018B-UL2018_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "153792795"
        ]
    }, 
    "Run2018B-UL2018_MiniAODv2_NanoAODv9-v2": {
        "SingleMuon": [
            "119918017"
        ]
    }, 
    "Run2018D-UL2018_MiniAODv2_NanoAODv9-v1": {
        "SingleMuon": [
            "513909894"
        ]
    }, 
    "Run2018D-UL2018_MiniAODv2_NanoAODv9-v3": {
        "SingleElectron": [
            "752524583"
        ]
    }
}