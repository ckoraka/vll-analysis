{
    "Run2017E-UL2017_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "102122055"
        ], 
        "SingleMuon": [
            "154618774"
        ]
    }, 
    "Run2017D-UL2017_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "51512837"
        ], 
        "SingleMuon": [
            "70361660"
        ]
    }, 
    "Run2017F-UL2017_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "128467223"
        ], 
        "SingleMuon": [
            "242140980"
        ]
    }, 
    "Run2017C-UL2017_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "136637888"
        ], 
        "SingleMuon": [
            "165652756"
        ]
    }, 
    "Run2017B-UL2017_MiniAODv2_NanoAODv9-v1": {
        "SingleElectron": [
            "60537490"
        ], 
        "SingleMuon": [
            "136300266"
        ]
    }
}