{
    "Run2016D-HIPM_UL2016_MiniAODv2_NanoAODv9-v2": {
        "SingleElectron": [
            "148167727"
        ], 
        "SingleMuon": [
            "98017996"
        ]
    }, 
    "Run2016C-HIPM_UL2016_MiniAODv2_NanoAODv9-v2": {
        "SingleElectron": [
            "97259854"
        ], 
        "SingleMuon": [
            "67441308"
        ]
    }, 
    "Run2016B-ver1_HIPM_UL2016_MiniAODv2_NanoAODv9-v2": {
        "SingleElectron": [
            "1422819"
        ], 
        "SingleMuon": [
            "2789243"
        ]
    }, 
    "Run2016B-ver2_HIPM_UL2016_MiniAODv2_NanoAODv9-v2": {
        "SingleElectron": [
            "246440440"
        ], 
        "SingleMuon": [
            "158145722"
        ]
    }, 
    "Run2016E-HIPM_UL2016_MiniAODv2_NanoAODv9-v2": {
        "SingleElectron": [
            "117269446"
        ], 
        "SingleMuon": [
            "90984718"
        ]
    }
}